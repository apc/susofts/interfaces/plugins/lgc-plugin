#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <tabs/outputs/LGCOutputResLexer.hpp>

namespace tut
{
struct lgcoutputreslexerdata
{
};

typedef test_group<lgcoutputreslexerdata> tg;
tg plugin_lgcoutputlexerres_test_group("Test LGCOutputLexerRes class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("LGCOutputLexerRes: Test of base class");

	LGCOutputResLexer lgor;
	ensure_equals(lgor.getMIMEType(), "application/vnd.cern.susoft.surveypad.plugin.lgc.pointResRAW");
	ensure(lgor.isOloc());
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("LGCOutputLexerRes: Test of reading bad input");

	// emtpy
	LGCOutputResLexer lgor;
	ensure_THROW(lgor.read(""), SPIOException);

	// garbage
	ensure_THROW(lgor.read("lol"), SPIOException);

	// good start
	ensure_THROW(lgor.read(R"""(

**

CALCUL DU LOL
**
A :
*** STATISTIQUES ***)"""),
		SPIOException);
}

template<>
template<>
void testobject::test<10>()
{
	set_test_name("LGCOutputLexerRes: Test of position IO");
	tut::skip("NYI");

	const ShareablePosition position{12.5, 0.769, 9.12, 0, 0, 0, true, true, true};

	LGCOutputResLexer lgor;
	ShareablePosition loaded = lgor.readPosition(lgor.write(position));
	ensure(position == loaded);
}

template<>
template<>
void testobject::test<11>()
{
	set_test_name("LGCOutputLexerRes: Test of extraInfos IO");

	ensure(LGCOutputResLexer().readExtraInfos("") == ShareableExtraInfos());
}

template<>
template<>
void testobject::test<12>()
{
	set_test_name("LGCOutputLexerRes: Test of point IO");

	const ShareablePoint point{"name", {12.5, 0.769, 9.12, 1.01, 0, 0, true, true, true}, "", "", true};
	const std::string spoint = "name 12.5 0.769 9.12 1.01";
	const ShareablePoint point2{"name", {12.5, 0.769, 9.12, 0, 0, 3.4, false, false, true}, "", "", true};
	const std::string spoint2 = "POINTS VARIABLES EN Z UNIQUEMENT (NB. = 0,  REFERENTIEL = RS2K )\nSFP A\nA\nname 12.5 0.769 0.101 9.12 3.4";
	const ShareablePoint point3{ "name", {12.5, 0.769, 9.12, 3.4, 7.2, 0, true, true, false}, "", "", true };
	const std::string spoint3 = "POINTS INVARIABLES EN Z (NB. = 0,  REFERENTIEL = RS2K )\nSFP A\nA\nname 12.5 0.769 0.101 9.12 3.4 7.2 1.01";

	LGCOutputResLexer lgor;
	ShareablePoint loaded = lgor.readPoint(spoint);
	ensure(point == loaded);
	loaded = lgor.readPoint(spoint2);
	ensure(point2 == loaded);
	loaded = lgor.readPoint(spoint3);
	ensure(point3 == loaded);
}

template<>
template<>
void testobject::test<13>()
{
	set_test_name("LGCOutputLexerRes: Test of params IO");

	ensure(LGCOutputResLexer().readParams("") == ShareableParams());
}

template<>
template<>
void testobject::test<14>()
{
	set_test_name("LGCOutputLexerRes: Test of frame IO");
	skip("Farmes are not supported yet...");

	// we create a tree like this (leaves have "*" prefix):
	// root ______ *p0
	//      \_____ *p1
	//       \____ child1 ______ *p2
	//        \___ child2 ______ *p3
	//         \__ child3 ______ *p4
	//                    \_____ *p5
	//                     \____ grandchild ______ *p6

	auto params = std::make_shared<ShareableParams>();
	ShareableFrame frame(params);

	frame.addPoint().name = "p0";
	frame.addPoint().name = "p1";
	frame.addFrame().addPoint().name = "p2";
	frame.addFrame().addPoint().name = "p3";
	auto &child3 = frame.addFrame();
	child3.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "", false, {{"DCUM", "dcum"}, {"id", "id"}}});
	child3.addPoint().name = "p5";
	child3.addFrame().addPoint().name = "p6";

	LGCOutputResLexer lgor;
	ShareableFrame loaded = lgor.readFrame(lgor.write(frame));
	const auto &fpoints = frame.getAllPoints(), lpoints = loaded.getAllPoints();
	ensure(std::equal(std::cbegin(fpoints), std::cend(fpoints), std::cbegin(lpoints), [](const auto *a, const auto *b) -> bool { return *a == *b; }));
}

template<>
template<>
void testobject::test<15>()
{
	set_test_name("LGCOutputLexerRes: Test of points list IO");
	skip("measures are not part of SPL yet...");

	ShareablePointsList spl;
	spl.getParams().extraInfos.addExtraInfo("coordinateSystem", "*OLOC");
	spl.getParams().precision = 5;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p4", {7, 8, 9, 0, 0, 0, false, false, true}, "inline", "", true, {{"lgcType", "*VZ"}}});
	frame.addPoint().name = "p5";
	frame.addPoint().name = "p6";

	LGCOutputResLexer lgor;
	ShareablePointsList loaded = lgor.read(lgor.write(spl));
	ensure(spl == loaded);
}
} // namespace tut
