#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <LGCMeasures.hpp>
#include <QtStreams.hpp>

namespace
{
LGCMeasures test_createMeasures()
{
	return LGCMeasures{
		// points
		{
			{"point1", {12, "point1", "point1", "point1 1 2 3", "*CALA", "", "", {{"Test", 1}}}}, // point1
			{"point2", {13, "point2", "point2", "point2 1 2 3 % comment", "*POIN", "*PDOR", "comment", {}}}, // point2
			{"keydifferentthannametest", {14, "keydifferentthannametest", "point3", "point3 1 2 3", "*VZ", "", "", {{"Test", 4}}}}, // point3
		},
		// frames
		{
			{"Frame1", {1, "Frame1", "Frame1", "*FRAME Frame1 0 0 0 0 0 0 1 TX TY TZ RX RZ", "*FRAME", "", ""}}, // frame1
			{"Frame2", {1, "Frame2", "Frame2", "*FRAME Frame2 1 2 3 4 5 6 7", "*FRAME", "", ""}}, // frame2
		},
		// instrument
		{
			{"CAMD", {22, "CAMD", "CAMD", "*CAMD CAMD CAMDEF 12.3", "*CAM", "CAMDEF", ""}}, // intrument1
			{"LEVEL", {23, "LEVEL", "LEVEL", "*LEVEL LEVEL LEVELDEF 1 31.2", "*LEVEL", "LEVELDEF", "comment"}}, // instrument2
			{"POLAR", {24, "POLAR", "POLAR", "*POLAR POLAR POLARDEF 0 0 0 0", "*POLAR", "POLARDEF", ""}}, // instrument3
		},
		// targets
		{
			{"CAMD/CAMDEF", {32, "CAMD/CAMDEF", "CAMDEF", "CAMDEF 0.4 0.5 0.2 43 2", "", "CAMD", ""}}, // target1
			{"LEVEL/LEVELDEF", {33, "LEVEL/LEVELDEF", "LEVELDEF", "LEVELDEF 0.3 -4 -5 3.1 0 0", "", "LEVEL", "comment"}}, // target2
			{"POLAR/POLARDEF", {34, "POLAR/POLARDEF", "POLARDEF", "POLARDEF 3 3 .02 6 0 0 0 0 0 0", "", "POLAR", ""}}, // target3
		},
		// measures
		{{"test", {42, "test", "test"}}} // NYD
	};
}
} // namespace

namespace tut
{
struct lgcmeasuresdata
{
};

typedef test_group<lgcmeasuresdata> tg;
tg plugin_lgcmeasures_test_group("Test LGCMeasures class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("LGCMeasures: Test clearAll()");

	LGCMeasures measures = test_createMeasures();
	ensure_not(measures.points.empty());
	ensure_not(measures.frames.empty());
	ensure_not(measures.instruments.empty());
	ensure_not(measures.targets.empty());
	ensure_not(measures.measures.empty());

	measures.clearAll();
	ensure(measures.points.empty());
	ensure(measures.frames.empty());
	ensure(measures.instruments.empty());
	ensure(measures.targets.empty());
	ensure(measures.measures.empty());
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("LGCMeasures: Test contains()");

	LGCMeasures measures = test_createMeasures();

	ensure(contains(measures.points, "point1"));
	ensure(contains(measures.points, "keydifferentthannametest"));
	ensure_not(contains(measures.points, "point3"));

	ensure(contains(measures.frames, "Frame1"));
	ensure_not(contains(measures.frames, "Frame3"));

	ensure(contains(measures.instruments, "CAMD"));
	ensure_not(contains(measures.instruments, "CAMDEF"));

	ensure(contains(measures.targets, "CAMD/CAMDEF"));
	ensure_not(contains(measures.targets, "CAMDEF"));

	ensure(contains(measures.measures, "test"));
	ensure_not(contains(measures.measures, "lol"));
}

template<>
template<>
void testobject::test<3>()
{
	set_test_name("LGCMeasures: Test find()");

	LGCMeasures measures = test_createMeasures();

	ensure_equals(find(measures.points, "point1"), "point1");
	ensure(find(measures.points, "point4").isEmpty());

	ensure_equals(find(measures.frames, "Frame1"), "Frame1");
	ensure(find(measures.frames, "Frame3").isEmpty());

	ensure_equals(find(measures.instruments, "CAMD"), "CAMD");
	ensure(find(measures.instruments, "CAMDEF").isEmpty());

	ensure_equals(find(measures.targets, "CAMD/CAMDEF"), "CAMD/CAMDEF");
	ensure(find(measures.targets, "CAMD").isEmpty());

	ensure_equals(find(measures.measures, ""), ""); // empty string returns empty
	ensure_equals(find(measures.measures, "test"), "test");
	ensure(find(measures.measures, "lol").isEmpty());
}
} // namespace tut
