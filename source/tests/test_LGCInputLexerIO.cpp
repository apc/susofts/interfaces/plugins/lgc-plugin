#include <tut/tut.hpp>
#include <tut/tut_macros.hpp>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <tabs/inputs/LGCInputLexerIO.hpp>

namespace tut
{
struct lgcinputlexeriodata
{
};

typedef test_group<lgcinputlexeriodata> tg;
tg plugin_lgcinputlexerio_test_group("Test LGCInputLexerIO class.");
typedef tg::object testobject;
} // namespace tut

namespace tut
{
template<>
template<>
void testobject::test<1>()
{
	set_test_name("LGCInputLexerIO: Test of base class");

	LGCInputLexerIO lgio;
	ensure_equals(lgio.getMIMEType(), "application/vnd.cern.susoft.surveypad.plugin.lgc.pointRAW");
}

template<>
template<>
void testobject::test<2>()
{
	set_test_name("LGCInputLexerIO: Test of reading bad input");

	// emtpy
	LGCInputLexerIO lgio;
	ensure_THROW(lgio.read(""), SPIOException);

	// garbage
	ensure_THROW(lgio.read("lol"), SPIOException);

	// bad coordinate system
	ensure_THROW(lgio.read("*LOL"), SPIOException);

	// good start
	ensure_THROW(lgio.read("*TITR\nLOL LOL LOL\n*HAHA\n"), SPIOException);
}

template<>
template<>
void testobject::test<10>()
{
	set_test_name("LGCInputLexerIO: Test of position IO");

	const ShareablePosition position{12.5, 0.769, 9.12, 0, 0, 0, true, true, true};

	LGCInputLexerIO lgio;
	ShareablePosition loaded = lgio.readPosition(lgio.write(position));
	ensure(position == loaded);
}

template<>
template<>
void testobject::test<11>()
{
	set_test_name("LGCInputLexerIO: Test of extraInfos IO");

	ensure(LGCInputLexerIO().readExtraInfos("") == ShareableExtraInfos());
}

template<>
template<>
void testobject::test<12>()
{
	set_test_name("LGCInputLexerIO: Test of point IO");

	const ShareablePoint point{"name", {12.5, 0.769, 9.12, 0, 0, 0, true, true, true}, "inline comment", "header comment", true, {{"lgcType", "*POIN"}}};
	const ShareablePoint point2{"name", {12.5, 0.769, 9.12, 0, 0, 0, true, true, true}, "", "header comment", true, {{"lgcType", "*POIN"}, {"id", "54632"}, {"DCUM", "393.5642"}}};

	LGCInputLexerIO lgio;
	ShareablePoint loaded = lgio.readPoint(lgio.write(point));
	ensure(point == loaded);
	loaded = lgio.readPoint(lgio.write(point2));
	ensure(point2 == loaded);
}

template<>
template<>
void testobject::test<13>()
{
	set_test_name("LGCInputLexerIO: Test of params IO");

	const ShareableParams params{5, ShareableParams::ECoordSys::k2DPlusH, {{"coordinateSystem", "*SPHE"}}};

	LGCInputLexerIO lgio;
	ShareableParams loaded = lgio.readParams(lgio.write(params));
	ensure(params == loaded);
}

template<>
template<>
void testobject::test<14>()
{
	set_test_name("LGCInputLexerIO: Test of frame IO");

	// we create a tree like this (leaves have "*" prefix):
	// root ______ *p0
	//      \_____ *p1
	//       \____ child1 ______ *p2 -- Frame translation modified
	//        \___ child2 ______ *p3 -- Frame rotation modified 
	//         \__ child3 ______ *p4 -- Frame scale modified
	//                    \_____ *p5
	//                     \____ grandchild ______ *p6

	auto params = std::make_shared<ShareableParams>();
	ShareableFrame frame(params);

	// add chaning trans rot and scale in some frames
	frame.add(new ShareablePoint{"p0", {1, 2, 3}, "inline", "", true, {{"lgcType", "*CALA"}}});
	frame.add(new ShareablePoint{"p1", {2, 3, 4}, "inline", "", true, {{"lgcType", "*CALA"}}});
	auto &child1 = frame.addFrame();
	child1.getTranslation().isfreex = true;
	child1.getTranslation().x = 100;
	child1.add(new ShareablePoint{"p2", {4, 5, 6}, "inline", "", true, {{"lgcType", "*CALA"}}});
	auto &child2 = frame.addFrame();
	child2.getRotation().isfreex = true;
	child2.getRotation().x = 100;
	child2.add(new ShareablePoint{"p3", {5, 6, 7}, "inline", "", true, {{"lgcType", "*CALA"}}});
	auto &child3 = frame.addFrame();
	child3.setScale(100);
	child3.add(new ShareablePoint{"p4", {6, 7, 8}, "", "header", true, {{"lgcType", "*CALA"}, {"DCUM", "dcum"}, {"id", "id"}}});
	child3.add(new ShareablePoint{"p5", {7, 8, 9}, "inline", "", true, {{"lgcType", "*CALA"}}});
	auto &grandchild = child3.addFrame();
	grandchild.add(new ShareablePoint{"p6", {8, 9, 10}, "inline", "", true, {{"lgcType", "*CALA"}}});

	LGCInputLexerIO lgio;
	auto test = lgio.write(frame);
	ShareableFrame loaded = lgio.readFrame(lgio.write(frame));
	const auto &fpoints = frame.getAllPoints(), lpoints = loaded.getAllPoints();
	ensure(std::equal(std::cbegin(fpoints), std::cend(fpoints), std::cbegin(lpoints), [](const auto *a, const auto *b) -> bool {
		return *a == *b; }));
	ensure_equals(loaded.getFrame(0).getTranslation().isfreex, child1.getTranslation().isfreex);
	ensure_equals(loaded.getFrame(0).getTranslation().x, child1.getTranslation().x);
	ensure_equals(loaded.getFrame(1).getRotation().isfreex, child2.getRotation().isfreex);
	ensure_equals(loaded.getFrame(1).getRotation().x, child2.getRotation().x);
	ensure_equals(loaded.getFrame(2).getScale(), child3.getScale());
}

template<>
template<>
void testobject::test<15>()
{
	set_test_name("LGCInputLexerIO: Test of points list IO");
	skip("measures are not part of SPL yet...");

	ShareablePointsList spl;
	spl.getParams().extraInfos.addExtraInfo("coordinateSystem", "*OLOC");
	spl.getParams().precision = 5;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p4", {7, 8, 9, 0, 0, 0, false, false, true}, "inline", "", true, {{"lgcType", "*VZ"}}});
	frame.addPoint().name = "p5";
	frame.addPoint().name = "p6";

	LGCInputLexerIO lgio;
	ShareablePointsList loaded = lgio.read(lgio.write(spl));
	ensure(spl == loaded);
}

template<>
template<>
void testobject::test<16>()
{
	set_test_name("LGCInputLexerIO: Test read and write files");
	skip("measures are not part of SPL yet...");

	ShareablePointsList spl;
	spl.getParams().extraInfos.addExtraInfo("coordinateSystem", "*OLOC");
	spl.getParams().precision = 5;
	auto &frame = spl.getRootFrame();
	frame.add(new ShareablePoint{"p4", {7, 8, 9}, "inline", "", false, {{"DCUM", "dcum"}, {"id", "id"}}});
	frame.addPoint().name = "p5";
	frame.addPoint().name = "p6";

	LGCInputLexerIO lgio;
	IShareablePointsListIO::writeFile("splistlgcTest.lgc2", lgio.write(spl));
	auto loaded = lgio.read(IShareablePointsListIO::readFile("splistlgcTest.lgc2"));
	ensure(spl == loaded);
	std::remove("splistlgcTest.lgc2");
}
} // namespace tut
