#include "LGCMeasures.hpp"

#include <algorithm>

#include <QObject>

#if (QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
namespace std
{
template<>
struct hash<QString>
{
	std::size_t operator()(const QString &s) const noexcept { return (size_t)qHash(s); }
};
} // namespace std
#endif

// offsets required to shift the int value in order to encode enum and isLine respectively
const static int enumOffset = 29;
const static int isLineOffset = 28;

//
// LGCMeasures::Value
//
int LGCMeasures::Value::totalCount() const
{
	return std::accumulate(std::begin(counter), std::end(counter), 0, [](const int previous, const std::pair<QString, int> &p) { return previous + p.second; });
}
QString LGCMeasures::Value::getObservationsSummary() const
{
	QString out = QObject::tr("  %1 Observation(s):").arg(totalCount());
	for (auto const &element : counter)
		out += "\n    " + element.first + ": " + QString::number(element.second);
	return out;
}
QString LGCMeasures::Value::getSummary() const
{
	return QObject::tr("  %1 %2\n  %3\n  %4").arg(type, name, representation, others);
}

//
// LGCMeasures::MeasureMap
//
LGCMeasures::MeasureMap::MeasureMap(std::initializer_list<std::pair<const QString, Value>> list)
{
	for (const auto &[key, value] : list)
		_map[hashString(key)] = value;
}

const LGCMeasures::Value &LGCMeasures::MeasureMap::at(int key) const
{
	return _map.at(key);
}
template<typename StringType>
LGCMeasures::Value &LGCMeasures::MeasureMap::at(const StringType &key)
{
	return _map.at(hashString(key));
}
template<typename StringType>
const LGCMeasures::Value &LGCMeasures::MeasureMap::at(const StringType &key) const
{
	return _map.at(hashString(key));
}
template<typename StringType>
LGCMeasures::Value &LGCMeasures::MeasureMap::operator[](const StringType &key)
{
	return _map[hashString(key)];
}

template<typename StringType>
std::pair<LGCMeasures::MeasureMap::iterator, bool> LGCMeasures::MeasureMap::emplace(const StringType &key, Value val)
{
	return _map.emplace(hashString(key), val);
}

LGCMeasures::MeasureMap::iterator LGCMeasures::MeasureMap::begin()
{
	return _map.begin();
}
LGCMeasures::MeasureMap::iterator LGCMeasures::MeasureMap::end()
{
	return _map.end();
}
LGCMeasures::MeasureMap::const_iterator LGCMeasures::MeasureMap::begin() const
{
	return _map.cbegin();
}
LGCMeasures::MeasureMap::const_iterator LGCMeasures::MeasureMap::end() const
{
	return _map.cend();
}

template<typename StringType>
bool LGCMeasures::MeasureMap::contains(const StringType &key) const
{
	return _map.count(hashString(key));
}
template<typename StringType>
size_t LGCMeasures::MeasureMap::count(const StringType &key) const
{
	return _map.count(hashString(key));
}
size_t LGCMeasures::MeasureMap::count(int key) const
{
	return _map.count(key);
}
size_t LGCMeasures::MeasureMap::size() const
{
	return _map.size();
}
bool LGCMeasures::MeasureMap::empty() const
{
	return !(bool)_map.size();
}

void LGCMeasures::MeasureMap::clear()
{
	_map.clear();
}

//
// LGCMeasures
//
// static keyword not allowed here since it is a storage specifier, we declared it in the .hpp file
template<typename StringType>
inline uint32_t LGCMeasures::hashString(const StringType &key)
{
	const static std::hash<StringType> hasher;
	uint32_t fullHash = static_cast<uint32_t>(hasher(key));
	// Mask the full hash to 28 bits
	// data conversion loss, increased collision chance, but QScintilla requires int for indicator so there is no way around it
	return fullHash & 0xFFFFFFF;
}

// inline keyword cannot appear in cpp if the function is not templated
uint32_t LGCMeasures::encodeIndicator(const QString &key, LGCMeasures::LGCMeasuresEnum enumValue, bool isLine)
{
	// get hash
	uint32_t fullHash = hashString(key);
	// encode enum
	uint32_t enumBits = static_cast<uint32_t>(enumValue) << enumOffset;
	fullHash = fullHash | enumBits;
	// encode line position
	fullHash |= (uint32_t)isLine << isLineOffset;
	return fullHash;
}

// input is unsigned (since there is no negative position and the position will not be higher than a few milions)
uint32_t LGCMeasures::encodeIndicator(uint32_t key, bool isLine)
{
	// by default the highest bit are not set (direct link), so only the isLine remains to be set
	key |= (uint32_t)isLine << isLineOffset;
	return key;
}

void LGCMeasures::clearAll() noexcept
{
	points.clear();
	frames.clear();
	instruments.clear();
	targets.clear();
	measures.clear();
}

// Template instantiations (otherwise templates have to be defined in the header file)
// Here we instantiate all the possible cases we want to support
// If new type is needed, the compilation will fail since it cannot see the implementation that is defined in the .cpp file.
// The other approach is to leave all the definitions of these templated functions in the header file and then the relevant
// template instatiations will be compiled when needed.
// We are supporting only QString and std::string here.

// LGCMeasures
template uint32_t LGCMeasures::hashString(const QString &key);
template uint32_t LGCMeasures::hashString(const std::string &key);
// LGCMeasures::MeasureMap
template LGCMeasures::Value &LGCMeasures::MeasureMap::at(const QString &key);
template LGCMeasures::Value &LGCMeasures::MeasureMap::at(const std::string &key);
template const LGCMeasures::Value &LGCMeasures::MeasureMap::at(const QString &key) const;
template const LGCMeasures::Value &LGCMeasures::MeasureMap::at(const std::string &key) const;
template LGCMeasures::Value &LGCMeasures::MeasureMap::operator[](const QString &key);
template LGCMeasures::Value &LGCMeasures::MeasureMap::operator[](const std::string &key);
template std::pair<LGCMeasures::MeasureMap::iterator, bool> LGCMeasures::MeasureMap::emplace(const QString &key, LGCMeasures::Value val);
template std::pair<LGCMeasures::MeasureMap::iterator, bool> LGCMeasures::MeasureMap::emplace(const std::string &key, LGCMeasures::Value val);
template bool LGCMeasures::MeasureMap::contains(const QString &key) const;
template bool LGCMeasures::MeasureMap::contains(const std::string &key) const;
template size_t LGCMeasures::MeasureMap::count(const QString &key) const;
template size_t LGCMeasures::MeasureMap::count(const std::string &key) const;

// Map helpers
QString find(const LGCMeasures::MeasureMap &map, const QString &name)
{
	if (name.isEmpty())
		return QString();
	const auto it = std::find_if(std::cbegin(map), std::cend(map), [&name](const auto &p) -> bool { return p.second.key == name; });
	return it == std::cend(map) ? QString() : it->second.key;
}

bool contains(const LGCMeasures::MeasureMap &map, const QString &key)
{
	return map.contains(key);
}
