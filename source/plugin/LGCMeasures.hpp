/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef LGCMEASURES_HPP
#define LGCMEASURES_HPP

#include <unordered_map>

#include <QHash>
#include <QString>


/**
* This class is used to:
* - track all the elements of the LGC file inside of separate maps,
* - to provide the smart indicator features for LGC files linking,
* - to provide calltips to some LGC elements.
* This class provides helpers to make all these things easier to manage.
* 
* The most complicated concept is the smart indicator feature. Since QScintilla provides the indicator concept
* we can use it to link to specific positions in the files. The problem is that the user can modify the input
* file, and then the positions fixed in the output files are wrong. To solve this, the encoding of position
* was devised.
* 
* Now the map key is being hashed so that we can provide it to QScintilla as an indicator.
* This indicator only tells us which key we are interested in, but we also need to know in which map do we want to 
* search for the key. For that we use 3 bits of the int since we have 5 maps. Lastly, we use one bit to define if
* we want to point to a line or to a position in case of direct links (fixed links to specific positions in a file).
* This preserves all the link and simultaneously allows to map to the new positions in the input file despite the changes.
* All this information is decoded in @LGCGraphicalWidget::globalIndicatorClicked to make the jump to the position.
* 
* In terms of bit layout, it is the best to refer to LGCMeasures::Hash that uses the bitsets and union concept.
*/
struct LGCMeasures
{
	// Enum used to decode which map the indicator refers to
	enum class LGCMeasuresEnum
	{
		direct = 0,
		points,
		frames,
		instruments,
		targets,
		measures
	};

	// Helper class that allows easy decoding of specific bit ranges encoding information about key, isLine and enumMap
	union Hash
	{
		Hash(uint32_t hash) : hash(hash) {}

		uint32_t hash;

		struct
		{
			uint32_t key : 28;
			uint32_t isLine : 1;
			uint32_t enumMap : 3;
		} hashFields;
	};

	// Returns hashed string that occupies only the first 28 bits of an int
	template<typename StringType>
	static inline uint32_t hashString(const StringType &key);
	// Encodes the entire indicator by hashing the key to 28 bits, encoding the is line in 1 bit, and encoding the desired map in 3 bits
	static uint32_t encodeIndicator(const QString &key, LGCMeasuresEnum enumValue, bool isLine = false);
	// input is unsigned (since there is no negative position and the position will not be higher than a few milions)
	// Encodes the entire indicator that points to the direct map/position (therefore, no map is required)
	static uint32_t encodeIndicator(uint32_t key, bool isLine = false);


	struct Value
	{
		/** position in the editor */
		int position = -1;
		/** key used for hashing */
		QString key;
		/** real name */
		QString name;
		/** representation (with options...) */
		QString representation;
		/** type of instrument, of point... */
		QString type;
		/** default target for instrument, PDOR for points, linked instrument for targets */
		QString others;
		/** comment before the current statement */
		QString headerComment;
		/** number of measurements types for the point, assigned in LGCInputLexerIO::setPoint */
		std::unordered_map<QString, int> counter{}; // scalar, so new entries default to 0

		int totalCount() const;
		QString getObservationsSummary() const;
		QString getSummary() const;
	};

	/**
	* Helper class that wraps std::unordered_map and exposes some of its methods.
	* The purpose of this class is to abstract the hashing/key manipulation from the user of this class.
	* The user only needs to use std::string or QString and the hashing will take place implicitly.
	* Lastly, all the methods for checking or counting the elements also provide methods using the non-hashed values.
	**/
	class MeasureMap
	{
	public:
		// Types
		using MapType = std::unordered_map<uint32_t, Value>;
		using iterator = typename MapType::iterator;
		using const_iterator = typename MapType::const_iterator;

		MeasureMap() {}
		MeasureMap(std::initializer_list<std::pair<const QString, Value>> list);

		// Access
		const Value &at(int key) const;
		template<typename StringType>
		Value &at(const StringType &key);
		template<typename StringType>
		const Value &at(const StringType &key) const;
		template<typename StringType>
		Value &operator[](const StringType &key);

		// Insertion
		template<typename StringType>
		std::pair<iterator, bool> emplace(const StringType &key, Value val);

		// Iterators
		iterator begin();
		iterator end();
		const_iterator begin() const;
		const_iterator end() const;

		// Seach functionalities
		template<typename StringType>
		bool contains(const StringType &key) const;

		template<typename StringType>
		size_t count(const StringType &key) const;
		size_t count(int key) const;

		// Size related
		size_t size() const;
		bool empty() const;
		void clear();

	private:
		std::unordered_map<uint32_t, Value> _map;
	};

	/** key is point name */
	MeasureMap points;
	/** key is frame_name */
	MeasureMap frames;
	/** key is instrument name */
	MeasureMap instruments;
	/** key is instrument_name/target_name */
	MeasureMap targets;
	/** key is instrument_name/measure_type/point_name/idx */
	MeasureMap measures;

	void clearAll() noexcept;
};

// Map helpers 
QString find(const LGCMeasures::MeasureMap &map, const QString &name);
bool contains(const LGCMeasures::MeasureMap &map, const QString &key);

#endif // LGCMEASURES_HPP
