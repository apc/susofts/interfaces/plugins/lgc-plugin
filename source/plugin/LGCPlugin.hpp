/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef LGCPLUGIN_HPP
#define LGCPLUGIN_HPP

#include <memory>

#include <interface/SPluginInterface.hpp>

class LGCPlugin : public QObject, public SPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID SPluginInterfaceIID)
	Q_INTERFACES(SPluginInterface)

public:
	LGCPlugin(QObject *parent = nullptr);
	virtual ~LGCPlugin() override;

	virtual const QString& name() const noexcept override { return LGCPlugin::_name(); }
	virtual QIcon icon() const override;

	virtual void init() override;
	virtual void terminate() override;

	virtual bool hasConfigInterface() const noexcept override { return true; }
	virtual SConfigWidget *configInterface(QWidget *parent = nullptr) override;
	virtual bool hasGraphicalInterface() const noexcept override { return true; }
	virtual SGraphicalWidget *graphicalInterface(QWidget *parent = nullptr) override;
	virtual bool hasLaunchInterface() const noexcept override { return true; }
	virtual SLaunchObject *launchInterface(QObject *parent = nullptr) override;

	virtual QString getExtensions() const noexcept override { return tr("LGC (*.lgc2 *.lgc *.inp)"); }
	virtual bool isMonoInstance(void) const noexcept override { return false; }

	// not exposed methods
public:
	/** @return the name of the plugin */
	static const QString& _name();
	/** @return the base mimetype usde */
	static const QString& _baseMimetype();

private:
	class _LGCPlugin_pimpl;
	std::unique_ptr<_LGCPlugin_pimpl> _pimpl;
};

#endif // LGCPLUGIN_HPP
