#include "LGCPlugin.hpp"

#include <Logger.hpp>
#include <interface/SPluginLoader.hpp>
#include <utils/ClipboardManager.hpp>

#include "Version.hpp"
#include "entrypoints/EPplugin_lexers.hpp"
#include "tabs/inputs/LGCInputLexerIO.hpp"
#include "tabs/outputs/LGCOutputResLexer.hpp"
#include "trinity/LGCConfig.hpp"
#include "trinity/LGCGraphicalWidget.hpp"
#include "trinity/LGCLaunchObject.hpp"

class LGCPlugin::_LGCPlugin_pimpl
{
public:
};

LGCPlugin::LGCPlugin(QObject *parent) : QObject(parent), SPluginInterface(), _pimpl(std::make_unique<_LGCPlugin_pimpl>())
{
}

LGCPlugin::~LGCPlugin() = default;

QIcon LGCPlugin::icon() const
{
	return QIcon(":/icons/lgc");
}

void LGCPlugin::init()
{
	ClipboardManager::getClipboardManager().add(std::pair{new LGCInputLexerIO(), false}, std::pair{new LGCOutputResLexer(), false});
	// Register entry points
	SPluginLoader::getPluginLoader().registerEntryPoint("plugin_lexers", {&EPplugin_lexers::staticMetaObject, this});
	logDebug() << "Plugin Exp_Plugin loaded";
}

void LGCPlugin::terminate()
{
	logDebug() << "Plugin Exp_Plugin unloaded";
}

SConfigWidget *LGCPlugin::configInterface(QWidget *parent)
{
	logDebug() << "loading LGCPlugin::configInterface (LGCConfig)";
	return new LGCConfig(this, parent);
}

SGraphicalWidget *LGCPlugin::graphicalInterface(QWidget *parent)
{
	logDebug() << "loading LGClugin::graphicalInterface (LGCGraphicalWidget)";
	return new LGCGraphicalWidget(this, parent);
}

SLaunchObject *LGCPlugin::launchInterface(QObject *parent)
{
	logDebug() << "loading LGCPlugin::launchInterface (LGCLaunchObject)";
	return new LGCLaunchObject(this, parent);
}

const QString &LGCPlugin::_name()
{
	static const QString _sname = QString::fromStdString(getPluginName());
	return _sname;
}

const QString &LGCPlugin::_baseMimetype()
{
	static const QString _sbaseMimetype = "application/vnd.cern.susoft.surveypad.plugin.lgc";
	return _sbaseMimetype;
}
