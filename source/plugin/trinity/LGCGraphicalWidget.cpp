#include "LGCGraphicalWidget.hpp"

#include <vector>

#include <QCoreApplication>
#include <QDateTime>
#include <QFileInfo>
#include <QTabWidget>

#include <Logger.hpp>
#include <QtStreams.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <editors/web/WebEditorWidget.hpp>
#include <interface/SPluginLoader.hpp>
#include <io/LogLexer.hpp>
#include <utils/StylesHelper.hpp>

#include "LGCMeasures.hpp"
#include "LGCPlugin.hpp"
#include "tabs/inputs/LGCInputText.hpp"
#include "tabs/outputs/LGCOutputCooText.hpp"
#include "tabs/outputs/LGCOutputErrText.hpp"
#include "tabs/outputs/LGCOutputLogLexer.hpp"
#include "tabs/outputs/LGCOutputResText.hpp"

class LGCGraphicalWidget::_LGCGraphicalWidget_pimpl
{
public:
	/** input tab */
	LGCInputText *input = nullptr;
	/** Res output file*/
	LGCOutputResText *res = nullptr;
	/** Log output file */
	TextEditorWidget *log = nullptr;
	/** Log2 output file */
	TextEditorWidget *log2 = nullptr;

	/** output connections */
	std::vector<TextEditorWidget *> outputs;

	// Entry-point related
	/** Action coming from `plugin_uploadresults` entry point that on click should redirect to geode-plugin with proper upload website. */
	QAction *actionGeodeUpload = nullptr;

	static const QString htmlReportBase;
};

const QString LGCGraphicalWidget::_LGCGraphicalWidget_pimpl::htmlReportBase = QCoreApplication::applicationDirPath() + "/LGC_Report.html";

LGCGraphicalWidget::LGCGraphicalWidget(SPluginInterface *owner, QWidget *parent) :
	TabEditorScriptWidget(owner, parent), _pimpl(std::make_unique<_LGCGraphicalWidget_pimpl>())
{
	sameDescriptionMode(false);

	_pimpl->input = new LGCInputText(owner, this);
	_pimpl->input->setWindowTitle(tr("LGC file"));
	connect(_pimpl->input, &SGraphicalWidget::hasChanged, this, &LGCGraphicalWidget::inputHasChanged);
	tab()->addTab(_pimpl->input, _pimpl->input->windowTitle());

	setupEntryPoints();
	setupScripts();
}

LGCGraphicalWidget::~LGCGraphicalWidget() = default;

QString LGCGraphicalWidget::getPath() const
{
	return _pimpl->input->getPath();
}

void LGCGraphicalWidget::runFinished(bool status)
{
	generateHTMLReport();
	openOutputs(!status);
	TabEditorScriptWidget::runFinished(status);

	// After it finishes the execution it shows the tab "Result"
	int resIdx = -1;
	if (_pimpl->res)
		resIdx = tab()->indexOf(_pimpl->res);
	if (resIdx != -1)
	{
		tab()->setCurrentIndex(resIdx);
		return;
	}

	// if there is no res file something there was an error and instead we try to change to the log file
	if (_pimpl->log)
		resIdx = tab()->indexOf(_pimpl->log);
	if (resIdx != -1)
		tab()->setCurrentIndex(resIdx);
}

bool LGCGraphicalWidget::_save(const QString &path)
{
	bool b = _pimpl->input->save(path);
	setWindowTitle(QFileInfo(path).baseName());
	tab()->setTabToolTip(0, path);
	return b;
}

bool LGCGraphicalWidget::_open(const QString &path)
{
	bool b = _pimpl->input->open(path);
	openOutputs();
	setWindowTitle(QFileInfo(path).baseName());
	tab()->setTabToolTip(0, path);
	return b;
}

void LGCGraphicalWidget::_newEmpty()
{
	removeOutputs();
	_pimpl->input->newEmpty();
	setWindowTitle(tr("New Project"));
	tab()->setTabToolTip(0, "");
}

SGraphicalWidget *LGCGraphicalWidget::openOutputFile(SGraphicalWidget *widget, const QString &file, bool checkForOpened)
{
	if (STabInterface::openOutputFile(widget, file, checkForOpened))
	{
		auto *textWidget = qobject_cast<TextEditorWidget *>(widget);
		if (textWidget)
		{
			setBackground(textWidget->editor(), getThemeColor(QColor(240, 240, 240)));
			_pimpl->outputs.push_back(textWidget);
			textWidget->editor()->setReadOnly(true);
		}
		return widget;
	}

	delete widget;
	return nullptr;
}

void LGCGraphicalWidget::openOutputs(bool onlyLog)
{
	QFileInfo fi(getPath());

	auto openOutputfile = [this, &fi](const char *type, const char *ext, SGraphicalWidget *w = nullptr) -> SGraphicalWidget * {
		if (!w)
			w = new TextEditorWidget(owner(), this);
		w->setWindowTitle(QString("%1 (%2)").arg(type, ext));
		return openOutputFile(w, fi.canonicalPath() + '/' + fi.completeBaseName() + ext);
	};

	removeOutputs();

	LGCOutputCooText *cooWidget = nullptr;
	LGCOutputErrText *errWidget = nullptr;
	if (!onlyLog)
	{
		// .html report
		openOutputfile("REPORT", ".html", new WebEditorWidget(owner(), this));
		// result
		_pimpl->res = new LGCOutputResText(owner(), this);
		_pimpl->res->setWindowTitle(tr("Results (.res)"));
		connect(_pimpl->res->editor(), &TextEditor::globalIndicatorClicked, this, &LGCGraphicalWidget::globalIndicatorClicked);
		_pimpl->res->lgcMeasures(_pimpl->input->lgcMeasures());
		if (!openOutputFile(_pimpl->res, fi.canonicalPath() + '/' + fi.completeBaseName() + ".res"))
			_pimpl->res = nullptr;
		// others, just try to open them all, only the existing ones more recent than the input will be open
		// .cov
		openOutputfile("COVAR", ".cov");
		// .def
		openOutputfile("DEFA", ".def");
		// .err
		errWidget = new LGCOutputErrText(owner(), this);
		errWidget->setWindowTitle(tr("FAUT (.err)"));
		connect(errWidget->editor(), &TextEditor::globalIndicatorClicked, this, &LGCGraphicalWidget::globalIndicatorClicked);
		errWidget->lgcMeasures(_pimpl->input->lgcMeasures());
		if (!openOutputFile(errWidget, fi.canonicalPath() + '/' + fi.completeBaseName() + ".err"))
			errWidget = nullptr;
		// .coo
		cooWidget = new LGCOutputCooText(owner(), this);
		cooWidget->setWindowTitle(tr("PUNC (.coo)"));
		connect(cooWidget->editor(), &TextEditor::globalIndicatorClicked, this, &LGCGraphicalWidget::globalIndicatorClicked);
		cooWidget->lgcMeasures(_pimpl->input->lgcMeasures());
		if (openOutputFile(cooWidget, fi.canonicalPath() + '/' + fi.completeBaseName() + ".coo") && _pimpl->actionGeodeUpload)
			cooWidget->toolbar()->addAction(_pimpl->actionGeodeUpload);
		// .sim
		openOutputfile("SOBS", ".sim");
		// .chabaOut
		openOutputfile("CHABA", ".chabaOut");
		// .json
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
		if (openOutputfile("JSON", ".json")) // hide json tab if present, the widget and its path(/extension) can still be found thanks to this trick
			tab()->setTabVisible(tab()->count() - 1, false);
#else
		openOutputfile("JSON", ".json");
#endif
	}
	if (_pimpl->actionGeodeUpload)
		_pimpl->actionGeodeUpload->setEnabled(cooWidget);

	// log
	try
	{
		// .log
		auto logWidget = openOutputfile("Log", ".log");
		if (logWidget)
		{
			auto *textLogWidget = qobject_cast<TextEditorWidget *>(logWidget);
			if (textLogWidget)
			{
				textLogWidget->editor()->setLexer(new LGCOutputLogLexer(this));
				connect(textLogWidget->editor(), &TextEditor::globalIndicatorClicked, this, &LGCGraphicalWidget::globalIndicatorClicked);
			}
			_pimpl->log = textLogWidget;
		}
		// .log2
		_pimpl->log2 = new TextEditorWidget(owner(), this);
		_pimpl->log2->setWindowTitle(tr("Log2 (.log2)"));
		_pimpl->log2->editor()->setLexer(new LogLexer(this));
		_pimpl->log2 = qobject_cast<TextEditorWidget *>(openOutputFile(_pimpl->log2, fi.canonicalPath() + '/' + fi.completeBaseName() + ".log2"));
	}
	catch (...)
	{
	}

	restoreTabsAfterRun();
}

void LGCGraphicalWidget::removeOutputs()
{
	QWidget *w = nullptr;

	_pimpl->outputs.clear();
	tab()->setCurrentIndex(0);
	while (w = tab()->widget(1))
		delete w;
	_pimpl->res = nullptr;
	_pimpl->log = nullptr;
	_pimpl->log2 = nullptr;
}

void LGCGraphicalWidget::setupEntryPoints()
{
	// plugin_uploadresults
	// Check for GEODE upload results entry point
	auto EPuploadresults = SPluginLoader::getPluginLoader().getEntryPoints("plugin_uploadresults");
	if (EPuploadresults.empty())
		return;
	// Vector should contain just one element (as Geode is the only one that registers under this entrypoint name)
	auto instanceEP = std::unique_ptr<QObject>(EPuploadresults[0].meta->newInstance());
	if (!instanceEP)
		return;

	std::function<void(const QString &, const QString &lgc)> uploadFunction;
	bool callGetAction = QMetaObject::invokeMethod(instanceEP.get(), "getUploadAction", Q_RETURN_ARG(QAction *, _pimpl->actionGeodeUpload), Q_ARG(QObject *, this));
	bool callGetFunction = QMetaObject::invokeMethod(instanceEP.get(), "getUploadFunction", Q_RETURN_ARG(std::function<void(const QString &, const QString &)>, uploadFunction));
	if (callGetAction && _pimpl->actionGeodeUpload && callGetFunction && uploadFunction)
	{
		addAction(Menus::plugin, _pimpl->actionGeodeUpload);
		// Passing the path to .coo file and to .lgc file
		connect(_pimpl->actionGeodeUpload, &QAction::triggered, [uploadFunction, this]() -> void {
			QFileInfo filePath(getPath());
			uploadFunction(filePath.absolutePath() + "/" + filePath.baseName() + ".coo", filePath.absoluteFilePath());
		});
	}
}

void LGCGraphicalWidget::generateHTMLReport()
{
	// Get Report file contents
	QFile reportFile(_pimpl->htmlReportBase);
	if (!reportFile.exists())
	{
		logWarning() << "LGC Base Report file does not exist in SurveyPad installation path!";
		return;
	}
	if (!reportFile.open(QIODevice::ReadOnly))
	{
		logWarning() << "LGC Base Report file cannot be opened!";
		return;
	}
	const char *codec = "UTF-8";
	QTextStream reportTS(&reportFile);
	reportTS.setCodec(codec);
	QString reportFileContents = reportTS.readAll();

	// Get JSON data contents
	QFileInfo fiProject(getPath());
	const QString jsonPath = fiProject.canonicalPath() + '/' + fiProject.completeBaseName() + ".json";
	QFile jsonFile(jsonPath);
	if (!jsonFile.exists())
	{
		logWarning() << "LGC JSON file does not exist!";
		return;
	}
	if (!jsonFile.open(QIODevice::ReadOnly))
	{
		logWarning() << "LGC JSON file cannot be opened!";
		return;
	}
	QTextStream jsonTS(&jsonFile);
	jsonTS.setCodec(codec);
	QString jsonContents = jsonTS.readAll();

	// Replace input file placeholder
	reportFileContents.replace(R"("linkPathPlaceholder")", QString("\"%1\"").arg(fiProject.absoluteFilePath()));
	// Replace the Report JSON contents with LGC data
	reportFileContents.replace(R"({"PLACEHOLDER_JSON":""})", jsonContents);

	// Save the newly created report to file
	QFile reportOutFile(fiProject.canonicalPath() + '/' + fiProject.completeBaseName() + ".html");
	if (reportOutFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
	{
		QTextStream stream(&reportOutFile);
		stream.setCodec(codec);
		stream << reportFileContents;
		logInfo() << "LGC Report file was generated!";
	}
}

void LGCGraphicalWidget::globalIndicatorClicked(const QString &, int indicator)
{
	const auto &measures = _pimpl->input->lgcMeasures();
	LGCMeasures::Hash hash(indicator);
	const LGCMeasures::MeasureMap *map = nullptr;

	switch (LGCMeasures::LGCMeasuresEnum(hash.hashFields.enumMap))
	{
		// default LGCMeasures::LGCMeasuresEnum::direct ignored here
	case LGCMeasures::LGCMeasuresEnum::points:
		map = &measures.points;
		break;
	case LGCMeasures::LGCMeasuresEnum::frames:
		map = &measures.frames;
		break;
	case LGCMeasures::LGCMeasuresEnum::instruments:
		map = &measures.instruments;
		break;
	case LGCMeasures::LGCMeasuresEnum::targets:
		map = &measures.targets;
		break;
	case LGCMeasures::LGCMeasuresEnum::measures:
		map = &measures.measures;
		break;
	}

	int position = hash.hashFields.key; // position for default LGCMeasures::LGCMeasuresEnum::direct case
	if (map)
	{
		int key = hash.hashFields.key;
		if (!map->count(key))
			return;
		position = map->at(key).position;
	}

	tab()->setCurrentIndex(0);
	_pimpl->input->editor()->jumpTo(position, hash.hashFields.isLine);
}

void LGCGraphicalWidget::inputHasChanged()
{
	if (!isModified())
		return;
	for (auto &o : _pimpl->outputs)
		o->inputHasChanged(true);
}
