#include "LGCLaunchObject.hpp"

#include <QProcess>

#include <ShareablePoints/SPIOException.hpp>
#include <utils/SettingsManager.hpp>

#include "LGCPlugin.hpp"
#include "trinity/LGCConfig.hpp"

QString LGCLaunchObject::exepath() const
{
	auto conf = SettingsManager::settings().settings<LGCConfigObject>(LGCPlugin::_name());
	return conf.lgcPath;
}

void LGCLaunchObject::launch(const QString &file)
{
	if (file.isEmpty())
	{
		emit finished(false);
		throw SPIOException("Error: no LGC project file given. Should you save your project?");
	}

	auto conf = SettingsManager::settings().settings<LGCConfigObject>(LGCPlugin::_name());
	process().setProgram(conf.lgcPath);
	QStringList arguments = {"-i", file, "-n", QString::number(conf.nbIter)};
	if (conf.useDebug)
		arguments.append("-d");
	process().setArguments(arguments);
	ProcessLauncherObject::launch(file);
}
