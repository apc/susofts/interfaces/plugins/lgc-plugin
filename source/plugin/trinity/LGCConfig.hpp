/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef LGCCONFIG_HPP
#define LGCCONFIG_HPP

#include <memory>

#include <interface/SConfigInterface.hpp>

namespace Ui
{
class LGCConfig;
}

struct LGCConfigObject : public SConfigObject
{
	virtual ~LGCConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	/** Path to LGC executable */
	const QString lgcVersion = "2.6.1"; // Cannot be modified by a plugin user
	QString lgcPath = "C:/Program Files/SUSoft/LGC/" + lgcVersion + "/LGC.exe";
	/** Use debug flag for LGC */
	bool useDebug = false;
	/** Number of iterations */
	unsigned int nbIter = 80;
	/** enable syntax highlighting */
	bool color = true;
	/** Point sigma threshold */
	double threshPSigma = 5;
	/** Point coordinate threshold */
	double threshPDisplacement = 5;
	/** Measure RES/SIG threshold */
	double threshMSigma = 5;
	/** Input editor update timer */
	double inputUpdateTime = 0;
	/** Toggle sigma precision value */
	double inputToggleSigmaPrecision = 1000;
	/** Toggle constant angle value */
	double inputToggleConstantAngle = 100;
};

class LGCConfig : public SConfigWidget
{
	Q_OBJECT

public:
	LGCConfig(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~LGCConfig();

	// SConfigInterface
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private slots:
	void selectLGCPath();
	void updateCommand();

private:
	std::unique_ptr<Ui::LGCConfig> ui;
};

#endif // LGCCONFIG_HPP
