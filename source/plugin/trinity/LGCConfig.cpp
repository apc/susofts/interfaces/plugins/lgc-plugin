#include "LGCConfig.hpp"
#include "ui_LGCConfig.h"

#include <QDir>
#include <QFileDialog>
#include <QSettings>

#include "LGCPlugin.hpp"
#include "Version.hpp"

bool LGCConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const LGCConfigObject &oc = static_cast<const LGCConfigObject &>(o);
	return lgcPath == oc.lgcPath && nbIter == oc.nbIter && useDebug == oc.useDebug && color == oc.color && threshPSigma == oc.threshPSigma && threshPDisplacement == oc.threshPDisplacement && threshMSigma == oc.threshMSigma && inputUpdateTime == oc.inputUpdateTime
		&& inputToggleSigmaPrecision == oc.inputToggleSigmaPrecision && inputToggleConstantAngle == oc.inputToggleConstantAngle;
}

void LGCConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(LGCPlugin::_name());

	settings.setValue("lgcPath_v" + lgcVersion, lgcPath);
	settings.setValue("nbIter", nbIter);
	settings.setValue("useDebug", useDebug);
	settings.setValue("color", color);
	settings.setValue("threshPSigma", threshPSigma);
	settings.setValue("threshPDisplacement", threshPDisplacement);
	settings.setValue("threshMSigma", threshMSigma);
	settings.setValue("inputUpdateTime", inputUpdateTime);
	settings.setValue("inputToggleSigmaPrecision", inputToggleSigmaPrecision);
	settings.setValue("inputToggleConstantAngle", inputToggleConstantAngle);

	settings.endGroup();
}

void LGCConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(LGCPlugin::_name());

	lgcPath = settings.value("lgcPath_v" + lgcVersion, "C:/Program Files/SUSoft/LGC/" + lgcVersion + "/LGC.exe").toString();
	nbIter = settings.value("nbIter", 80).toUInt();
	useDebug = settings.value("useDebug", false).toBool();
	color = settings.value("color", true).toBool();
	threshPSigma = settings.value("threshPSigma", 5).toDouble();
	threshPDisplacement = settings.value("threshPDisplacement", 5).toDouble();
	threshMSigma = settings.value("threshMSigma", 5).toDouble();
	inputUpdateTime = settings.value("inputUpdateTime", 0).toDouble();
	inputToggleSigmaPrecision = settings.value("inputToggleSigmaPrecision", 1000).toDouble();
	inputToggleConstantAngle = settings.value("inputToggleConstantAngle", 100).toDouble();

	settings.endGroup();
}

LGCConfig::LGCConfig(SPluginInterface *owner, QWidget *parent) : SConfigWidget(owner, parent), ui(std::make_unique<Ui::LGCConfig>())
{
	ui->setupUi(this);
	ui->lblVersion->setText(ui->lblVersion->text().arg(QString::fromStdString(getVersion())));
}

LGCConfig::~LGCConfig() = default;

SConfigObject *LGCConfig::config() const
{
	auto *tmp = new LGCConfigObject();
	tmp->lgcPath = QDir::fromNativeSeparators(ui->lineExePath->text());
	tmp->nbIter = (unsigned int)ui->nbIter->value();
	tmp->useDebug = ui->useDebug->isChecked();
	tmp->color = ui->colorCheck->isChecked();
	tmp->threshPSigma = ui->pointSigmaThreshold->value() == 0 ? -1 : ui->pointSigmaThreshold->value();
	tmp->threshPDisplacement = ui->pointDisplacementThreshold->value() == 0 ? -1 : ui->pointDisplacementThreshold->value();
	tmp->threshMSigma = ui->measureResSigThershold->value() == 0 ? -1 : ui->measureResSigThershold->value();
	tmp->inputUpdateTime = ui->inputUpdateTime->value();
	tmp->inputToggleSigmaPrecision = ui->inputToggleSigmaPrecision->value();
	tmp->inputToggleConstantAngle = ui->inputToggleConstantAngle->value();
	return tmp;
}

void LGCConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const LGCConfigObject *>(conf);
	if (config)
	{
		ui->lineExePath->setText(QDir::toNativeSeparators(config->lgcPath));
		ui->nbIter->setValue(config->nbIter);
		ui->useDebug->setChecked(config->useDebug);
		ui->colorCheck->setChecked(config->color);
		ui->pointSigmaThreshold->setValue(config->threshPSigma < 0 ? 0 : config->threshPSigma);
		ui->pointDisplacementThreshold->setValue(config->threshPDisplacement < 0 ? 0 : config->threshPDisplacement);
		ui->measureResSigThershold->setValue(config->threshMSigma < 0 ? 0 : config->threshMSigma);
		ui->inputUpdateTime->setValue(config->inputUpdateTime);
		ui->inputToggleSigmaPrecision->setValue(config->inputToggleSigmaPrecision);
		ui->inputToggleConstantAngle->setValue(config->inputToggleConstantAngle);

		// handle groupboxes
		ui->groupBoxInput->setEnabled(config->color);
		ui->groupBoxResults->setEnabled(config->color);
	}
}

void LGCConfig::reset()
{
	LGCConfigObject config;
	config.read();
	setConfig(&config);
}

void LGCConfig::restoreDefaults()
{
	LGCConfigObject config;
	setConfig(&config);
}

void LGCConfig::selectLGCPath()
{
	QString filename = QFileDialog::getOpenFileName(this, tr("LGC executable file"), ui->lineExePath->text(), tr("LGC.exe (LGC.exe);;Executable (*.exe);;Any (*.*)"));
	if (!filename.isEmpty())
		ui->lineExePath->setText(QDir::toNativeSeparators(filename));
}

void LGCConfig::updateCommand()
{
	ui->lblcmd->setText(tr("lgc.exe -i $FILE -n %1 %2").arg(ui->nbIter->value()).arg(ui->useDebug->isChecked() ? "-d" : ""));
}
