/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef LGCLAUNCHOBJECT_HPP
#define LGCLAUNCHOBJECT_HPP

#include <interface/ProcessLauncherObject.hpp>

class LGCLaunchObject : public ProcessLauncherObject
{
	Q_OBJECT

public:
	LGCLaunchObject(SPluginInterface *owner, QObject *parent = nullptr) : ProcessLauncherObject(owner, parent) {}
	virtual ~LGCLaunchObject() override = default;

	// SLaunchObject
	virtual QString exepath() const override;

public slots:
	// ProcessLauncherObject
	virtual void launch(const QString& file) override;
};

#endif // LGCLAUNCHOBJECT_HPP
