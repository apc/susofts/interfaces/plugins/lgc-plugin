#ifndef LGCCOOLEXERIO_HPP
#define LGCCOOLEXERIO_HPP

#include <io/CooLexerIO.hpp>

#include "LGCMeasures.hpp"

class LGCOutputCooLexerIO : public CooLexerIO
{
	Q_OBJECT

public:
	LGCOutputCooLexerIO(QObject *parent = nullptr) : CooLexerIO(parent) {}
	virtual ~LGCOutputCooLexerIO() override = default;

	/**
	 * Uses the input measures to create links between input and output file.
	 *
	 * @warning This method must be called before opening the output file.
	 */
	void lgcMeasures(LGCMeasures measures) noexcept
	{
		_inputData = std::move(measures);
		_inputData.instruments.clear();
		_inputData.measures.clear();
		_inputData.targets.clear();
	}
	const LGCMeasures &lgcMeasures() const noexcept { return _inputData; }

protected:
	// TextLexer
	virtual QString calltip(int position, int indicatorType, const QString &indicator) const override;

	// CooLexerIO
	virtual void gotPointName(QStringRef sym) override;
	virtual void gotFrameName(QStringRef sym) override;

private:
	/** input measures */
	LGCMeasures _inputData;
};

#endif // LGCCOOLEXERIO_HPP
