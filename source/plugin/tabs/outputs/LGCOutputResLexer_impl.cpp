#include <Qsci/qsciscintilla.h>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <StringManager.h>
#include <utils/SettingsManager.hpp>
#include <utils/StylesHelper.hpp>

#include "LGCOutputResLexer.hpp"
#include "LGCPlugin.hpp"
#include "trinity/LGCConfig.hpp"

LGCOutputResLexer::LGCOutputResLexer(QObject *parent) : LexerParser(parent)
{
}

const std::string &LGCOutputResLexer::getMIMEType() const
{
	static const std::string mime = LGCPlugin::_baseMimetype().toStdString() + ".pointResRAW";
	return mime;
}

const char *LGCOutputResLexer::keywords(int) const
{
	return nullptr;
}

QString LGCOutputResLexer::description(int style) const
{
	switch (style)
	{
	case Styles::Default:
		return "Default";
	case Styles::Comment:
		return "Comment";
	case Styles::Normal:
		return "Normal";
		// Values
	case Styles::ValueImportant:
		return "ValueImportant";
	case Styles::ValueCorrect:
		return "ValueCorrect";
	case Styles::ValueWarning:
		return "ValueWarning";
	case Styles::ValueError:
		return "ValueError";
		// keywords
	case Styles::Section:
		return "Section";
		// Options
	case Styles::DatumOption:
		return "Datum option";
	case Styles::OptionParameter:
		return "Option parameter";
		// Intruments
	case Styles::InstrumentType:
		return "Instrument type";
	case Styles::InstrumentName:
		return "Instrument name";
	case Styles::TargetName:
		return "Target name";
		// Points
	case Styles::PointType:
		return "Point type";
	case Styles::PointName:
		return "Point name";
	case Styles::PointCoord:
		return "Point coordinates";
	case Styles::PointSigma:
		return "Point sigma";
		// measures
	case Styles::MeasureType:
		return "Measure type";
		// Frames
	case Styles::Frame:
		return "Frame";
	case Styles::FrameName:
		return "Frame Name";
	}

	return QString();
}

const char *LGCOutputResLexer::wordCharacters() const
{
	return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-+*[]()<>";
}

bool LGCOutputResLexer::defaultEolFill(int) const
{
	return false;
}

QColor LGCOutputResLexer::defaultColor(int style) const
{
	switch (style)
	{
	case Styles::Default:
		return Qt::GlobalColor::darkGray;
	case Styles::Comment:
		return Qt::GlobalColor::darkGreen;
	case Styles::Normal:
		return getThemeColor(QColor(0, 43, 54, 255)); // almost black
		// Values
	case Styles::ValueImportant:
		return Qt::GlobalColor::blue;
	case Styles::ValueCorrect:
		return QColor(51, 102, 0); // green
	case Styles::ValueWarning:
		return Qt::GlobalColor::yellow;
	case Styles::ValueError:
		return Qt::GlobalColor::red;
		// keywords
	case Styles::Section:
		return QColor(220, 50, 47, 255); // red orange
	case Styles::PointType:
		return QColor(181, 137, 0, 255); // brown
	case Styles::MeasureType:
		return QColor(203, 75, 22, 255); // orange
	// Options
	case Styles::DatumOption:
		return Qt::GlobalColor::darkBlue;
	case Styles::OptionParameter:
		return Qt::GlobalColor::black;
		// Intruments
	case Styles::InstrumentType:
	case Styles::InstrumentName:
	case Styles::TargetName:
		return QColor(108, 113, 196, 255); // turquoise
	// Points
	case Styles::PointName:
		return Qt::GlobalColor::darkCyan;
	case Styles::PointCoord:
		return Qt::GlobalColor::magenta;
	case Styles::PointSigma:
		return Qt::GlobalColor::darkMagenta;
	// Frames
	case Styles::Frame:
		return Qt::GlobalColor::darkRed;
	case Styles::FrameName:
		return QColor(182, 8, 82, 255); // Jazzberry Jam
	}

	return ShareablePointsListIOLexer::defaultColor(style);
}

QFont LGCOutputResLexer::defaultFont(int style) const
{
	QFont baseFont("Courier New", 10);
	baseFont.setStyleHint(QFont::StyleHint::Monospace);
	baseFont.setWeight(QFont::Weight::Light);

	switch (style)
	{
	case Styles::Default:
	case Styles::Normal:
	case Styles::InstrumentName:
	case Styles::PointCoord:
	case Styles::PointSigma:
		return baseFont;
	case Styles::Comment:
	case Styles::TargetName:
		baseFont.setItalic(true);
		return baseFont;
	case Styles::DatumOption:
		baseFont.setItalic(true);
		[[fallthrough]];
	case Styles::ValueImportant:
	case Styles::ValueCorrect:
	case Styles::ValueWarning:
	case Styles::ValueError:
	case Styles::Section:
	case Styles::OptionParameter:
	case Styles::InstrumentType:
	case Styles::PointName:
	case Styles::PointType:
	case Styles::MeasureType:
	case Styles::Frame:
	case Styles::FrameName:
		baseFont.setWeight(QFont::Weight::Black);
		return baseFont;
	}

	return ShareablePointsListIOLexer::defaultFont(style);
}

QColor LGCOutputResLexer::defaultPaper(int style) const
{
	return ShareablePointsListIOLexer::defaultPaper(style);
}

ShareablePointsList LGCOutputResLexer::read(const std::string &contents)
{
	ShareablePointsList spl;
	_curFrame = &spl.getRootFrame();

	parseString(contents);

	return std::move(spl);
}

ShareableExtraInfos LGCOutputResLexer::readExtraInfos(const std::string &)
{
	return ShareableExtraInfos();
}

ShareableFrame LGCOutputResLexer::readFrame(const std::string &contents)
{
	ShareablePointsList spl;
	_curFrame = &spl.getRootFrame();

	std::string toparse = ltrim(contents);
	if (toparse.size() == 0 || (toparse.rfind("POINTS DE", 0) != 0 && toparse.rfind("POINTS VARIABLES EN", 0) != 0 && toparse.rfind("POINTS INVARIABLES EN", 0) != 0))
		toparse = std::string("POINTS VARIABLES EN XYZ   (NB. = 0,  REFERENTIEL = ") + (_isOloc ? "OLOC" : "RS2K") + " )\nSFP A\nA\n" + toparse;
	if (toparse.size() == 0 || toparse.rfind("SFP = Sub-Frame Point; * = TRUE") == std::string::npos)
		toparse += "\nSFP = Sub-Frame Point; * = TRUE\n";

	toparse = R"""(

**

CALCUL DU LOL
**
A :
*** STATISTIQUES ***
A = 12
A = 12
A = 12
A = 12
SIGMA ZERO A POSTERIORI = 12.5 a a a = (12.4, 12.5)
LES ECARTS-TYPES SONT CALCULES PAR RAPPORT AU SIGMA ZERO A POSTERIORI

FRAME	ROOT  ID(1)   

)""" + toparse
		+ R"""(
*** RESUME DES MESURES ***
ANGL
A
A
LHC.GGPSO.1L804.            2.00     -1.91     -0.00      1.56
A = 12 A : a = a
A = 12 A : a = a
*** MESURES ***
DVER
A
A
A
A A 12 12 12 12 12 12
*** FIN DE FICHIER ***
		)"""
		+ '\n';
	parseString(toparse);

	return std::move(spl.getRootFrame());
}

ShareableParams LGCOutputResLexer::readParams(const std::string &)
{
	return ShareableParams();
}

ShareablePoint LGCOutputResLexer::readPoint(const std::string &contents)
{
	auto frame = readFrame(contents);
	if (frame.getPoints().empty())
		throw SPIOException("No point read", "", contents);
	auto &p = frame.getPoint(0);
	p.parent = nullptr;
	return std::move(p);
}

ShareablePosition LGCOutputResLexer::readPosition(const std::string &)
{
	return ShareablePosition();
}

int LGCOutputResLexer::indicatorJumpTo(int indicatorType, int position, const QString &indicatorText)
{
	if (!editor() || indicatorType != TextIndicator::INTERNAL)
		return LexerParser::indicatorJumpTo(indicatorType, position, indicatorText);

	int line, index;
	editor()->lineIndexFromPosition(position, &line, &index);
	const auto &key = _summarylineToMeasure.find(line);
	if (key == std::cend(_summarylineToMeasure) || find(_resData.instruments, key->second).isEmpty())
		return LexerParser::indicatorJumpTo(indicatorType, position, indicatorText);

	return _resData.instruments[key->second].position;
}

QString LGCOutputResLexer::calltip(int, int, const QString &indicator) const
{
	LGCMeasures::Value value;
	QString outCalltip{"%1\n"};

	if (contains(_inputData.instruments, indicator))
		value = _inputData.instruments.at(indicator);
	else if (contains(_inputData.frames, indicator))
		value = _inputData.frames.at(indicator);
	else if (contains(_inputData.points, indicator))
	{
		value = _inputData.points.at(indicator);
		outCalltip += value.getObservationsSummary();
	}
	else
	{
		QString target = find(_inputData.targets, indicator);
		if (target.isEmpty())
			return QString();
		value = _inputData.targets.at(target);
	}

	return outCalltip.arg(value.getSummary());
}

void LGCOutputResLexer::initLexer()
{
	LexerParser::initLexer();

	_contexts.push_back(Context::HEADER);
	_summarylineToMeasure.clear();
	_summaryMeasures.clear();
	_resData.clearAll();
	_isOloc = true;
	_tmpcounter = 0;
	_curTmp.clear();

	const auto config = SettingsManager::settings().settings<LGCConfigObject>(LGCPlugin::_name());
	_threshPSigma = config.threshPSigma;
	_threshPDisplacement = config.threshPDisplacement;
	_threshMSigma = config.threshMSigma;
}

void LGCOutputResLexer::endLexer()
{
	LexerParser::endLexer();

	_curTmp.clear();
	_summaryMeasures.clear();

	_threshPSigma = 5;
	_threshPDisplacement = 5;
	_threshMSigma = 5;
}
