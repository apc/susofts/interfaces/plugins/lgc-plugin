#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>

#include "LGCOutputErrLexer.hpp"

LGCOutputErrLexer::LGCOutputErrLexer(QObject *parent) : LexerParser(parent)
{
}

const char *LGCOutputErrLexer::keywords(int) const
{
	return nullptr;
}

QString LGCOutputErrLexer::description(int style) const
{
	switch (style)
	{
	/** Default */
	case Styles::Default:
		return "Default";
	case Styles::Normal:
		return "Normal";
	case Styles::Comment:
		return "Comment";
	/** Values */
	case Styles::ValueImportant:
		return "ValueImportant";
	case Styles::ValueError:
		return "ValueError";
	case Styles::ValueCorrect:
		return "ValueCorrect";
	/** Table */
	case Styles::TablePoint:
		return "Table Point";
	case Styles::TableHeader:
		return "Table Header";
	case Styles::TableHeaderUnits:
		return "Table Header Units";
	case Styles::TableFrameTransformation:
		return "Table Frame Transformation";
	/** Frames */
	case Styles::Frame:
		return "Frame";
	case Styles::FrameName:
		return "Frame Name";
	/** Overall Reliability */
	case Styles::OverallReliability:
		return "Overall Reliability";
	}

	return QString();
}

const char *LGCOutputErrLexer::wordCharacters() const
{
	return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-+*:/!éèà[]()";
}

bool LGCOutputErrLexer::defaultEolFill(int) const
{
	return false;
}

QColor LGCOutputErrLexer::defaultColor(int style) const
{
	switch (style)
	{
	/** Default */
	case Styles::Default:
		return Qt::GlobalColor::darkGray;
	case Styles::Comment:
		return Qt::GlobalColor::darkGreen;
	case Styles::Normal:
		return QColor(0, 43, 54, 255); // almost black
	/** Values */
	case Styles::ValueImportant:
		return Qt::GlobalColor::blue;
	case Styles::ValueError:
		return Qt::GlobalColor::red;
	case Styles::ValueCorrect:
		return QColor(51, 102, 0); // green
	/** Table */
	case Styles::TablePoint:
		return Qt::GlobalColor::darkCyan;
	case Styles::TableHeader:
	case Styles::TableHeaderUnits:
		return QColor(195, 90, 155); // purplish
	case Styles::TableFrameTransformation:
		return QColor(99, 104, 221); // bluish
	/** Frames */
	case Styles::Frame:
		return Qt::GlobalColor::darkRed;
	case Styles::FrameName:
		return QColor(182, 8, 82, 255); // Jazzberry Jam
	/** Overall Reliability */
	case Styles::OverallReliability:
		return Qt::GlobalColor::darkMagenta;
	}

	return ShareablePointsListIOLexer::defaultColor(style);
}

QFont LGCOutputErrLexer::defaultFont(int style) const
{
	QFont baseFont("Courier New", 10);
	baseFont.setStyleHint(QFont::StyleHint::Monospace);
	baseFont.setWeight(QFont::Weight::Light);

	switch (style)
	{
	case Styles::Default:
	case Styles::Normal:
	case Styles::Comment:
	case Styles::TableHeaderUnits:
		return baseFont;
	case Styles::ValueImportant:
	case Styles::ValueError:
	case Styles::ValueCorrect:
	case Styles::TablePoint:
	case Styles::TableHeader:
	case Styles::TableFrameTransformation:
	case Styles::Frame:
	case Styles::FrameName:
	case Styles::OverallReliability:
		baseFont.setWeight(QFont::Weight::Black);
		return baseFont;
	}

	return ShareablePointsListIOLexer::defaultFont(style);
}

QColor LGCOutputErrLexer::defaultPaper(int style) const
{
	return ShareablePointsListIOLexer::defaultPaper(style);
}

ShareablePointsList LGCOutputErrLexer::read(const std::string &)
{
	return ShareablePointsList();
}

ShareableExtraInfos LGCOutputErrLexer::readExtraInfos(const std::string &)
{
	return ShareableExtraInfos();
}

ShareableFrame LGCOutputErrLexer::readFrame(const std::string &)
{
	return ShareableFrame(std::make_shared<ShareableParams>());
}

ShareableParams LGCOutputErrLexer::readParams(const std::string &)
{
	return ShareableParams();
}

ShareablePoint LGCOutputErrLexer::readPoint(const std::string &)
{
	return ShareablePoint();
}

ShareablePosition LGCOutputErrLexer::readPosition(const std::string &)
{
	return ShareablePosition();
}

QString LGCOutputErrLexer::calltip(int, int, const QString &indicator) const
{
	LGCMeasures::Value value;
	QString outCalltip{"%1\n"};

	if (contains(_inputData.frames, indicator))
		value = _inputData.frames.at(indicator);
	else if (contains(_inputData.points, indicator))
	{
		value = _inputData.points.at(indicator);
		outCalltip += value.getObservationsSummary();
	}
	else
		return QString();

	return outCalltip.arg(value.getSummary());
}

void LGCOutputErrLexer::setPoint(QStringRef sym)
{
	QString name = sym.trimmed().toString();
	if (contains(_inputData.points, name))
		addIndicationSym(TextIndicator::EXTERNAL, 1, LGCMeasures::encodeIndicator(name, LGCMeasures::LGCMeasuresEnum::points, false));
}

void LGCOutputErrLexer::setFrame(QStringRef sym)
{
	QString name = sym.trimmed().toString();
	if (contains(_inputData.frames, name))
		addIndicationSym(TextIndicator::EXTERNAL, 1, LGCMeasures::encodeIndicator(name, LGCMeasures::LGCMeasuresEnum::frames, false));
}
