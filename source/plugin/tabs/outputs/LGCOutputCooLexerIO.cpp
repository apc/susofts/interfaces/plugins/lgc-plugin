#include "LGCOutputCooLexerIO.hpp"

QString LGCOutputCooLexerIO::calltip(int, int, const QString &indicator) const
{
	if (!contains(_inputData.points, indicator))
		return QString();

	LGCMeasures::Value value = _inputData.points.at(indicator);
	return QString("%1\n%2").arg(value.getSummary(), value.getObservationsSummary());
}

void LGCOutputCooLexerIO::gotPointName(QStringRef sym)
{
	QString name = sym.trimmed().toString();
	if (contains(_inputData.points, name))
		addIndicationSym(TextIndicator::EXTERNAL, 1, LGCMeasures::encodeIndicator(name, LGCMeasures::LGCMeasuresEnum::points, false));
}

void LGCOutputCooLexerIO::gotFrameName(QStringRef sym)
{
	QString name = sym.trimmed().toString();
	if (contains(_inputData.frames, name))
		addIndicationSym(TextIndicator::EXTERNAL, 1, LGCMeasures::encodeIndicator(name, LGCMeasures::LGCMeasuresEnum::frames, false));
}
