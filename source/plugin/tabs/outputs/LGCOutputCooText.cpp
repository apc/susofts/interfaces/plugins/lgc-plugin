#include "LGCOutputCooText.hpp"

#include <QMimeData>

#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <utils/ClipboardManager.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/StylesHelper.hpp>

#include "LGCMeasures.hpp"
#include "LGCOutputCooLexerIO.hpp"
#include "LGCPlugin.hpp"
#include "trinity/LGCConfig.hpp"

LGCOutputCooText::LGCOutputCooText(SPluginInterface *owner, QWidget *parent) : TextEditorWidget(owner, parent)
{
	enablefileWatcher(true);
	installEventFilterAll(this, this);

	updateUi(owner->name());

	// setup editor
	editor()->setMimeTypesSupport(
		{clipboardMimeType().toStdString()}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });
}

void LGCOutputCooText::lgcMeasures(const LGCMeasures &measures)
{
	auto lex = qobject_cast<LGCOutputCooLexerIO *>(editor()->lexer());
	if (lex)
		lex->lgcMeasures(measures);
}

const LGCMeasures &LGCOutputCooText::lgcMeasures()
{
	static const LGCMeasures _m;
	auto lex = qobject_cast<LGCOutputCooLexerIO *>(editor()->lexer());
	return lex ? lex->lgcMeasures() : _m;
}

void LGCOutputCooText::updateUi(const QString &pluginName)
{
	TextEditorWidget::updateUi(pluginName);

	if (pluginName != LGCPlugin::_name())
		return;
	auto config = SettingsManager::settings().settings<LGCConfigObject>(pluginName);

	if (!config.color)
		editor()->setLexer(nullptr);
	else
	{
		if (config.color && !editor()->lexer())
			editor()->setLexer(new LGCOutputCooLexerIO(this));
		updateContent();
	}
	setBackground(editor(), getThemeColor(QColor(240, 240, 240)));
}

QString LGCOutputCooText::clipboardMimeType() const
{
	return QString::fromStdString(LGCOutputCooLexerIO().getMIMEType());
}

QByteArray LGCOutputCooText::fromMime(const QMimeData *mimedata)
{
	LGCOutputCooLexerIO io;
	io.utf8(editor()->isUtf8());
	return contentFromMime(mimedata, io);
}

QMimeData *LGCOutputCooText::toMime(const QByteArray &text, QMimeData *mimedata)
{
	LGCOutputCooLexerIO io;
	io.utf8(editor()->isUtf8());
	auto lex = qobject_cast<LGCOutputCooLexerIO *>(editor()->lexer());
	if (lex)
		io.columnsHeader(lex->columnsHeader());
	return contentToMime(text, mimedata, io);
}
