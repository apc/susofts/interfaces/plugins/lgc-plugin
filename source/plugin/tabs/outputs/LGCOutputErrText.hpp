/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef LGCOUTPUTERRTEXT_HPP
#define LGCOUTPUTERRTEXT_HPP

#include <editors/text/TextEditorWidget.hpp>

struct LGCMeasures;
class QMimeData;

/**
 * Text editor for output points in LGC.
 *
 * This class uses LGCOutputLexerRes.
 */
class LGCOutputErrText : public TextEditorWidget
{
	Q_OBJECT

public:
	LGCOutputErrText(SPluginInterface *owner = nullptr, QWidget *parent = nullptr);
	virtual ~LGCOutputErrText() override = default;

	// TextEditorWidget
	virtual bool isModified() const noexcept override { return false; }

	// LGCOutpuErrText
	/**
	 * Uses the input measures to create links between input and output file.
	 *
	 * @warning This method must be called before opening the output file.
	 */
	void lgcMeasures(const LGCMeasures &measures);
	const LGCMeasures &lgcMeasures();

public slots:
	// SGraphicalWidget
	virtual void updateUi(const QString &pluginName) override;

protected:
	// SGraphicalWidget
	virtual bool _open(const QString &path) override;

private:
	QString clipboardMimeType() const;
	QByteArray fromMime(const QMimeData *mimedata);
	QMimeData *toMime(const QByteArray &text, QMimeData *mimedata);
};

#endif // LGCOUTPUTERRTEXT_HPP
