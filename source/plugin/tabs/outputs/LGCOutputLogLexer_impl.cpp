#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>

#include "LGCOutputLogLexer.hpp"

LGCOutputLogLexer::LGCOutputLogLexer(QObject *parent) : LexerParser(parent)
{
}

const char *LGCOutputLogLexer::keywords(int) const
{
	return nullptr;
}

QString LGCOutputLogLexer::description(int style) const
{
	switch (style)
	{
	case Styles::Default:
		return "Default";
	case Styles::Error:
		return "Error";
	}

	return QString();
}

const char *LGCOutputLogLexer::wordCharacters() const
{
	return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-+*/![]()";
}

bool LGCOutputLogLexer::defaultEolFill(int) const
{
	return false;
}

QColor LGCOutputLogLexer::defaultColor(int style) const
{
	switch (style)
	{
	case Styles::Default:
		return Qt::GlobalColor::darkGray;
	case Styles::Error:
		return QColor(218, 68, 83);
	}

	return ShareablePointsListIOLexer::defaultColor(style);
}

QFont LGCOutputLogLexer::defaultFont(int style) const
{
	QFont baseFont("Courier New", 10);
	baseFont.setStyleHint(QFont::StyleHint::Monospace);
	baseFont.setWeight(QFont::Weight::Light);
	switch (style)
	{
	case Styles::Default:
		return baseFont;
	case Styles::Error:
		baseFont.setWeight(QFont::Weight::Black);
		return baseFont;
	}

	return ShareablePointsListIOLexer::defaultFont(style);
}

QColor LGCOutputLogLexer::defaultPaper(int style) const
{
	return ShareablePointsListIOLexer::defaultPaper(style);
}

ShareablePointsList LGCOutputLogLexer::read(const std::string &)
{
	return ShareablePointsList();
}

ShareableExtraInfos LGCOutputLogLexer::readExtraInfos(const std::string &)
{
	return ShareableExtraInfos();
}

ShareableFrame LGCOutputLogLexer::readFrame(const std::string &)
{
	return ShareableFrame(std::make_shared<ShareableParams>());
}

ShareableParams LGCOutputLogLexer::readParams(const std::string &)
{
	return ShareableParams();
}

ShareablePoint LGCOutputLogLexer::readPoint(const std::string &)
{
	return ShareablePoint();
}

ShareablePosition LGCOutputLogLexer::readPosition(const std::string &)
{
	return ShareablePosition();
}
