#include "LGCOutputErrText.hpp"

#include <QMimeData>

#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <utils/ClipboardManager.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/StylesHelper.hpp>

#include "LGCMeasures.hpp"
#include "LGCPlugin.hpp"
#include "tabs/outputs/LGCOutputErrLexer.hpp"
#include "trinity/LGCConfig.hpp"

LGCOutputErrText::LGCOutputErrText(SPluginInterface *owner, QWidget *parent) : TextEditorWidget(owner, parent)
{
	enablefileWatcher(true);
	installEventFilterAll(this, this);

	updateUi(owner->name());

	// setup editor
	editor()->setMimeTypesSupport(
		{clipboardMimeType().toStdString()}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });
}

void LGCOutputErrText::lgcMeasures(const LGCMeasures &measures)
{
	auto *lex = qobject_cast<LGCOutputErrLexer *>(editor()->lexer());
	if (lex)
		lex->lgcMeasures(measures);
}

const LGCMeasures &LGCOutputErrText::lgcMeasures()
{
	static const LGCMeasures _m;
	auto *lex = qobject_cast<LGCOutputErrLexer *>(editor()->lexer());
	return lex ? lex->lgcMeasures() : _m;
}

void LGCOutputErrText::updateUi(const QString &pluginName)
{
	TextEditorWidget::updateUi(pluginName);

	if (pluginName != LGCPlugin::_name())
		return;
	auto config = SettingsManager::settings().settings<LGCConfigObject>(pluginName);

	if (!config.color)
	{
		editor()->setLexer(nullptr);
		editor()->clearFolds();
	}
	else
	{
		if (config.color && !editor()->lexer())
			editor()->setLexer(new LGCOutputErrLexer(this));
		updateContent();
	}
	setBackground(editor(), getThemeColor(QColor(240, 240, 240)));
}

bool LGCOutputErrText::_open(const QString &path)
{
	bool retour = TextEditorWidget::_open(path);
	editor()->foldLine(1);
	editor()->foldLine(16);
	return retour;
}

QString LGCOutputErrText::clipboardMimeType() const
{
	return LGCPlugin::_baseMimetype() + ".outputpointRAW";
}

QByteArray LGCOutputErrText::fromMime(const QMimeData *mimedata)
{
	LGCOutputErrLexer io;
	io.utf8(editor()->isUtf8());
	return contentFromMime(mimedata, io);
}

QMimeData *LGCOutputErrText::toMime(const QByteArray &text, QMimeData *mimedata)
{
	LGCOutputErrLexer io;
	io.utf8(editor()->isUtf8());
	return contentToMime(text, mimedata, io);
}
