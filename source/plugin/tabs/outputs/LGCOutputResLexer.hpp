
/**
 * /!\ This file is automatically generated by QLALR from LGCOutputResLexer.g.
 * /!\ Don't edit it!
 */

#ifndef LGCOUTPUTRESLEXER_HPP
#define LGCOUTPUTRESLEXER_HPP

#include <unordered_set>

#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/text/LexerParser.hpp>

#include "LGCMeasures.hpp"

#include "table_lgcoutputreslexer_p.h"

class LGCPlugin;

class LGCOutputResLexer : public LexerParser<Table_LGCOutputResLexer>
{
	Q_OBJECT

public:
	LGCOutputResLexer(QObject *parent = nullptr);
	virtual ~LGCOutputResLexer() override = default;

	// QsciLexerCustom
	enum Styles
	{
		/** Default */
		Default,
		/** Comment / ignored part */
		Comment,
		/** not special stuff */
		Normal,
		// values
		ValueImportant,
		ValueCorrect,
		ValueWarning,
		ValueError,
		// keywords
		Section,
		/** Option keywords */
		DatumOption,
		OptionParameter,
		/** Intruments */
		InstrumentType,
		InstrumentName,
		TargetName,
		/** Points */
		PointType,
		PointName,
		PointCoord,
		PointSigma,
		/** Frames */
		Frame,
		FrameName,
		/** Measures (same as Options) */
		MeasureType,
		MeasureParameter = OptionParameter
	};
	// implemented in LGCOutputResLexer_impl.cpp
	virtual const char *language() const override { return "LGCPointRes"; }
	virtual const char *keywords(int set) const override;
	virtual QString description(int style) const override;
	virtual const char *wordCharacters() const override;
	virtual bool defaultEolFill(int style) const override;
	QColor defaultColor(int style) const override;
	QFont defaultFont(int style) const override;
	QColor defaultPaper(int style) const override;

	// IShareablePointsListIO
	// implemented in LGCOutputResLexer_impl.cpp
	virtual const std::string &getMIMEType() const override;
	virtual ShareablePointsList read(const std::string &contents) override;
	virtual ShareableExtraInfos readExtraInfos(const std::string &contents) override;
	virtual ShareableFrame readFrame(const std::string &contents) override;
	virtual ShareableParams readParams(const std::string &contents) override;
	virtual ShareablePoint readPoint(const std::string &contents) override;
	virtual ShareablePosition readPosition(const std::string &contents) override;
	virtual std::string write(const ShareablePointsList &) override { return std::string(); }
	virtual std::string write(const ShareableExtraInfos &) override { return std::string(); }
	virtual std::string write(const ShareableFrame &) override { return std::string(); }
	virtual std::string write(const ShareableParams &) override { return std::string(); }
	virtual std::string write(const ShareablePoint &) override { return std::string(); }
	virtual std::string write(const ShareablePosition &) override { return std::string(); }

	// LGCOutputResLexer
	/**
	 * Uses the input measures to create links between input and output file.
	 *
	 * @warning This method must be called before opening the output file.
	 */
	void lgcMeasures(LGCMeasures measures) noexcept { _inputData = std::move(measures); }
	const LGCMeasures &lgcMeasures() const noexcept { return _inputData; }
	
	bool isOloc() const noexcept { return _isOloc; }
	void isOloc(bool oloc) noexcept { _isOloc = oloc; }

protected:
	// TextLexer
	virtual int indicatorJumpTo(int indicatorType, int position, const QString &indicatorText) override;
	virtual QString calltip(int position, int indicatorType, const QString &indicator) const override;
	// LexerParser
	virtual void initLexer() override;
	virtual void endLexer() override;

private:
	// LexerParser
	// generated in in LGCInputLexerIO_gen.cpp
	int nextToken(QStringRef value) override;
	void consumeRule(int ruleno) override;

	// used in lexer
	void checkNumberInRange(int symidx, double min, double max, TextMarker marker, Styles styleOK = Styles::Normal, Styles styleError = Styles::ValueError, const QString &additionalMessage = "");
	QString findSummaryMeasureKey(const QString &instrType, const QString &pointName);
	QString setMeasure(const QString &type, const QString &measureName);
	void setFrame(QStringRef sym);
	void setFrameOrPointInSummary();

private:
	/** Inline contexts */
	enum InlineCxt
	{
		STATISTICS_POINT
	};
	/** Contexts */
	enum Context
	{
		HEADER,
		SUMMARY,
		STATISTICS_HEADER,
		STATISTICS_CONTENTS,
		STATISTICS_POINT_CALA,
		STATISTICS_POINT_XYZ,
		STATISTICS_POINT_XY,
		STATISTICS_POINT_XZ,
		STATISTICS_POINT_YZ,
		STATISTICS_POINT_Z,
		STATISTICS_POINT_PDOR,
		MEASURES_SUMMARY,
		MEASURES
	};
	// Context handling ensuring changing the context or adding new if there is none
	void contextChange(Context val);

private:
	/** Current frame during parsing */
	ShareableFrame *_curFrame = nullptr;
	/** input measures */
	LGCMeasures _inputData;
	/** output measures */
	LGCMeasures _resData;
	/** mapping line indexes to summary measures; only used along with _resData.instruments to map summaries to the MEASURE blocks properly */
	std::unordered_map<int, QString> _summarylineToMeasure;
	/** set containing all measures in summary; used to keep track of previously defined measures; only used with _summarylineToMeasure to map sections properly */
	std::unordered_set<QString> _summaryMeasures;
	/** Reference system */
	bool _isOloc = true;
	/** temporary counter */
	size_t _tmpcounter = 0;
	/** current instrument or frame name */
	QString _curTmp;
	/** current header point name */
	QString _curHeaderPoint;
	/** Point sigma threshold */
	double _threshPSigma = 5;
	/** Point coordinate threshold */
	double _threshPDisplacement = 5;
	/** Measure RES/SIG threshold */
	double _threshMSigma = 5;
	/** Temporary ints used for frame folding based on ID */
	int _curTmpFrameLevel;
	int _curTmpFrameLevelNext;
	/** Required for measurement data point and frame identification */
	char _pointOrFrameFlags = 0x00;
};

#endif // LGCOUTPUTRESLEXER_HPP
