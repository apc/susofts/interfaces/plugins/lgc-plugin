#include "LGCOutputResText.hpp"

#include <QMimeData>
#include <QTabWidget>

#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <utils/ClipboardManager.hpp>
#include <utils/SettingsManager.hpp>
#include <utils/StylesHelper.hpp>

#include "LGCMeasures.hpp"
#include "LGCPlugin.hpp"
#include "tabs/outputs/LGCOutputResLexer.hpp"
#include "trinity/LGCConfig.hpp"

LGCOutputResText::LGCOutputResText(SPluginInterface *owner, QWidget *parent) : TextEditorWidget(owner, parent)
{
	enablefileWatcher(true);
	installEventFilterAll(this, this);

	updateUi(owner->name());

	setupEditor();
	
}

void LGCOutputResText::lgcMeasures(const LGCMeasures &measures)
{
	auto *lex = qobject_cast<LGCOutputResLexer *>(editor()->lexer());
	if (lex)
		lex->lgcMeasures(measures);
}

const LGCMeasures &LGCOutputResText::lgcMeasures()
{
	static const LGCMeasures _m;
	auto *lex = qobject_cast<LGCOutputResLexer *>(editor()->lexer());
	return lex ? lex->lgcMeasures() : _m;
}

void LGCOutputResText::updateUi(const QString &pluginName)
{
	TextEditorWidget::updateUi(pluginName);

	if (pluginName != LGCPlugin::_name())
		return;
	auto config = SettingsManager::settings().settings<LGCConfigObject>(pluginName);

	if (!config.color)
	{
		editor()->setLexer(nullptr);
		editor()->clearFolds();
	}
	else
	{
		if (config.color && !editor()->lexer())
			editor()->setLexer(new LGCOutputResLexer(this));
		updateContent();
	}
	setBackground(editor(), getThemeColor(QColor(240, 240, 240)));
}

bool LGCOutputResText::_open(const QString &path)
{
	bool retour = TextEditorWidget::_open(path);
	editor()->foldLine(2);
	editor()->foldLine(25);
	return retour;
}

QString LGCOutputResText::clipboardMimeType() const
{
	return LGCPlugin::_baseMimetype() + ".outputpointRAW";
}

QByteArray LGCOutputResText::fromMime(const QMimeData *mimedata)
{
	LGCOutputResLexer io;
	io.utf8(editor()->isUtf8());
	auto *lex = qobject_cast<LGCOutputResLexer *>(editor()->lexer());
	if (lex)
		io.isOloc(lex->isOloc());
	return contentFromMime(mimedata, io);
}

QMimeData *LGCOutputResText::toMime(const QByteArray &text, QMimeData *mimedata)
{
	LGCOutputResLexer io;
	io.utf8(editor()->isUtf8());
	auto *lex = qobject_cast<LGCOutputResLexer *>(editor()->lexer());
	if (lex)
		io.isOloc(lex->isOloc());
	return contentToMime(text, mimedata, io);
}

void LGCOutputResText::setupEditor()
{
	editor()->setMimeTypesSupport(
		{clipboardMimeType().toStdString()}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });

	QTabWidget *widgetHelp = qobject_cast<QTabWidget *>(helpWidget());
	if (widgetHelp)
	{
		// Set default tab to StructureViewer
		widgetHelp->setCurrentIndex(2);
	}
}
