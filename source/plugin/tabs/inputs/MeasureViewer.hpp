/*
© Copyright CERN 2000-2023. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef MEASUREVIEWER_HPP
#define MEASUREVIEWER_HPP

#include <memory>

#include <QWidget>

class QsciScintilla;
struct LGCMeasures;

/**
 * UI for measure lines. It shows all the measure lines for a pointname on a given line.
 */
class MeasureViewer : public QWidget
{
	Q_OBJECT

public:
	MeasureViewer(QsciScintilla *editor, std::function<const LGCMeasures &(void)> getMeasures, QWidget *parent = nullptr);
	virtual ~MeasureViewer() override;

protected:
	virtual void showEvent(QShowEvent *event) override;

private:
	/** Setup the looks and connections for a table. */
	void setupTable(QsciScintilla *editor, std::function<const LGCMeasures &(void)> getMeasures);

private slots:
	/** Clean and recreate the table. */
	void rebuildTable(int line = -1);

private:
	/** pimpl */
	class _MeasureViewer_pimpl;
	std::unique_ptr<_MeasureViewer_pimpl> _pimpl;
};

#endif // MEASUREVIEWER_HPP
