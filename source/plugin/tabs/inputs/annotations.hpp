#include <unordered_map>

#include <QObject>
#include <QString>

#include <LGCMeasures.hpp>

#define _ANNOTATE(annotation) (annotation) + _ANNOTATION_DOCLINK

// GENERAL
inline const QString _ANNOTATION_DOCLINK = QObject::tr(R"""(<p>For more information, please refer to the <a href="https://confluence.cern.ch/display/SUS/LGC2+User+Guide">documentation</a>.<p>
)""");

// GENERAL FILE LAYOUT
inline const QString _ANNOTATION_TITR = _ANNOTATE(QObject::tr(R"""(<h2>TITR</h2>
	<p>Every file has to start with the keyword <code>*TITR</code> with a new line after the keyword followed by an arbitrary number of lines and blank lines.<p>
)"""));

// OPTIONS
//// DATUM OPTIONS
inline const QString _ANNOTATION_DATUMOPTIONS = _ANNOTATE(QObject::tr(R"""(<h2>Datum Options</h2>
	<p>Datum options specifying coordinate system used by the points described in the file.<p>
)"""));

//// CALCULATION OPTIONS
inline const QString _ANNOTATION_ALLFIXED = _ANNOTATE(QObject::tr(R"""(<h2>ALLFIXED</h2>
	<p>When introduced, the program will run as if all points were listed under CALA - that is being fixed in all dimensions. The output file is in the same format, except having a recalculated V0 for all rounds of measurements, or a recalculated instrument heigth or distance correction.</p>
	<p>For ECSP (V0) and PLR3D (V0, Rx, Ry) two solutions can be found. They are writen in the output file, the user should analyze the results.</p>
	<p><em>NB: Usable only with fixed frame.</em></p>
	<h3>Usage:</h3>
	<pre>
    *ALLFIXED
	</pre>
)"""));

inline const QString _ANNOTATION_SIMU = _ANNOTATE(QObject::tr(R"""(<h2>SIMU</h2>
	<p>Simulates the network to gain insight into its error behavior before taking measurements. Measurement values are allowed but ignored. The desired number of simulations should be specified. The seed number can be specified if reproducibility is required.</p>
	<h3>Usage:</h3>
	<pre>
    *SIMU [NumberOfSimulations] [Seed]
	</pre>
	<p>- NumberOfSimulations &lt;positive int&gt; - Number of simulations to run.
	<br />- Seed &lt;int&gt; - Seed number for computation - if provided will ensure that the results are reproducible.</p>
	<h3>Example:</h3>
	<pre>
    *SIMU 10
    *SIMU 10 5
    *SIMU 10 -5
	</pre>
)"""));

inline const QString _ANNOTATION_PREC = _ANNOTATE(QObject::tr(R"""(<h2>PREC</h2>
	<p>Allows to modify the number of the digits in the results, and fix the convergence criteria, to be consistent with the expected precision. By default, results are given with 5 digits.</p>
	<h3>Usage:</h3>
	<pre>
    *PREC n,  where n is an integer from 0 to 7
	</pre>
	<h3>Example:</h3>
	<pre>
    *PREC 6
	</pre>
)"""));

//// OUTPUT OPTIONS
inline const QString _ANNOTATION_APRI = _ANNOTATE(QObject::tr(R"""(<h2>APRI</h2>
	<p>A priori sigma ( = 1) is used for all the calculation. Even if Sigma 0 a posteriori is outside the range, it doesn't apply to the residual.</p>
	<h3>Usage:</h3>
	<pre>
    *APRI
	</pre>
)"""));

inline const QString _ANNOTATION_COVAR = _ANNOTATE(QObject::tr(R"""(<h2>COVAR</h2>
	<p>Create an output file (.cov) listing all frames and their points and giving the covariance matrix for each variable object.</p>
	<h3>Usage:</h3>
	<pre>
    *COVAR
	</pre>
)"""));

inline const QString _ANNOTATION_CONSI = _ANNOTATE(QObject::tr(R"""(<h2>CONSI</h2>
	<p>Triggers a consistency check to detect issues with identifiability of points or groups of points. Additionally, it is used to perform "free" network adjustment. The "free" adjustment constraints are chosen either by an automatic analysis based on the consistency check or by supplying them manually. For more information see: <a href="https://confluence.cern.ch/display/SUS/LGC2+User+Guide#LGC2UserGuide-CONSI">link to the lgc user guide</a></p>
	<pre data-bidi-marker="true">Usage:
    *CONSI [LIBR [TX TY TZ RX RY RZ SCL]]
    ------
    Optional arguments:</pre>
	<p style="margin-left: 40.0px;">LIBR:</p>
	<p style="margin-left: 40.0px;">TX,TY,TZ:  Manually specified constraints on the * coordinate of the center of gravity</p>
	<p style="margin-left: 40.0px;">RZ,RY,RZ:  Manually specified constraints on the * rotation around the center of gravity</p>
	<p style="margin-left: 40.0px;">SCL:       Manually specified constraints on the Scale factor</p>
	<pre data-bidi-marker="true"><br>Examples:</pre>
    <p style="margin-left: 40.0px;">*CONSI&nbsp; % triggers a consistency check. results are written in the .log2 file. Computation is stopped if a problem is detected</p>
    <p style="margin-left: 40.0px;">*CONSI&nbsp; LIBR % does a consistency check and in case a problem is detected there will be an attempt to find a minimal set of constraints that makes the computation possible. After this a computation is started with the proposed constraints</p>
    <p style="margin-left: 40.0px;">*CONSI&nbsp; LIBR TX RY % does a consistency check, displays the results but then attempts the calculation with the user defined constraints imposed on all points. In this example the x coordinate of the center of gravity and the rotation around the y axis will be fixed&nbsp; to the values defined by the provisional positions</p>
)"""));

inline const QString _ANNOTATION_DEFA = _ANNOTATE(QObject::tr(R"""(<h2>DEFA</h2>
	<p>Allows to create an input file for deform software. This file contains the upper triangular entries of the variance-covariance matrix of the point coordinates row by row (equivalent to the lower triangular entries column by column).</p>
	<h3>Usage:</h3>
	<pre>
    *DEFA
	</pre>
)"""));

inline const QString _ANNOTATION_EREL = _ANNOTATE(QObject::tr(R"""(<h2>EREL</h2>
	<p>List of point pair for the relative errors.</p>
	<h3>Usage:</h3>
	<pre>
    *EREL
    PT1 PT2
    PT5 PT3 ...
	</pre>
)"""));

inline const QString _ANNOTATION_ERELFRAME = _ANNOTATE(QObject::tr(R"""(<h2>ERELFRAME</h2>
    <p>The keyword ERELFRAME can be used to compute the Helmert parameters corresponding to the relative transformation between two specified frames.</p>
    <p>This computation will be carried out after the estimation process and also includes the precisions and covariances of the relative transformation.</p>
    <p>Usage:</p>
    <p>*ERELFRAME<br>F1 F2 % will compute the relative transformation going from frame F1 to frame F2</p>
    <p>F1 ROOT % this computes the transformation going from frame F1 to ROOT frame (the ROOT frame always exists)</p>
)"""));

inline const QString _ANNOTATION_FAUT = _ANNOTATE(QObject::tr(R"""(<h2>FAUT</h2>
	<p>Allows calculation of the reliability parameters of each observation and the blunders.
	<br /> We distinguish local reliability, internal, external and even for a global network. All of these tools are summurised in the .err file.</p>
	<h3>Usage:</h3>
	<pre>
    *FAUT [alpha beta]
	</pre>
	<p>- The alpha percentage is the significance level of the tests, and the risk of rejecting a rigth measure. By default alpha is equal to 1%.
	<br />- The beta percentage is the risk of accepting a false measure. By default beta is equal to 10% (write 0.1)</p>
	<h4>Alpha</h4>
	<p>alpha (significance level of the tests)</p>
	<ul>
	<li>1 - alpha (confidence level)</li>
	<li>(1-alpha) is the upper limit wherein residuals are less than a tolerance value Wmax.</li></ul>
	<p>Example: If the tolerance is 2 times wmax the measurement accuracy, the probability that any residual is less than wmax, this tolerance is 97%.</p>
	<h4>Beta</h4>
	<p>The beta percentage is the risk of accepting a false measure. More dangerous risk than alpha.</p>
	<p><Example: if we deliberately falsifies a measure introducing him a blunder, the symmetry axis for the residual distribution will be shifted to a very big willful misconduct, for a little less displaced.</p>
	<p>Follow this link for more information: <a href="https://confluence.cern.ch/display/SUS/Reliability+and+Blunder+detection">link to blunder detection</a>.</p>
)"""));

inline const QString _ANNOTATION_FMTP = _ANNOTATE(QObject::tr(R"""(<h2>FMTP</h2>
	<p>Allows to format the punc file</span>. By default the separator is a column. If an othe separator is expected call:</p>
	<h3>Usage:</h3>
	<pre>
    *FMTP SEP &quot;separator&quot;, where separator is the expected separator. The double quote are needed.
    *FMTP COL, is also the default separator.
	</pre>
)"""));

inline const QString _ANNOTATION_HIST = _ANNOTATE(QObject::tr(R"""(<h2>HIST</h2>
	<p>Allows to write an histogram with the residual per measurement type in the output file. Write the histogram if at least 5 measurements are done.</p>
	<h3>Usage:</h3>
	<pre>
    *HIST
	</pre>
)"""));
inline const QString _ANNOTATION_JSON = _ANNOTATE(QObject::tr(R"""(<h2>JSON</h2>
	<p>Allows to write the JSON object that contains the serialized output data.</p>
	<h3>Usage:</h3>
	<pre>
    *JSON, saves only the main FILENAME.json file.
    *JSON COVAR, saves covariance matrices in addition to the main object. They will be saved in FILENAME_ucovar.json.
	</pre>
)"""));

inline const QString _ANNOTATION_NODUP = _ANNOTATE(QObject::tr(R"""(<h2>NODUP</h2>
	<p>Returns an error if duplicated measurements are found in the file.</p>
	<h3>Usage:</h3>
	<pre>
    *NODUP
	</pre>
)"""));

inline const QString _ANNOTATION_PDOR = _ANNOTATE(QObject::tr(R"""(<h2>PDOR</h2>
	<p>Allow to give an orientation of the network.</p>
	<p>Using this keyword, at least one CALA point has to be defined in ROOT (the first point following the CALA keyword is considered as the unique fixed point), and the chosen orientation point must be defined after in POIN or VXY.</p>
	<h3>Usage:</h3>
	<pre>
    *PDOR
    point_name [bearing],  where the bearing is a gon value
	</pre>
	<p>NB: Only one unique PDOR is considered for constraining the orientation of the network calculations.</p>
	<p><span style="color: rgb(155,135,12);"><strong>PDOR must be defined between the instruments and the points.</strong></span></p>
	<p>PDOR keyword is not allowded after keywords: CALA, POIN, VXY, VYZ, VXZ, VZ.</p>
)"""));

inline const QString _ANNOTATION_PRES = _ANNOTATE(QObject::tr(R"""(<h2>PRES</h2>
	<p>Gives <a href="https://confluence.cern.ch/display/SUS/Calculation+of+Ellips+and+Ellipsoid+error">ellips or ellispodal error</a> for the variable points (POIN, VXY, VXZ or VYZ). The ellips or ellipsoidal data are given in the subframe of the point.</p>
	<h3>Usage:</h3>
	<pre>
    *PRES
	</pre>
)"""));

inline const QString _ANNOTATION_PUNC = _ANNOTATE(QObject::tr(R"""(<h2>PUNC</h2>
	<p>Writes a list of points with optional data (.coo):</p>
	<p>*PUNC <br /><em>Nom_Pt X Y Z</em></p>
	<p>*PUNC E <br /><em>Nom_Pt X Y Z Sx Sy Sz Cxy Cxz Cyz Dx Dy Dz</em></p>
	<p>*PUNC EE<br /><em>Nom_Pt X Y Z direction_1 direction_2 direction_3 demi_axe_1 demi_axe_2 demi_axe_3 Dx Dy Dz</em></p>
	<p>The ellipsoide give 3 axis which are defined by an associated direction. For an ellips we fill in only the first direction as the bearing of the major axis (demi_axe 1 or 2)<em><br /></em></p>
	<p>*PUNC H<br /><em>Nom_Pt X Y H</em></p>
	<p>*PUNC Z<br /><em>Nom_Pt X Y Z</em></p>
	<p>*PUNC HZ<br /><em>Nom_Pt X Y Z H</em></p>
	<p>*PUNC HN<br /><em>Nom_Pt X Y H N</em></p>
	<p>*PUNC ZHN<br /><em>Nom_Pt X Y Z H N</em></p>
	<p>*PUNC T<br /><em>Nom_Pt X Y Z Sx Sy Sz</em></p>
	<p>*PUNC OUT1 <br /><em>Nom_Pt X Y Z ID dx dy dz DCum in a file *.coo</em></p>
	<p>*PUNC OUT3 (not yet implemented)<br /><em>write a *.mes file with all measurements for GEODE</em><br /></p>
	<p>All points coordinates are given in the root frame, so if additionnal data should be writen, only initial root points will have this information.</p>
)"""));

inline const QString _ANNOTATION_SOBS = _ANNOTATE(QObject::tr(R"""(<h2>SOBS</h2>
	<p>Write na input file with the simulated observation. Need to be used with SIMU.</p>
	<h3>Usage:</h3>
	<pre>
    *SOBS
	</pre>
)"""));

inline const QString _ANNOTATION_CHABA = _ANNOTATE(QObject::tr(R"""(<h2>CHABA</h2>
	<p>Write a output file for the best fit calculation with a simular format to CHABA. See page <a href="https://confluence.cern.ch/pages/viewpage.action?pageId=55117116">here</a> for more information.</p>
	<h3>Usage:</h3>
	<pre>
    *CHABA
	</pre>
)"""));

// INSTRUMENTS
inline const QString _ANNOTATION_INSTR = _ANNOTATE(QObject::tr(R"""(<h2>INSTR</h2>
	<p>All instruments and targets that are used in the measurements must be specified in the instruments section.</p>
	<p>The section is opened by the keyword <code>*INSTR</code> followed by the keywords for the respective instruments:</p>
	<ul>
	<li><code>*POLAR</code> Total station or laser tracker</li>
	<li><code>*CAMD</code> Camera type instrument</li>
	<li><code>*EDM</code> Electronic distance meter</li>
	<li><code>*LEVEL</code> Leveling instrument</li>
	<li><code>*SCALE</code> Standard (desktop) scale</li></ul>
	<p>Each instrument used is identified with the appropriate keyword.</p>
	<p>A list of the targets used with a given instrument (POLAR, EDM, LEVEL) are placed on subsequent lines starting with the Target ID, with one line for each target.</p>
	<p>All targets used for measurements from a given instrument must be identified.</p>
	<p>Parameters of the instrument or instrument / target pair are entered in this section. Some arguments are specified on the declaration line for the instrument, other arguments may be specified for an instrument / target pair. All arguments specified must be present.</p>
)"""));

inline const QString _ANNOTATION_POLAR = _ANNOTATE(QObject::tr(R"""(<h2>POLAR</h2>
	<p>Defines an instrument that makes polar measurements, such as a total station or laser tracker, and it's associated targets.</p>
	<h3>Usage:</h3>
	<pre>
    *POLAR instr_ID    default_tgt_ID   instr_height_m    sigma_instr_height_mm    sigma_instr_centering_mm   const_angl_gon
    tgt_ID   sigma_ANGL_cc  sigma_ZEND_ cc   sigma_DIST_mm ppm_mm   dCorr_adjustable  dCorr_m   sigma_dCorr_mm   sigma_tgt_centering_mm   tgt_height_m   sigma_tgt_height_mm
-----
    Instrument definition arguments:
    instr_ID:                 An unique name for the instrument.
    default_tgt_ID:           The name of the default target to use with this instrument. The target must be defined in this instrument section.
    instr_height_m:           Height of the instrument [m].
    sigma_instr_height_mm:    1-sigma precision of the instrument height [mm].
    sigma_instr_centering_mm: 1-sigma precision of the instrument centering [mm].
    const_angl_gon:           A known constant angle of the instrument [gon].

    Target definition arguments:
    tgt_ID:                   Target ID name. At least one must be defined and one shall be defined as default in the Instrument definition
    sigma_ANGL_cc:            1-Sigma precision of the &quot;Horizontal&quot; Angle [cc].
    sigma_ZEND_cc:            1-Sigma precision for the Zenith Distance (&quot;Vertical&quot; angle) [cc].
    sigma_DIST_mm:            1-Sigma precision for the Distance measured [mm].
    ppm_mm:                   ppm to apply to the measured distance [mm/km].
    dCorr_adjustable:         bol(0 or 1): Identifies if the distance correction value is an &quot;unknown value&quot; to be determined in the adjustment.
    dCorr_m:                  Distance correction value for the measurements [m].
    sigma_dCorr_mm:           1-Sigma precision for the distance correction [mm].
    sigma_tgt_centering_mm:   1-Sigma precision for the target centering [mm].
    tgt_height_m:             Height of the target [m].
    sigma_tgt_height_mm:      1-Sigma precision of the target height [mm]
	<pre/>
	<h3>Example:</h3>
	<pre>
    *POLAR   AT401_1    CCR2    0.2598    0     0     0
    CCR1     3          3       0.02      6     1     0     0    0     0     0
    CCR2     5          6       0.03      8     0     -0.2  0    0     0     0
    *POLAR   AT401_2    CCR3    0.28      0     0     0
    CCR3     3          3       0.02      6     1     0     0    0     0     0
	</pre>
	<h3>Important remark:</h3>
	<p>In LGC2, the prism constant (<em>dCorr_m</em>) is defined for a <ins>given pair</ins> instrument/target. In other words, a prism defined with another instrument will be considered as a different prism with a different constant even if the prism name (tgt_ID) is declared identical for both instruments. When defined as unknown (both instrument parameters <em>dCorr_adjustable</em> set to 1), the Least Square process will try to estimate 2 distinct parameters (unknowns) and not a single one, eventhough it concerns one unique physical target.</p>
)"""));

inline const QString _ANNOTATION_POLAR_TGT = _ANNOTATE(QObject::tr(R"""(<h2>POLAR TARGET</h2>
	<h3>Usage:</h3>
	<pre>
    tgt_ID   sigma_ANGL_cc  sigma_ZEND_ cc   sigma_DIST_mm ppm_mm   dCorr_adjustable  dCorr_m   sigma_dCorr_mm   sigma_tgt_centering_mm   tgt_height_m   sigma_tgt_height_mm
    ----
    Target definition arguments:
    tgt_ID:                   Target ID name. At least one must be defined and one shall be defined as default in the Instrument definition.
    sigma_ANGL_cc:            1-Sigma precision of the "Horizontal" Angle [cc].
    sigma_ZEND_cc:            1-Sigma precision for the Zenith Distance ("Vertical" angle) [cc].
    sigma_DIST_mm:            1-Sigma precision for the Distance measured [mm].
    ppm_mm:                   ppm to apply to the measured distance [mm/km].
    dCorr_adjustable:         bol(0 or 1): Identifies if the distance correction value is an "unknown value" to be determined in the adjustment.
    dCorr_m:                  Distance correction value for the measurements [m].
    sigma_dCorr_mm:           1-Sigma precision for the distance correction [mm].
    sigma_tgt_centering_mm:   1-Sigma precision for the target centering [mm].
    tgt_height_m:             Height of the target [m].
    sigma_tgt_height_mm:      1-Sigma precision of the target height [mm]
	<pre/>
	<h3>Example:</h3>
	<pre>
    *POLAR     AT401_1     CCR2     0.2598     0     0    0
    CCR1     3    3     0.02     6     1     0     0    0     0     0
    CCR2     5    6     0.03     8     0     -0.2  0    0     0     0
    *POLAR     AT401_2     CCR3     0.28       0     0    0
    CCR3     3    3     0.02     6     1     0     0    0     0     0
	</pre>
)"""));

inline const QString _ANNOTATION_CAMD = _ANNOTATE(QObject::tr(R"""(<h2>CAMD</h2>
	<p>Defines a camera instrument and it's associated targets.</p>
	<p>A camera observation on a target is defined by an unit vector. See the CAM definition for an example.</p>
	<h3>Usage:</h3>
	<pre>
    *CAMD instr_ID  default_tgt_ID sigma_instr_centering_mm
    tgt_ID sigma_X  sigma_Y  sigma_dist_mm sigma_tgt_centering_mm
Or from v2.02 (old definition still available, we just assume that sigma_Z = sigma_X in this case)
    tgt_ID sigma_X  sigma_Y  sigma_Z  sigma_dist_mm sigma_tgt_centering_mm
-----
    Instrument definition arguments:
    instr_ID:                 An unique name for the instrument.
    default_tgt_ID:           The name of the default target to use with this instrument. The target must be defined in this instrument section.
    sigma_instr_centering_mm: 1-sigma precision of the instrument centering [mm].

    Target definition arguments:
    tgt_ID:                   Target ID name. At least one must be defined and one shall be defined as default in the Instrument definition.
    sigma_X:                  Standard deviation of a measured X component of the unit vector [mm/m].
    sigma_Y:                  Standard deviation of a measured Y component of the unit vector [mm/m].
    sigma_Z:                  Standard deviation of a measured Z component of the unit vector [mm/m].
    sigma_dist_mm:            Standard deviation of a distance measurement within the target [mm].
    sigma_tgt_centering_mm:   Standard deviation of the centering of the target[mm].
	</pre>
	<h3>Note:</h3>
	<p>Even if the distance are not taken by the instrument, the distance precision must be specified.</p>
)"""));

inline const QString _ANNOTATION_CAMD_TGT = _ANNOTATE(QObject::tr(R"""(<h2>CAMD TARGET</h2>
	<h3>Usage:</h3>
	<pre>
    tgt_ID sigma_X  sigma_Y  sigma_Z  sigma_dist_mm sigma_tgt_centering_mm
    ----
    Target definition arguments:
    tgt_ID:                   Target ID name. At least one must be defined and one shall be defined as default in the Instrument definition.
    sigma_X:                  Standard deviation of a measured X component of the unit vector [mm/m].
    sigma_Y:                  Standard deviation of a measured Y component of the unit vector [mm/m].
    sigma_Z:                  Standard deviation of a measured Z component of the unit vector [mm/m].
    sigma_dist_mm:            Standard deviation of a distance measurement within the target [mm].
    sigma_tgt_centering_mm:   Standard deviation of the centering of the target[mm].
	<pre/>
	<h3>Example:</h3>
	<pre>
    *POLAR     AT401_1     CCR2     0.2598     0     0    0
    CCR1     3    3     0.02     6     1     0     0    0     0     0
    CCR2     5    6     0.03     8     0     -0.2  0    0     0     0
    *POLAR     AT401_2     CCR3     0.28       0     0    0
    CCR3     3    3     0.02     6     1     0     0    0     0     0
	</pre>
)"""));

inline const QString _ANNOTATION_EDM = _ANNOTATE(QObject::tr(R"""(<h2>EDM</h2>
	<p>Defines an electronic distance meter that can be stationed on a point and its associated targets.</p>
	<h3>Usage:</h3>
	<pre>
    *EDM   instr_ID   default_tgt_ID   instr_height_m   sigma_instr_height_mm   sigma_instr_centering_mm
    tgt_ID   sigma_dist_mm  ppm_mm  dCorr_adjustable   dCorr_m   sigma_dCorr_mm  sigma_tgt_centering_mm   tgt_height_m   sigma_tgt_height_mm
-----
    Instrument definition arguments:
    instr_ID:                 An unique name for the instrument.
    default_tgt_ID:           The name of the default target to use with this instrument. The target must be defined in this instrument section.
    instr_height_m:           Height of the instrument [m].
    sigma_instr_height_mm:    1-sigma precision of the instrument height [mm].
    sigma_instr_centering_mm: 1-sigma precision of the instrument centering [mm].

    Target definition arguments:
    tgt_ID:                   Target ID name. At least one must be defined and one shall be defined as default in the Instrument definition.
    sigma_dist_mm:            1-Sigma precision for the Distance measured [mm].
    ppm_mm:                   ppm to apply to the measured distance [mm/km].
    dCorr_adjustable:         bol(0 or 1): Identifies if the distance correction value is an &quot;unknown value&quot; to be determined in the adjustment.
    dCorr_m:                  Distance correction value for the measurements [m].
    sigma_dCorr_mm:           1-Sigma precision for the distance correction [mm].
    sigma_tgt_centering_mm:   1-Sigma precision for the target centering [mm].
    tgt_height_m:             Height of the target [m].
    sigma_tgt_height_mm:      1-Sigma precision of the target height [mm].
	<pre/>
	<h3>Example:</h3>
	<pre>
    *EDM Instr_Theo Prism_Theo 0 0 0
    Prism_Theo .1 .1 0 0 0 0 0 0
	</pre>
	<h3>Important remark:</h3>
	<p>In LGC2, the prism constant (<em>dCorr_m</em>) is defined for a <ins>given pair</ins> instrument/target. In other words, a prism defined with another instrument will be considered as a different prism with a different constant even if the prism name (tgt_ID) is declared identical for both instruments. When defined as unknown (both instrument parameters <em>dCorr_adjustable</em> set to 1), the Least Square process will try to estimate 2 distinct parameters (unknowns) and not a single one, eventhough it concerns one unique physical target.</p>
)"""));

inline const QString _ANNOTATION_EDM_TGT = _ANNOTATE(QObject::tr(R"""(<h2>EDM TARGET</h2>
	<h3>Usage:</h3>
	<pre>
    tgt_ID   sigma_dist_mm  ppm_mm  dCorr_adjustable   dCorr_m   sigma_dCorr_mm  sigma_tgt_centering_mm   tgt_height_m   sigma_tgt_height_mm
    ----
    Target definition arguments:
    tgt_ID:                   Target ID name. At least one must be defined and one shall be defined as default in the Instrument definition.
    sigma_dist_mm:            1-Sigma precision for the Distance measured [mm].
    ppm_mm:                   ppm to apply to the measured distance [mm/km].
    dCorr_adjustable:         bol(0 or 1): Identifies if the distance correction value is an "unknown value" to be determined in the adjustment.
    dCorr_m:                  Distance correction value for the measurements [m].
    sigma_dCorr_mm:           1-Sigma precision for the distance correction [mm].
    sigma_tgt_centering_mm:   1-Sigma precision for the target centering [mm].
    tgt_height_m:             Height of the target [m].
    sigma_tgt_height_mm:      1-Sigma precision of the target height [mm].
	<pre/>
	<h3>Example:</h3>
	<pre>
    *EDM Instr_Theo Prism_Theo 0 0 0
     Prism_Theo .1 .1 0 0 0 0 0 0
	</pre>
)"""));

inline const QString _ANNOTATION_LEVEL = _ANNOTATE(QObject::tr(R"""(<h2>LEVEL</h2>
	<p>Defines a (digital) leveling instrument and its associated levelling staves.</p>
	<h3>Usage:</h3>
	<pre>
    *LEVEL   instr_ID   default_staff_ID  collimationAngle_adjustable  collimationAngle_gon
    staff_ID   sigma_dist_mm   ppm_mm   dCorr_m   sigma_dCorr_mm   vert_offset_m   sigma_offset_mm
-----
    Instrument definition arguments:
    instr_ID:                        An unique name for the instrument.
    default_staff_ID:            The name of the default staff to use with this instrument. The target must be defined in this instrument section.
    collimationAngle_adjustable: Identifies if the collimation angle is an &quot;unknown value&quot; to be determined in the adjustment. Values: 1 for unknown, 0 for known.
    collimationAngle_gon:        Value of the collimation angle [gon].

    Target definition arguments:
    staff_ID:                    Staff ID name. At least one must be defined and one shall be defined as default in the Instrument definition.
    sigma_dist_mm:               1-Sigma precision for the Distance measured [mm].
    ppm_mm:                      ppm to apply to the measured distance [mm/km].
    dCorr_m:                     Distance correction value for the measurements [m].
    sigma_dCorr_mm:              1-Sigma precision for the distance correction [mm].
    vert_offset_m:               Vertical offset of the staff [m].
    sigma_offset_mm:             Standard deviation of the vertical offset of the staff [mm].
	</pre>
	<h3>Note:</h3>
	<p>Keyword needed if the *DLEV type of measurements need to be used. Most of the levelling is defined with the type *DVER. In that case no *LEVEL instruments need to de specified.</p>
)"""));

inline const QString _ANNOTATION_LEVEL_TGT = _ANNOTATE(QObject::tr(R"""(<h2>LEVEL STAFF</h2>
	<h3>Usage:</h3>
	<pre>
    staff_ID   sigma_dist_mm   ppm_mm   dCorr_m   sigma_dCorr_mm   vert_offset_m   sigma_offset_mm
    ----
    Target definition arguments:
    staff_ID:                    Staff ID name. At least one must be defined and one shall be defined as default in the Instrument definition.
    sigma_dist_mm:               1-Sigma precision for the Distance measured [mm].
    ppm_mm:                      ppm to apply to the measured distance [mm/km].
    dCorr_m:                     Distance correction value for the measurements [m].
    sigma_dCorr_mm:              1-Sigma precision for the distance correction [mm].
    vert_offset_m:               Vertical offset of the staff [m].
    sigma_offset_mm:             Standard deviation of the vertical offset of the staff [mm].
	</pre>
	<h3>Note:</h3>
	<p>Keyword needed if the *DLEV type of measurements need to be used. Most of the levelling is defined with the type *DVER. In that case no *LEVEL instruments need to de specified.</p>
)"""));

inline const QString _ANNOTATION_SCALE = _ANNOTATE(QObject::tr(R"""(<h2>SCALE</h2>
	<p>Defines a scale instrument (e.g. ruler, offset measurement device). Does not use a target, and as a consequence it does not have additional lines like the other instruments.</p>
	<h3>Usage:</h3>
	<pre>
    *SCALE  instr_ID  sigma_dist_mm  ppm_mm  dCorr_m  sigma_dCorr_mm  sigma_instr_centering_mm
    -----
    Instrument definition arguments:
    instr_ID:                    An unique name for the instrument.
    sigma_dist_mm:               1-Sigma precision for the Distance measured [mm].
    ppm_mm:                      ppm to apply to the measured distance [mm/km].
    dCorr_m:                     Distance correction value for the measurements [m].
    sigma_dCorr_mm:              1-Sigma precision for the distance correction [mm].
    sigma_instr_centering_mm:    1-sigma precision of the instrument centering [mm].
	<pre/>
	<h3>Example:</h3>
	<pre>
    *SCALE RS_12   .1   0   0   0   0
    *SCALE RS_13   .2   0   0   0   0</pre>
)"""));

inline const QString _ANNOTATION_INCL = _ANNOTATE(QObject::tr(R"""(<h2>INCL</h2>
	<p>Defines an inclinometer instrument, i.e. measuring an angle. Does not use a target, and as a consequence it does not have additional lines like the other instruments.</p>
	<h3>Usage:</h3>
	<pre>
    *INCL  instr_ID  sigma_angle_cc  sigma_ppm  aCorr_gon  sigma_aCorr_cc  refAngle_gon  sigma_refAngle_cc
    -----
    Instrument definition arguments:
    instr_ID:                    An unique name for the instrument.
    sigma_angle_cc:              1-Sigma precision for the angle measured [cc].
    sigma_ppm:                   1-Sigma precision for the angle measured [μrad or μm/m]. It is added (NOT QUARATICALLY) in the sigma_angle.
    aCorr_gon:                   Angle correction value for the measurements [gon]. This value can be used to define an instrumental constant.
    sigma_aCorr_cc:              1-Sigma precision for the angle correction [cc].
    refAngle_gon:                Default reference angle correction value for the measurements [gon]. This value can be used to define the reference angle as defined in GEODE DB.
    sigma_refAngle_cc:           1-Sigma precision for the reference angle correction [cc].
	<pre/>
	<h3>Example:</h3>
	<pre>
    *INCL I_12   5   3    1   1  0  0 
    *INCL I_13   10  3   -5   2  1  2</pre>
)"""));

inline const QString _ANNOTATION_HLSR = _ANNOTATE(QObject::tr(R"""(<h2>HLRS</h2>
	<p>Defines a hydrostatic levelling sensor, i.e. measuring a distance. Does not use a target, and as a consequence it does not have additional lines like the other instruments.</p>
	<h3>Usage:</h3>
	<pre>
    *HLSR  instr_ID  sigma_dist_mm  sigma_instr_height_mm  sigma_instr_centering_mm
    -----
    Instrument definition arguments:
    instr_ID:                    An unique name for the instrument.
    sigma_dist_mm:               1-Sigma precision for the Distance measured [mm].
    sigma_instr_height_mm:       1-sigma precision of the instrument height [mm].
    sigma_instr_centering_mm:    1-sigma precision of the instrument centering [mm].
	<pre/>
	<h3>Example:</h3>
	<pre>
    *HLSR HL_SNSR_1   0.03   0     0
    *HLSR HL_SNSR_2   0.05   0.02  1</pre>
)"""));

inline const QString _ANNOTATION_WPSR = _ANNOTATE(QObject::tr(R"""(<h2>WPSR</h2>
    <p>Available From version 2.6.0: Defines a Wire Positioning Sensor, i.e. measuring 2 distances. Does not use a target, and as a consequence it does not have additional lines like the other instruments.</p>
    <h3>Usage:</h3>
    <pre>
    *WPSR  instr_ID  sigma_dist_X_mm  sigma_dist_Z_mm  sigma_instr_centering_X_mm  sigma_instr_centering_Z_mm
    -----
    Instrument definition arguments:
    instr_ID:                    An unique name for the instrument.
    sigma_dist_X_mm:             1-Sigma precision for the X Distance measured [mm].
    sigma_dist_Z_mm:             1-sigma precision for the Z Distance measured [mm].
    sigma_instr_centering_X_mm:  1-sigma precision of the instrument centering in the station's X-axis coordinate system [mm].
    sigma_instr_centering_Z_mm:  1-sigma precision of the instrument centering in the station's Z-axis coordinate system [mm].
	<pre/>
	<h3>Example:</h3>
	<pre>
    *WPSR   WPS_1   0.03    0.03    0.04    0.04
    *WPSR   WPS_2   0.07    0.01    0.14    0.01</pre>
)"""));

// FRAMES
inline const QString _ANNOTATION_FRAME = _ANNOTATE(QObject::tr(R"""(<h2>FRAME</h2>
	<p>Stacking local Cartesian frames arbitrarily in the configuration is a major new feature in this version of LGC. A FRAME section is a logical block that can contain points, certain measurements like UVEC, UVD, OBSXYZ (and ANGL, ZEND, DIST from version 2.02) and further frames. There is a note in the keyword description when a measurement is only allowed in the root frame. The section must be closed by ENDFRAME. A frame is for example useful to create a group of points that can only move together. The points must be declared using CALA inside the frame declaration to achieve a moving point group. A frame is defined by;three <em>translations</em>;relative to its parent frame, three <em>rotations</em> and a <em>scale factor.</em></p>
	<p>By default a frame is fixed, i.e. it does not introduce additional parameters itself. The rotations around the axes may be enabled by setting the RX, RY and RZ flags, translations are enabled by the TX, TY and TZ flags and scale by SCL flag.</p>
	<p>The transformation into the specified frame is done using transformation matrix in homogeneous coordinates. Rotation matrix used has form Rxyz = Rx * Ry * Rz, i.e. firstly rotation about Z axis is applied, followed by a rotation about Y axis, and ending with a rotation about the X axis.</p>
	<p>Parameters are the values to pass from the child into the parent frame (Xparent= M * Xchild)</p>
	<h3>Usage:</h3>
	<pre>     
*FRAME  name  trx_m  try_m  trz_m  rx_gon  ry_gon  rz_gon  scale  [TX] [TY] [TZ] [RX] [RY] [RZ] [SCL]
-----  
    trx_m: flt: Translation around the X-axis [m]  
    try_m: flt: Translation around the Y-axis [m] 
    ... 
    rz_gon: flt: Rotation around the Z-axis [gon] 
    scale: flt: Unitless scale factor
    TX: flag: The frame may translate along the X-axis
    TY: flag: The frame may translate along the Y-axis
    TZ: flag: The frame may translate along the Z-axis
    RX: flag: The frame may rotate around its X-axis
    RY: flag: The frame may rotate around its Y-axis
    RZ: flag: The frame may rotate around its Z-axis
    SCL: flag: The frame may be scaled 
	</pre>
)"""));

// POINTS SECTION
inline const QString _ANNOTATION_POINTS = _ANNOTATE(QObject::tr(R"""(<h2>Points</h2>
	<p>The basic declaration of points does not differ from the previous version of LGC.</p>
	<p>One of the point type keyword can be followed by several points of this type, one per line following always the same structure:</p>
	<h3>Usage:</h3>
	<pre>
    *Point_Type
        Point_Name x_m y_m z_m

    with:

    *Point_Type:    Type of point, can be of value: *POIN, *CALA, *VXY, *VYZ or *VZ.
    Point_Name:     Name of the point, a point can only be declared once in the whole file.

    x_m:          X coordinate of the point [m].
    y_m:          Y coordinate of the point [m].
    z_m:          Z coordinate of the point [m].
	</pre>
	<p>The table below lists the degrees of freedom for each point type in the frame it is defined in.<br /></p>
	<table class="wrapped relative-table" style="width: 40.3%;"><colgroup><col style="width: 13.81%;" /><col style="width: 6.63%;" /><col style="width: 6.49%;" /><col style="width: 6.49%;" /><col style="width: 66.32%;" /></colgroup>
	<tbody>
	<tr>
	<th>Keyword</th>
	<th>Vx</th>
	<th>Vy</th>
	<th>Vz</th>
	<th colspan="1">Comments</th></tr>
	<tr>
	<td>POIN</td>
	<td style="text-align: center;"> x </td>
	<td style="text-align: center;"> x </td>
	<td style="text-align: center;"> x </td>
	<td colspan="1">Point totally free, ex: point measured by a laser tracker</td></tr>
	<tr>
	<td>CALA</td>
	<td>   </td>
	<td>   </td>
	<td>   </td>
	<td colspan="1">Point totally constrainded, ex: reference point</td></tr>
	<tr>
	<td>VXY</td>
	<td style="text-align: center;"> x </td>
	<td style="text-align: center;"> x </td>
	<td>   </td>
	<td colspan="1">Moving in the XY-Plane, ex: point measured by wire</td></tr>
	<tr>
	<td>VXZ</td>
	<td style="text-align: center;"> x </td>
	<td>   </td>
	<td style="text-align: center;"> x </td>
	<td colspan="1">Moving in the XZ-Plane</td></tr>
	<tr>
	<td>VYZ</td>
	<td>   </td>
	<td style="text-align: center;"> x </td>
	<td style="text-align: center;"> x </td>
	<td colspan="1">Moving in the YZ-Plane</td></tr>
	<tr>
	<td>VZ</td>
	<td>   </td>
	<td>   </td>
	<td style="text-align: center;"> x </td>
	<td colspan="1">Moving only in Z, ex: point observed only by a level</td></tr></tbody></table>
	<p>More info about points <a href="https://confluence.cern.ch/display/SUS/LGC2+User+Guide#LGC2UserGuide-Pointssection">here</a>.</p>
)"""));

//// POINT TYPES
inline const QString _ANNOTATION_POINT_POIN = _ANNOTATE(QObject::tr(R"""(<h2>POIN</h2>
	<p>Point totally free, ex: point measured by a laser tracker</p>
	<h3>Usage:</h3>
	<pre>
    Point_Name    x_m    y_m    z_m
	</pre>
	<h3>Example:</h3>
	<pre>
    *POIN
    LHC.MB.C24L8.S        4797.91877     6057.22215    329.03512
    LHC.MB.B24L8.E        4797.17329     6052.42131    329.03576
    LHC.MB.B24L8.S        4795.49054     6041.75318    329.04039
    LHC.MB.A24L8.E        4794.71689     6036.95967    329.0376
	</pre>
)"""));

inline const QString _ANNOTATION_POINT_CALA = _ANNOTATE(QObject::tr(R"""(<h2>CALA</h2>
	<p>Point totally constrainded, ex: reference point</p>
	<h3>Usage:</h3>
	<pre>
    Point_Name    x_m    y_m    z_m
	</pre>
	<h3>Example:</h3>
	<pre>
    *CALA
    LHC.MQ.24L8.E         4800.60321     6074.89790    329.03192
    LHC.MQ.24L8.S         4800.18209     6072.08288    329.0337
    LHC.MB.C24L8.E        4799.54724     6067.89932    329.03298
	</pre>
)"""));

inline const QString _ANNOTATION_POINT_VXY = _ANNOTATE(QObject::tr(R"""(<h2>VXY</h2>
	<p>Moving in the XY-Plane, ex: point measured by wire</p>
	<h3>Usage:</h3>
    <pre>
    Point_Name    x_m    y_m    z_m
	</pre>
	<h3>Example:</h3>
	<pre>
    *VXY
    LHC.MB.A24L8.S         4792.98037     6026.29701    329.04296
	</pre>
)"""));

inline const QString _ANNOTATION_POINT_VXZ = _ANNOTATE(QObject::tr(R"""(<h2>VXZ</h2>
	<p>Moving in the XZ-Plane</p>
	<h3>Usage:</h3>
	<pre>
    Point_Name    x_m    y_m    z_m
	</pre>
	<h3>Example:</h3>
	<pre>
    *VXZ
    LHC.MQ.23L8.E        4792.29507     6022.10132    329.04920
    LHC.MQ.23L8.S        4791.83184     6019.29436    329.05074
	</pre>
)"""));

inline const QString _ANNOTATION_POINT_VYZ = _ANNOTATE(QObject::tr(R"""(<h2>VYZ</h2>
	<p>Moving in the YZ-Plane</p>
	<h3>Usage:</h3>
	<pre>
    Point_Name    x_m    y_m    z_m
	</pre>
	<h3>Example:</h3>
	<pre>
    *VYZ
    LHC.MB.B23L8.S        4786.67425     5989.03963    329.06361
	</pre>
)"""));

inline const QString _ANNOTATION_POINT_VZ = _ANNOTATE(QObject::tr(R"""(<h2>VZ</h2>
	<p>Moving only in Z, ex: point observed only by a level</p>
	<h3>Usage:</h3>
	<pre>
    Point_Name    x_m    y_m    z_m
	</pre>
	<h3>Example:</h3>
	<pre>
    *VZ
    TPS.STL.07081902.        1898.159600     2074.158420    433.793010   $-999.000    792571
	</pre>
)"""));

const std::unordered_map<QString, const QString> _ANNOTATION_POINTTYPES{{"*POIN", _ANNOTATION_POINT_POIN}, {"*CALA", _ANNOTATION_POINT_CALA},
	{"*VXY", _ANNOTATION_POINT_VXY}, {"*VXZ", _ANNOTATION_POINT_VXZ}, {"*VYZ", _ANNOTATION_POINT_VYZ}, {"*VZ", _ANNOTATION_POINT_VZ}};

// MEASUREMENTS
//// POLAR TYPE
////// TSTN
inline const QString _ANNOTATION_TSTN = _ANNOTATE(QObject::tr(R"""(<h2>TSTN</h2>
	<p>This keyword (*TSTN) is used to start to define usually a total station or laser tracker station.</p>
	<p>It supports rounds of measurements with a common orientation. Different observations (angle measurement, horizontal distances, 3D distances, etc.) can be defined by keywords enclosed into a station started with the keyword TSTN. The details of this are described in the dedicated section of the user gudie.</p>
	<p><strong>Allowed measurements:</strong></p>
	<ul>
		<li>ANGL</li>
		<li>ZEND</li>
		<li>DIST</li>
		<li>ECTH</li>
		<li>ECDIR</li>
		<li>DHOR</li>
		<li>PLR3D</li>
	</ul>
	<p><strong>The TSTN keyword must always be succeeded by V0 to indicate a round of measurements.</strong></p>
	<p><strong>The TSTN shall be located in the root frame, if the instrument is located in a subframe (available from v2.02), only ANGL, ZEND and DIST measurements are allowed</strong>.</p>
	<p>TSTN in roots: the instrument is following the local vertical, the Z axis in case of an OLOC calculation, the local vertical for an RS2K, LEP or SPHE calculation.</p>
	<p>TSTN in a sub-frame (<strong>available from v2.02)</strong>: The TSTN follow the Z axis as defined by the frame.</p>
	<pre><span style="font-family: Arial , sans-serif;white-space: normal;">
Laser tracker measurements are usually given with ROT3D oflag since the laser tracker is not as perpendicular to the plumb line as a total station. 
Without using ROT3D, the station will floow the local vertical or the Z-axis. If ROT3D is used, the TSTN can rotate freely and therefore 2 rotation angle around X and Y axis are introduced (rotation around Z axis is done by the V0 angle), instrument height is set to 0 and fixed.  </span><strong style="font-family: Arial , sans-serif;">ROT3D is only available if PLR3D measurement are used and the instrument is defined in the root.</strong>
	<pre/>
	<h3>Usage:</h3>
	<pre>
    *TSTN stationned_point instr_ID [ ROT3D | [ IHFIX [ IH instr_heigth_m ] [IHSE sigma_instr_height_mm] ] [TRGT changed_default_tgt_ID] [ICSE sigma_instr_centering]
    ------
    Mandatory arguments:
    stationned_point:       Point on which the instrument is placed. Must be defined in the point section.
    instr_ID:               Name of the instrument as defined in the instrument section with a POLAR.
    Optional arguments:
    ROT3D:    Indicates that the instrument is not vertical (instrument height fixed and equal to 0). Introduces two unknown orientation parameters. No IHFIX flag can be added.
    IHFIX:    Indicates the instrument height is fixed, if so two other flags can be defined:
    IH:       Instrument height. The flag must be followed by the value [m]. If no IH is defined, the value defined for in the instrument section is taken.
    IHSE:     Instrument height sigma. The flag must be followed by the value [mm]. If no IHSE is defined, the value defined for in the instrument section is taken.
    TRGT:     Option to change the default target to use for this station. The flag TRGT must be followed by the name of a target defined in the instrument section.  
    ICSE:     Option to change the default 1-sigma precision of the centering of the instrument. The flag ICSE must be followed by a value [mm].
	<pre/>
	<h3>Usage:</h3>
	<pre>
    *TSTN    P1  Theo1
    *TSTN    P2  Theo1  IHFIX  IH 0.19
    *TSTN    P3  Theo1  IHFIX  IH 0.19 IHSE 0.1
    *TSTN    P4  Theo1  TRGT  Prism2 
    *TSTN    P5  Theo1  IHFIX
    *TSTN    P6  Theo1  IHFIX  IH 0.19 IHSE 0.1 TRGT Prism2
    *TSTN    P8  LTD1   ROT3D 
    *TSTN    P9  LTD1   ROT3d  TRGT  Prism2
    ...
	</pre>
	<p>*V0 is a mandatory keyword to insert directly in the line following a *TSTN keyword.</p>
)"""));

//////// V0
inline const QString _ANNOTATION_V0 = _ANNOTATE(QObject::tr(R"""(<h2>V0</h2>
	<p>*V0 is a mandatory keyword to insert directly in the line following a *TSTN keyword.</p>
	<p>All measurements that are entered below this keyword are taken with a common orientation, this implies that the instrument orientation unknowns are recalculated.</p>
	<p>It accepts a redefinition of the default target ID and the constant angle.</p>
	<p>V0 may be followed by these measurements:</p>
	<ul>
		<li>ANGL</li>
		<li>ZEND</li>
		<li>DIST</li>
		<li>ECTH</li>
		<li>ECDIR</li>
		<li>DHOR</li>
		<li>PLR3D</li>
	</ul>
	<p>The constant angle value is added to the &quot;horizontal&quot; angle to redetermine the V0 of a station.</p>
	<h3>Usage:</h3>
	<pre>
    *V0 [TRGT changed_default_tgt_ID_for_the_ROM] [ACST constant_angle_gons]
    ------
    Optional arguments:
    RGT:     Option to change the default target to use for this round of measurement. The flag TRGT must be followed by the name of a target defined in the instrument section.  
    ACST:    Change the default value, as defined in the instrument section, of the constant angle for this round of measurement. The flag ACST must be followed by a value [gon].
	<pre/>
	<h3>Example:</h3>
	<pre>
    *V0 TRGT Prism2 ACST 55
    *V0 ACST 55 TRGT Prism2
    *V0 ACST 55 TRGT Prism2
    *V0 ACST 55 
    *V0 TRGT Prism2
    *V0
	</pre>
)"""));

inline const QString _ANNOTATION_ANGL = _ANNOTATE(QObject::tr(R"""(<h2>ANGL</h2>
	<p>A standard horizontal angle measurement that can be done by a theodolite.</p>
	<pre/>
	<h3>Usage:</h3>
	<pre>
    *ANGL [TRGT changed_tgt_ID]   
    point obs_ANGL [TRGT changed_tgt_ID_meas] [OBSE sigma_cc] [TCSE tgt_centering_mm] [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_ANGL:            Value of the angle observed [gon].
    Optional arguments:
    TRGT:                Overrides the default target to use for the observation below. The flag TRGT must be followed by the name of a target defined in the instrument section.  
                         If the flag is defined at the *ANGL line it applies for all the measurement of the section, otherwise only for the observation.  
    OBSE:                Overrides the default &quot;horizontal&quot; angle 1-sigma precision for this observation. The flag OBSE must be followed by a value [cc].  
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ANGL
    PT1   205
    PT2   180  TRGT  Prism2  
    PT3   250  OBSE  0.2
    PT4   399  TCSE  0.2
    PT5   380  TRGT  Prism2  OBSE  0.3  TCSE  0.5

    *ANGL TRGT Prism4
    PT1   205
    PT2   180  TRGT  Prism2  
    PT3   250  OBSE  0.2
    PT4   399  TCSE  0.2
    PT5   380  TRGT  Prism2  OBSE  0.3  TCSE  0.5
	</pre>
	<h3>Note:</h3>
	<p>A point can be observed several time (in several lines) in the same ANGL section, each observation will contribute to the least square adjustment.</p>
	<p>The overriding rule for TRGT definition follows:</p>
	<pre>
Default TRGT on *POLAR
overriden by TRGT on *TSTN
overriden by TRGT on the *V0
overriden by TRGT on the *ANGL line
overriden by TRGT defined on the observation line
overriden by  OBSE/TCSE
	</pre>
)"""));

inline const QString _ANNOTATION_ANGL_POINT = _ANNOTATE(QObject::tr(R"""(<h3>ANGL POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point obs_ANGL [TRGT changed_tgt_ID_meas] [OBSE sigma_cc] [TCSE tgt_centering_mm] [ID observationID]
    ----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_ANGL:            Value of the angle observed [gon].
    Optional arguments:
    TRGT:                Overrides the default target to use for the observation below. The flag TRGT must be followed by the name of a target defined in the instrument section.  
                         If the flag is defined at the *ANGL line it applies for all the measurement of the section, otherwise only for the observation.  
    OBSE:                Overrides the default "horizontal" angle 1-sigma precision for this observation. The flag OBSE must be followed by a value [cc].  
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ANGL
    PT1   205
    PT2   180  TRGT  Prism2  
    PT3   250  OBSE  0.2
    PT4   399  TCSE  0.2
    PT5   380  TRGT  Prism2  OBSE  0.3  TCSE  0.5
    
    *ANGL TRGT Prism4
    PT1   205
    PT2   180  TRGT  Prism2  
    PT3   250  OBSE  0.2
    PT4   399  TCSE  0.2
    PT5   380  TRGT  Prism2  OBSE  0.3  TCSE  0.5
	</pre>
)"""));

inline const QString _ANNOTATION_ZEND = _ANNOTATE(QObject::tr(R"""(<h2>ZEND</h2>
<p>Zenith distance (&quot;Vertical angle&quot;) measurements from a theodolite or total station.</p>
	<h3>Usage:</h3>
	<pre>
    *ZEND [TRGT changed_tgt_ID] 
    point  obs_ZEND  [TRGT changed_tgt_ID_meas] [OBSE sigma_cc] [TH tgt_height_m] [THSE sigma_tgt_height_mm] [TCSE tgt_centering_mm] [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_ZEND:            Value of the observed zenith distance [gon].
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *ZEND line it applies for all the measurement of the section, otherwise only for the observation.  
    OBSE:                Overrides the default zenith distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [cc].  
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    TH:                  Overrides the default target height for this observation. The flag TH must be followed by a value [m].  
    THSE:                Overrides the default target height 1-sigma precision for this observation. The flag THSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ZEND
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2
    PT4   110  TCSE  0.2
    PT5   90   TRGT  Prism2  OBSE  0.3  TCSE  0.5
    PT6   88   TH    0.2     THSE  0.1
    PT7   55   THSE  0.1
    PT8   88   TH    0.2
    PT9   90   TRGT  Prism2  OBSE  0.3  TCSE  0.5   TH    0.2     THSE 0.1
    *ZEND TRGT Prism4
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2
    PT4   110  TCSE  0.2
    PT5   90   TRGT  Prism2  OBSE  0.3  TCSE  0.5
	</pre>
	<h3>Note:</h3>
	<p>A point can be observed several time (in several lines) in the same ZEND section, each observation will contribute to the least square adjustment.</p>
	<p>The overriding rule for TRGT definition follows:</p>
	<pre>
Default TRGT on *POLAR
overriden by TRGT on *TSTN
overriden by TRGT on the *V0
overriden by TRGT on the *ZEND line
overriden by TRGT defined on the observation line
overriden by  OBSE/TCSE/TH/THSE
	</pre>
)"""));

inline const QString _ANNOTATION_ZEND_POINT = _ANNOTATE(QObject::tr(R"""(<h3>ZEND POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point  obs_ZEND  [TRGT changed_tgt_ID_meas] [OBSE sigma_cc] [TH tgt_height_m] [THSE sigma_tgt_height_mm] [TCSE tgt_centering_mm] [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_ZEND:            Value of the observed zenith distance [gon].
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *ZEND line it applies for all the measurement of the section, otherwise only for the observation.  
    OBSE:                Overrides the default zenith distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [cc].  
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    TH:                  Overrides the default target height for this observation. The flag TH must be followed by a value [m].  
    THSE:                Overrides the default target height 1-sigma precision for this observation. The flag THSE must be followed by a value [mm].  
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ZEND
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2
    PT4   110  TCSE  0.2
    PT5   90   TRGT  Prism2  OBSE  0.3  TCSE  0.5
    PT6   88   TH    0.2     THSE 0.1
    PT7   55   THSE 0.1
    PT8   88   TH    0.2
    PT9   90   TRGT  Prism2  OBSE  0.3  TCSE  0.5   TH    0.2     THSE 0.1

    *ZEND TRGT Prism4
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2
    PT4   110  TCSE  0.2
    PT5   90  TRGT  Prism2  OBSE  0.3  TCSE  0.5
	</pre>
)"""));

inline const QString _ANNOTATION_DIST = _ANNOTATE(QObject::tr(R"""(<h2>DIST</h2>
	<p>Spatial distance measurements from a polar device.</p>
	<h3>Usage:</h3>
	<pre>
    *DIST [TRGT changed_tgt_ID] 
    point  obs_dist_m  [TRGT changed_tgt_ID_meas]  [OBSE sigma_mm]  [PPM ppm_mm]  [TH tgt_height_m] [THSE sigma_tgt_height_mm] [TCSE tgt_centering_mm] [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_dist_m:          Value of the observed spatial distance [m].
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *DIST line it applies for all the measurement of the section, otherwise only for the observation.  
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm]. 
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].    
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    TH:                  Overrides the default target height for this observation. The flag TH must be followed by a value [m].  
    THSE:                Overrides the default target height 1-sigma precision for this observation. The flag THSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *DIST
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2     PPM   0.2
    PT4   110  TCSE  0.2
    PT5   90   TRGT  Prism2  OBSE  0.3  TCSE  0.5
    PT6   88   TH    0.2     THSE  0.1
    PT7   55   THSE  0.1
    PT8   88   TH    0.2
    PT9   88   PPM   0.2
    PT10  90   TRGT  Prism2  OBSE  0.3   PPM  0.1  TCSE  0.5   TH    0.2     THSE 0.1

    *DIST TRGT Prism4
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2
    PT4   110  TCSE  0.2
    PT5   90  TRGT  Prism2  OBSE  0.3  TCSE  0.5</pre>
	<h3>Note:</h3>
	<p>A point can be observed several time (in several lines) in the same DIST section, each observation will contribute to the least square adjustment.</p>
	<p>The overriding rule for TRGT definition follows:</p>
	<pre>
Default TRGT on *POLAR
overriden by TRGT on *TSTN
overriden by TRGT on the *V0
overriden by TRGT on the *ZEND line
overriden by TRGT defined on the observation line
overriden by  OBSE/TCSE/TH/THSE/PPM
	</pre>
)"""));

inline const QString _ANNOTATION_DIST_POINT = _ANNOTATE(QObject::tr(R"""(<h3>DIST POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point  obs_dist_m  [TRGT changed_tgt_ID_meas]  [OBSE sigma_mm]  [PPM ppm_mm]  [TH tgt_height_m] [THSE sigma_tgt_height_mm] [TCSE tgt_centering_mm] [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_dist_m:          Value of the observed spatial distance [m].
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *DIST line it applies for all the measurement of the section, otherwise only for the observation.  
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm]. 
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    TH:                  Overrides the default target height for this observation. The flag TH must be followed by a value [m].  
    THSE:                Overrides the default target height 1-sigma precision for this observation. The flag THSE must be followed by a value [mm].  
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *DIST
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2     PPM   0.2
    PT4   110  TCSE  0.2
    PT5   90   TRGT  Prism2  OBSE  0.3  TCSE  0.5
    PT6   88   TH    0.2     THSE  0.1
    PT7   55   THSE 0.1
    PT8   88   TH    0.2
    PT9   88   PPM   0.2
    PT10   90   TRGT  Prism2  OBSE  0.3   PPM  0.1  TCSE  0.5   TH    0.2     THSE 0.1

    *DIST TRGT Prism4
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2
    PT4   110  TCSE  0.2
    PT5   90  TRGT  Prism2  OBSE  0.3  TCSE  0.5
	</pre>
)"""));

inline const QString _ANNOTATION_ECTH = _ANNOTATE(QObject::tr(R"""(<h2>ECTH</h2>
	<p>Stands for &quot;&eacute;cart th&eacute;odolite&quot;. A horizontal angle sets up a plane along the range of the zenith angles in this position.</p>
	<p>The measurements are the shortest distances from a point to this plane.</p>
	<h3>Usage:</h3>
	<pre>
    *ECTH  angl_gon  scale_ID 
    point  obs_m [SCALE changed_scale_ID] [OBSE sigma_dist_mm] [PPM ppm_mm] [ICSE sigma_inst_centering] [ID observationID]
    -----
    Mandatory arguments:
    angl_gon:            Observed horizontal angle defining the reference plane [gon].
    scale_ID:            Name of a staff as defined in the instrument section (*SCALE).
    point:               Name of the point measured. Must be defined in the point section. Stationed point ID (location of the scale).
    obs_m:               Observed offset distance value [m].
    Optional arguments:
    SCALE:               Overrides the default staff. The flag SCALE must be followed by the name of a staff defined in the instrument section.
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].    
    ICSE:                Overrides the default instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ECTH    40      Staff1
    PT1     1.05
    PT2     1.06    SCALE   Staff2 
    PT3     1.02    OBSE    0.2     PPM     0.2
    PT4     0.50    ICSE    0.2
    PT5     0.90    SCALE   Staff2  OBSE    0.2      PPM      0.2    ICSE      0.2</pre>
)"""));

inline const QString _ANNOTATION_ECTH_POINT = _ANNOTATE(QObject::tr(R"""(<h3>ECTH POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point  obs_m [SCALE changed_scale_ID] [OBSE sigma_dist_mm] [PPM ppm_mm] [ICSE sigma_inst_centering] [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section. Stationed point ID (location of the scale).
    obs_m:               Observed offset distance value [m].
    Optional arguments:
    SCALE:               Overrides the default staff. The flag SCALE must be followed by the name of a staff defined in the instrument section.
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    ICSE:                Overrides the default instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].  
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ECTH    40      Staff1
     PT1     1.05
     PT2     1.06    SCALE   Staff2 
     PT3     1.02    OBSE    0.2     PPM    0.2
     PT4     0.50    ICSE    0.2
     PT5     0.90    SCALE   Staff2  OBSE   0.2    PPM    0.2    ICSE    0.2
	</pre>
)"""));

inline const QString _ANNOTATION_ECDIR = _ANNOTATE(QObject::tr(R"""(<h2>ECDIR</h2>
	<p>Shortest spatial distance from points to a spatial line define by a theodolith measurement.</p>
	<h3>Usage:</h3>
	<pre> 
    *ECDIR hz_angle_gon  vert_angle_gon  scale_ID
    point  obs_m [SCALE changed_scale_ID] [OBSE sigma_mm] [PPM ppm_mm] [ICSE sigma_instr_centering_mm] [ID observationID] 
    -----
    Mandatory arguments:
    hz_angle_gon:        &quot;Horizontal&quot; angle [gon].
    vert_angle_gon:         &quot;Vertical&quot; angle [gon].
    scale_ID:            Name of a staff as defined in the instrument section (*SCALE).
    point:               Name of the point measured. Must be defined in the point section. Stationed point ID (location of the scale).
    obs_m:               Observed offset distance value [m].
    Optional arguments:
    SCALE:               Overrides the default staff. The flag SCALE must be followed by the name of a staff defined in the instrument section.
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].    
    ICSE:                Overrides the default instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ECDIR    40     30  Staff1
    PT1       1.05
    PT2       1.06   SCALE    Staff2 
    PT3       1.02   OBSE     0.2      PPM    0.2
    PT4       0.50   ICSE     0.2
    PT5       0.90   SCALE    Staff2   OBSE   0.2   PPM    0.2    ICSE    0.2</pre>
)"""));

inline const QString _ANNOTATION_ECDIR_POINT = _ANNOTATE(QObject::tr(R"""(<h3>ECDIR POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point  obs_m [SCALE changed_scale_ID] [OBSE sigma_mm] [PPM ppm_mm] [ICSE sigma_instr_centering_mm] [ID observationID] 
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section. Stationed point ID (location of the scale).
    obs_m:               Observed offset distance value [m].
    Optional arguments:
    SCALE:               Overrides the default staff. The flag SCALE must be followed by the name of a staff defined in the instrument section.
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    ICSE:                Overrides the default instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].  
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ECDIR    40   30  Staff1
    PT1       1.05
    PT2       1.06   SCALE    Staff2 
    PT3       1.02   OBSE     0.2      PPM    0.2
    PT4       0.50   ICSE     0.2
    PT5       0.90   SCALE    Staff2   OBSE   0.2   PPM    0.2    ICSE    0.2
	</pre>
)"""));

inline const QString _ANNOTATION_DHOR = _ANNOTATE(QObject::tr(R"""(<h2>DHOR</h2>
	<p>Horizontal distance measurements.</p>
	<h3>Usage:</h3>
	<pre>
    *DHOR [TRGT changed_tgt_ID]
    point   obs_m [TRGT changed_tgt_ID_meas]  [OBSE sigma_mm]  [PPM ppm_mm]  [TCSE sigma_tgt_centering_mm]  [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_dist_m:          Value of the observed horizontal distance [m].
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *DIST line it applies for all the measurement of the section, otherwise only for the observation.  
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm]. 
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].    
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *DHOR
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2     PPM   0.2
    PT4   110  TCSE  0.2
    PT5   90   TRGT  Prism2  OBSE  0.3  TCSE  0.5
    PT6   88   PPM   0.2

    *DHOR TRGT  Prism4
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2
    PT4   110  TCSE  0.2
    PT5   90   TRGT  Prism2  OBSE  0.3  TCSE  0.5
	</pre>
)"""));

inline const QString _ANNOTATION_DHOR_POINT = _ANNOTATE(QObject::tr(R"""(<h3>DHOR POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point   obs_m [TRGT changed_tgt_ID_meas]  [OBSE sigma_mm]  [PPM ppm_mm]  [TCSE sigma_tgt_centering_mm]  [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_m:          Value of the observed horizontal distance [m].
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *DIST line it applies for all the measurement of the section, otherwise only for the observation.  
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm]. 
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].   
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *DHOR
    PT1    105
    PT2    100  TRGT  Prism2  
    PT3    150  OBSE  0.2     PPM   0.2
    PT4    110  TCSE  0.2
    PT5    90   TRGT  Prism2  OBSE  0.3  TCSE  0.5
    PT6    88   PPM   0.2

    *DHOR TRGT  Prism4
    PT1   105
    PT2   100  TRGT  Prism2  
    PT3   150  OBSE  0.2
    PT4   110  TCSE  0.2
    PT5   90   TRGT  Prism2  OBSE  0.3  TCSE  0.5
	</pre>
)"""));

inline const QString _ANNOTATION_PLR3D = _ANNOTATE(QObject::tr(R"""(<h2>PLR3D</h2>
	<p>Combines a horizontal angle (ANGL), a  zenith distance (ZEND) and a spatial distance (DIST).</p>
	<p>The values typically come from a laser tracker or total station and can create a better contribution to the adjustment when they are supplied together.</p>
	<h3>Usage:</h3>
	<pre>
    *PLR3D [TRGT changed_tgt_ID]
    point obs_ANGL  obs_ZEND  obs_DIST [TRGT changed_tgt_ID_meas]  [TH tgt_height_m]  [THSE sigma_tgt_height_mm]  [TCSE sigma_tgt_centering_mm] [ASE sigma_ANGL_cc]   [ZSE sigma_ZEND_cc]  [DSE sigma_dist_mm]  [PPM ppm_mm]  [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_ANGL:            Value of the angle observed [gon].
    obs_ZEND:            Value of the observed zenith distance [gon].
    obs_DIST:            Value of the observed spatial distance [m].
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *PLR3D line it applies for all the measurement of the section, otherwise only for the observation.  
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].    
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    TH:                  Overrides the default target height for this observation. The flag TH must be followed by a value [m].  
    THSE:                Overrides the default target height 1-sigma precision for this observation. The flag THSE must be followed by a value [mm].
    ASE:                 Overrides the default &quot;horizontal&quot; angle 1-sigma precision for this observation. The flag ASE must be followed by a value [cc].
    ZSE:                 Overrides the default Zenith distance 1-sigma precision for this observation. The flag ZSE must be followed by a value [c].
    DSE:                 Overrides the default distance 1-sigma precision for this observation. The flag DSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>   
    *PLR3D
    PT1       105   100   5.0
    PT2       100   101   10     TRGT  Prism2  
    PT3       150   90    25     TRGT  Prism2    TH   0.5   THSE   0.1   TCSE   0.2    ASE    6     ZSE    10    DSE   0.5    PPM   2

    *PLR3D  TRGT   Prism4
    PT1 105 100 5.0
    PT2 100 101 10 TRGT Prism2 
    PT3 150 90 25 TRGT Prism2 TH 0.5 THSE 0.1 TCSE 0.2 ASE 6 ZSE 10 DSE 0.5 PPM 2</pre>
)"""));

inline const QString _ANNOTATION_PLR3D_POINT = _ANNOTATE(QObject::tr(R"""(<h3>PLR3D POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point obs_ANGL  obs_ZEND  obs_DIST [TRGT changed_tgt_ID_meas]  [TH tgt_height_m]  [THSE sigma_tgt_height_mm]  [TCSE sigma_tgt_centering_mm] [ASE sigma_ANGL_cc]   [ZSE sigma_ZEND_cc]  [DSE sigma_dist_mm]  [PPM ppm_mm]  [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_ANGL:            Value of the angle observed [gon].
    obs_ZEND:            Value of the observed zenith distance [gon].
    obs_DIST:            Value of the observed spatial distance [m].
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *PLR3D line it applies for all the measurement of the section, otherwise only for the observation.  
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    TH:                  Overrides the default target height for this observation. The flag TH must be followed by a value [m].  
    THSE:                Overrides the default target height 1-sigma precision for this observation. The flag THSE must be followed by a value [mm].
    ASE:                 Overrides the default "horizontal" angle 1-sigma precision for this observation. The flag ASE must be followed by a value [cc].
    ZSE:                 Overrides the default Zenith distance 1-sigma precision for this observation. The flag ZSE must be followed by a value [c].
    DSE:                 Overrides the default distance 1-sigma precision for this observation. The flag DSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>   
    *PLR3D
    PT1       105   100   5.0
    PT2       100   101   10     TRGT  Prism2  
    PT3       150   90    25     TRGT  Prism2    TH   0.5   THSE   0.1   TCSE   0.2    ASE    6     ZSE    10    DSE   0.5    PPM   2

    *PLR3D  TRGT   Prism4
    PT1 105 100 5.0
    PT2 100 101 10 TRGT Prism2 
    PT3 150 90 25 TRGT Prism2 TH 0.5 THSE 0.1 TCSE 0.2 ASE 6 ZSE 10 DSE 0.5 PPM 2
	</pre>
)"""));

//////// ORIE
inline const QString _ANNOTATION_ORIE = _ANNOTATE(QObject::tr(R"""(<h2>ORIE</h2>
	<p>Gyro-theodolite azimuth. Measurement made in a local geodetic sytem, only in ROOT.</p>
	<h3>Usage:</h3>
	<pre> 
    *ORIE stn_point POLAR_ID [TRGT changed_default_tgt_ID] [ICSE sigma_instr_centering]  [CST constant_angle]
    point obs_gon [TRGT changed_tgt_ID] [OBSE sigma_CC] [TCSE tgt_centering_mm] [ID observationID]
    ----
    Mandatory arguments:
    stn_point
    POLAR_ID            
    point:               Name of the point measured. Must be defined in the point section.
    obs_gon:             Value of the angle observed [gon].
    Optional arguments:
    TRGT:                Changes the target ID for this measurement and the following.
    ICSE:                Overrides the default Instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].
    CST                  Constant angle.
    OBSE:                Overrides the default angle 1-sigma precision for this observation. The flag OBSE must be followed by a value [cc].
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
    <pre/>
	<h3>Example:</h3>
	<pre>   
    *ORIE     LBE.HLG.35.S     AT401.390769     CST     120     ICSE     4     TRGT     CCR1.5.1
    LBE.HLG.35.E     0.1     OBSE     1     TCSE     2     TRGT     CCR1.5.1
    LBE.HLG.35.E     0.1
	</pre>
)"""));

inline const QString _ANNOTATION_ORIE_POINT = _ANNOTATE(QObject::tr(R"""(<h3>ORIE POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point obs_gon [TRGT changed_tgt_ID] [OBSE sigma_CC] [TCSE tgt_centering_mm] [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_gon:             Value of the angle observed [gon].
    Optional arguments:
    TRGT:                Changes the target ID for this measurement and the following.
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm]. 
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
    <pre/>
	<h3>Example:</h3>
	<pre>   
    *ORIE     LBE.HLG.35.S     AT401.390769     CST     120     ICSE     4     TRGT     CCR1.5.1
    LBE.HLG.35.E     0.1     OBSE     1     TCSE     2     TRGT     CCR1.5.1
    LBE.HLG.35.E     0.1
	</pre>
)"""));

//// CAMD TYPE
////// CAM
inline const QString _ANNOTATION_CAM = _ANNOTATE(QObject::tr(R"""(<h2>CAM</h2>
	<p>This keyword (*CAM) is used to start to define usually a total station or laser tracker station.</p>
	<p>Camera can be defined in the root or sub-frames. Usually, the camera are defined in sub-frame to have a better control of their degree of freedoms.</p>
	<p>CAM keyword may be followed by these measurements:</p>
	<ul>
		<li>UVEC</li>
		<li>UVD</li>
	</ul>
	<h3>Usage:</h3>
	<pre>
    *CAM cam_position  instr_ID [TRGT changed_tgt_ID] [ICSE sigma_instr_centering_mm]
    -----
    Mandatory arguments:
    cam_position:        Point on which the instrument is placed. Must be defined in the point section.
    instr_ID:            Name of the instrument as defined in the instrument section with a CAMD.
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
    ICSE:                Overrides the default Instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].
	<pre/>
	<h3>Example:</h3>
	<pre>
    *CAM      ST1    BCAM1   
    *CAM      ST2      BCAM1    TRGT   Prism2
    *CAM      ST3      BCAM1    ICSE   0.2
    *CAM      ST4      BCAM1    TRGT   Prsim2   ICSE     0.2
	</pre>
    <p>A CAM (UVEC or UVD) measurement is defined by an unit vector from the cam_position (S) to the target (T).</p>
    <p>The unit vector uses the axis of the sub-frame in which the observation is defined.</p>
)"""));

inline const QString _ANNOTATION_UVEC = _ANNOTATE(QObject::tr(R"""(<h2>UVEC</h2>
	<p>Measurement of a spatial direction vector made by a camera, therefore the expected vector is a  <em>unit vector</em>.</p>
	<p>Measurement is made along the z-axis and thus the Z component of the vector can not be 0 and also, one group of UVEC measurements has to have the same sign of this Z component.</p>
	<h3>Usage:</h3>
	<pre>
    *UVEC [TRGT changed_tgt_ID]
    point obs_x   obs_y     z_comp [TRGT changed_tgt_ID]   [TCSE sigma_tgt_centering_mm] [XSE sigma_x]   [YSE sigma_y]  [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_x:               Value of the X-component of the unit vector [m].
    obs_y:               Value of the Y-component of the unit vector [m].
    z_comp:              Value of the Z-component of the unit vector [m].
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *UVEC line it applies for all the measurement of the section, otherwise only for the observation.  
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    XSE:                 Overrides the default X-component 1-sigma precision for this observation. The flag XSE must be followed by a value [mm/m].
    YSE:                 Overrides the default Y-component 1-sigma precision for this observation. The flag YSE must be followed by a value [mm/n].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *UVEC
    PT1      -4.820124e-005     4.193510e-003   -9.999912e-001
    PT2       5.811140e-003    -2.980641e-002   -9.995388e-001    TRGT    Prism2   TCSE   0.050   XSE   0.15    YSE   0.20

    *UVEC     TRGT     Prism2
    PT1      -4.820124e-005     4.193510e-003    9.999912e-001    TCSE    0.1
    PT2       5.811140e-003    -2.980641e-002    9.995388e-001    XSE     0.20
	</pre>
    <p>The degree of freedoms of the UVEC is defined by two factors:</p>
	<ul>
	<li>The type of point on which the camera is placed. A POIN will for instance allow the instrument to move only in translations. A CALA will constrain fully the instrument.</li>
	<li>The degree of freedoms of the sub-frame.</li></ul>
	<p>In normal use, an UVD is define within a sub-frame on a CALA point also declared in this sub-frame. This configuration allows to control the translations and rotations to be redetermined by the LS adjustment.</p>
	<h3>Example:</h3>
	<pre>
    *FRAME   CAMERA_MEAS   1000    1000    2000    20    15    250   1   TX   TY   TZ   RX   RY   RZ
        *CALA     ST1      0.001   0.080   0.100
        *CAM      ST1      BCAM1
        *UVEC
        PT1      -4.820124e-005     4.193510e-003   -9.999912e-001
        PT2       5.811140e-003    -2.980641e-002   -9.995388e-001    TRGT    Prism2   TCSE   0.050   XSE   0.15    YSE   0.20

        *UVEC     TRGT     Prism2
        PT3      -4.820124e-005     4.193510e-003    9.999912e-001    TCSE    0.1
        PT4       5.811140e-003    -2.980641e-002    9.995388e-001    XSE     0.20
    *ENDFRAME
	</pre>
	<p>In the example, the 3 translations and rotations of the camera can be computed by the LS adjustment.</p>
	<p>Other more complex configuration can be used (ex: OBSXYZ,etc.)</p>
)"""));

inline const QString _ANNOTATION_UVEC_POINT = _ANNOTATE(QObject::tr(R"""(<h3>UVEC POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point obs_x  obs_y   z_comp [TRGT changed_tgt_ID]  [TCSE sigma_tgt_centering_mm] [XSE sigma_x]  [YSE sigma_y]  [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_x:               Value of the X-component of the unit vector [m].
    obs_y:               Value of the Y-component of the unit vector [m].
    z_comp:              Value of the Z-component of the unit vector [m].
    Optional arguments:
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    XSE:                 Overrides the default X-component 1-sigma precision for this observation. The flag XSE must be followed by a value [mm/m].
    YSE:                 Overrides the default Y-component 1-sigma precision for this observation. The flag YSE must be followed by a value [mm/n].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *UVEC
    PT1      -4.820124e-005     4.193510e-003   -9.999912e-001
    PT2       5.811140e-003    -2.980641e-002   -9.995388e-001    TRGT    Prism2   TCSE   0.050   XSE   0.15    YSE   0.20

    *UVEC     TRGT     Prism2
    PT1      -4.820124e-005     4.193510e-003    9.999912e-001    TCSE    0.1
    PT2       5.811140e-003    -2.980641e-002    9.995388e-001    XSE     0.20
	</pre>
)"""));

inline const QString _ANNOTATION_UVD = _ANNOTATE(QObject::tr(R"""(<h2>UVD</h2>
	<p>Measurement of a spatial direction vector and a spatial distance made by a camera.</p>
	<p>The measured vector has to be a  <em>unit vector</em>. The observations are: X and Y components of the vector and the spatial distance.</p>
	<p>Measurement is made along the z-axis and thus the Z component of the vector can not be 0 and also, one group of UVD measurements has to have the same sign of this Z component.  </p>
	<h3>Usage:</h3>
	<pre>
    *UVD [TRGT changed_tgt_ID]
    point obs_x obs_y z_comp obs_dist_m [TRGT changed_tgt_ID]  [TCSE sigma_tgt_centering_mm] [XSE sigma_x] [YSE sigma_y] [DSE sigma_dist_mm]  [ID observationID]
    -----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_x:               Value of the X-component of the unit vector [m].
    obs_y:               Value of the Y-component of the unit vector [m].
    z_comp:              Value of the Z-component of the unit vector [m].
    obs_dist_m:          Value of the observed spatial distance [m].
    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *UVEC line it applies for all the measurement of the section, otherwise only for the observation.  
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    XSE:                 Overrides the default X-component 1-sigma precision for this observation. The flag XSE must be followed by a value [mm/m].
    YSE:                 Overrides the default Y-component 1-sigma precision for this observation. The flag YSE must be followed by a value [mm/n].
    DSE:                 Overrides the default spatial distance 1-sigma precision for this observation. The flag DSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *UVD
    PT1      -4.820124e-005     4.193510e-003   -9.999912e-001    10
    PT2       5.811140e-003    -2.980641e-002   -9.995388e-001    25     TRGT    Prism2   TCSE   0.050   XSE   0.15    YSE   0.20   DSE   0.01

    *UVD     TRGT     Prism2
    PT1      -4.820124e-005     4.193510e-003    9.999912e-001    10     TCSE    0.1
    PT2       5.811140e-003    -2.980641e-002    9.995388e-001    25     XSE     0.20
	</pre>
	<h3>Note:</h3>
	<p>See the UVEC chapter for the way to use the UVD.</p>
)"""));

inline const QString _ANNOTATION_UVD_POINT = _ANNOTATE(QObject::tr(R"""(<h3>UVD POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point obs_x obs_y z_comp obs_dist_m [TRGT changed_tgt_ID]  [TCSE sigma_tgt_centering_mm] [XSE sigma_x] [YSE sigma_y] [DSE sigma_dist_mm] [ID observationID]
    -----
    point:               Name of the point measured. Must be defined in the point section.
    obs_x:               Value of the X-component of the unit vector [m].
    obs_y:               Value of the Y-component of the unit vector [m].
    z_comp:              Value of the Z-component of the unit vector [m].
    obs_dist_m:          Value of the observed spatial distance [m].

    Optional arguments:
    TRGT:                Overrides the default target. The flag TRGT must be followed by the name of a target defined in the instrument section.
                         If the flag is defined at the *UVEC line it applies for all the measurement of the section, otherwise only for the observation.  
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    XSE:                 Overrides the default X-component 1-sigma precision for this observation. The flag XSE must be followed by a value [mm/m].
    YSE:                 Overrides the default Y-component 1-sigma precision for this observation. The flag YSE must be followed by a value [mm/n].
    DSE:                 Overrides the default spatial distance 1-sigma precision for this observation. The flag DSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *UVD
    PT1      -4.820124e-005     4.193510e-003   -9.999912e-001    10
    PT2       5.811140e-003    -2.980641e-002   -9.995388e-001    25     TRGT    Prism2   TCSE   0.050   XSE   0.15    YSE   0.20   DSE   0.01

    *UVD     TRGT     Prism2
    PT1      -4.820124e-005     4.193510e-003    9.999912e-001    10     TCSE    0.1
    PT2       5.811140e-003    -2.980641e-002    9.995388e-001    25     XSE     0.20
	</pre>
)"""));

//// EDM TYPE
////// DSPT
inline const QString _ANNOTATION_DSPT = _ANNOTATE(QObject::tr(R"""(<h2>DSPT</h2>
	<p>Defines a spatial (i.e. euclidean) distance measurement between two points measured by an electronic distance meter.</p>
	<h3>Usage:</h3>
	<pre>
    *DSPT  stn   instr_ID   [TRGT changed_tgt_ID] [IHFIX] [ IH instr_heigth_m ] [IHSE sigma_instr_height_mm] [ICSE  sigma_instr_centering_mm]
    point  obs_dist_m  [TRGT changed_tgt_ID]  [OBSE sigma_mm]  [PPM ppm_mm]  [TH tgt_height_m] [THSE sigma_tgt_height_mm] [TCSE sigma_tgt_centering_mm]  [ID observationID]
    -----
    Mandatory arguments:
    stn:                 Name of the point on which the EDM is installed. Must be defined in the point section.
    instr_ID:            Name of the instrument as defined in the instrument section with a EDM.
    obs_dist_m:          Value of the observed spatial distance [m].
    Optional arguments:
    TRGT:                Option to change the default target to use for this station. The flag TRGT must be followed by the name of a target defined in the instrument section.
    IHFIX:               Since in DPST height never contributes to the measurements as it is always fixed this keyword is not required,
                         nonetheless it is still recommended for the sake of completeness if IH or IHSE are specified.  
    IH:                  Instrument height. The flag must be followed by the value [m]. If no IH is defined, the value defined for in the instrument section is taken.
    IHSE:                Instrument height sigma. The flag must be followed by the value [mm]. If no IHSE is defined, the value defined for in the instrument section is taken.
    ICSE:                Option to change the default 1-sigma precision of the centering of the instrument. The flag ICSE must be followed by a value [mm]. 
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm]. 
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    TH:                  Overrides the default target height for this observation. The flag TH must be followed by a value [m].  
    THSE:                Overrides the default target height 1-sigma precision for this observation. The flag THSE must be followed by a value [mm]. 
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *DSPT   ST1   emd1
    PT1     0.1
    PT2     0.2     TRGT     Prism2      OBSE 0.1      PPM      2      TH     0.5     IHSE     0.1         TCSE     0.2

    *DSPT   ST1   emd1    TRGT     Prism2     IHFIX    IH     0     IHSE     0.1     ICSE     0.2 
    PT1     0.1
    PT2     0.2   TRGT   Prism2    OBSE 0.1    PPM    2    TH   0.5   IHSE   0.1      TCSE   0.2
	</pre>
)"""));

inline const QString _ANNOTATION_DSPT_POINT = _ANNOTATE(QObject::tr(R"""(<h3>DSPT POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point  obs_dist_m  [TRGT changed_tgt_ID]  [OBSE sigma_mm]  [PPM ppm_mm]  [TH tgt_height_m] [THSE sigma_tgt_height_mm] [TCSE sigma_tgt_centering_mm] [ID observationID]
    ----- 
    Mandatory arguments:
    obs_dist_m:          Value of the observed spatial distance [m].
    Optional arguments:
    TRGT:                Option to change the default target to use for this station. The flag TRGT must be followed by the name of a target defined in the instrument section.  
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm]. 
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km]. 
    TH:                  Overrides the default target height for this observation. The flag TH must be followed by a value [m].  
    THSE:                Overrides the default target height 1-sigma precision for this observation. The flag THSE must be followed by a value [mm]. 
    TCSE:                Overrides the default target centering 1-sigma precision for this observation. The flag TCSE must be followed by a value [mm].  
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *DSPT   ST1   emd1
    PT1     0.1
    PT2     0.2   TRGT   Prism2    OBSE 0.1    PPM    2    TH   0.5   IHSE   0.1      TCSE   0.2

    *DSPT   ST1   emd1   TRGT   Prism2    IH   0   IHSE   0.1   ICSE   0.2 
    PT1     0.1
    PT2     0.2   TRGT   Prism2    OBSE 0.1    PPM    2    TH   0.5   IHSE   0.1      TCSE   0.2
	</pre>
)"""));

//// LEVEL TYPE
////// DLEV
inline const QString _ANNOTATION_DLEV = _ANNOTATE(QObject::tr(R"""(<h2>DLEV</h2>
	<p>Levelling observation that defines a distance of a point from a horizontal plane.</p>
	<p>The measurement can only appear in the ROOT frame.</p>
	<p>The position of the station must be known or &ldquo;computable&rdquo;. It allows LGC2 to take in account the vertical plane of the instrument and to correct the observation with respect to the datum option chosen (see examples at the end of the section).</p>
	<p>The measurement is based on the vertical plane of the levelling instrument therefore so position of the station must be known, at least approximately.</p>
	<h3>Usage:</h3>
	<pre>
    *DLEV instr_ID [RefPt reference_point]
    point obs_m [TRGT change_tgt_ID] [OBSE sigma_mm] [PPM ppm_mm] [TH staff_offset_m] [THSE sigma_staff_offset_mm] [DHOR dist_m  DSE sigma_mm] [ID observationID]
    -----  
    Mandatory arguments:
    instr_ID:            Name of the instrument as defined in the instrument section with a LEVEL.
    point:               Name of the point measured. Must be defined in the point section.
    obs_m:               Obseverd vertical distance from the bottom of the staff [m].
    Optional arguments:
	RefPt:               Possible configurations are described <a href="https://confluence.cern.ch/display/SUS/LGC2+User+Guide#LGC2UserGuide-RefPtConfiguration">here</a>.</p>
    TRGT:                Option to change the default target to use for this station. The flag TRGT must be followed by the name of a target defined in the instrument section.  
    OBSE:                Overrides the default distance 1-sigma precision this vertical distance observation. The flag OBSE must be followed by a value [mm]. 
    PPM:                 Overrides the default ppm value for this vertical distance observation. The flag PPM must be followed by a value [mm/km].  
    TH:                  Overrides the default vertical offset of the staff for this observation. The flag TH must be followed by a value [m].  
    THSE:                Overrides the default vertical offset of the staff 1-sigma precision for this observation. The flag THSE must be followed by a value [mm]. 
    DHOR:                Horizontal distance from the station to the point if measured. The flag DHOR must be followed by a value [m]. The flag DSE is mandatory if DHOR is used.
    DSE:                 1-Sigma precision for the horizontal distance. The flag DHOR must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *DLEV     DNA03.331089
    LHC.MB.B10L1.S    0.672320       TH 0.070000   DHOR     12.76400   DSE 0.02  OBSE 0.02
    LHC.MB.A10L1.E    0.612780
    LHC.MB.A10L1.M    0.549290       TRGT Staff2    OBSE 0.02    PPM  0.1    TH 0.070000     THSE 0.06     DHOR       2.85200  DSE 100
    LHC.MB.A10L1.S    0.481250       TRGT Staff2    OBSE 0.02    PPM 0.1     TH 0.070000     THSE 0.06
    
    *DLEV     DNA03.331089    RefPt STN.253708
    LHC.MB.B10L1.S    0.672320       TH 0.070000   DHOR     12.76400   DSE 0.02  OBSE 0.02
    LHC.MB.A10L1.E    0.612780
    LHC.MB.A10L1.M    0.549290       TRGT Staff2    OBSE 0.02    PPM  0.1    TH 0.070000     THSE 0.06     DHOR       2.85200  DSE 100
    LHC.MB.A10L1.S    0.481250       TRGT Staff3    OBSE 0.02    PPM 0.1     TH 0.070000     THSE 0.06></pre>
	<p>For the last square computation, the position of the station needs to either be known using the flag &quot;RefPt&quot; or computable from the horizontal distance &quot;DHOR&quot; on the measure point.</p>
	<p>Several configuration are possible:</p>
	<ul>
	<li>RefPt only</li>
	<li>DHOR only</li>
	<li>RefPt and DHOR</li></ul>
	<p>Those configurations are described under <a href="https://confluence.cern.ch/display/SUS/LGC2+User+Guide#LGC2UserGuide-RefPtConfiguration">this</a> link.</p>
	<p>In practice for normal machine operation, the most stable configuration seems to be the RefPt only where the station is either known in position or pre-computed before inserted in the LGC2 input file.</p>
)"""));

inline const QString _ANNOTATION_DLEV_POINT = _ANNOTATE(QObject::tr(R"""(<h3>DLEV POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point obs_m [TRGT change_tgt_ID] [OBSE sigma_mm] [PPM ppm_mm] [TH staff_offset_m] [THSE sigma_staff_offset_mm] [DHOR dist_m  DSE sigma_mm] [ID observationID]
    -----  
    Mandatory arguments:
    instr_ID:            Name of the instrument as defined in the instrument section with a LEVEL.
    point:               Name of the point measured. Must be defined in the point section.
    obs_m:               Obseverd vertical distance from the bottom of the staff [m].
    Optional arguments:
    TRGT:                Option to change the default target to use for this station. The flag TRGT must be followed by the name of a target defined in the instrument section.  
    OBSE:                Overrides the default distance 1-sigma precision this vertical distance observation. The flag OBSE must be followed by a value [mm]. 
    PPM:                 Overrides the default ppm value for this vertical distance observation. The flag PPM must be followed by a value [mm/km].  
    TH:                  Overrides the default vertical offset of the staff for this observation. The flag TH must be followed by a value [m].  
    THSE:                Overrides the default vertical offset of the staff 1-sigma precision for this observation. The flag THSE must be followed by a value [mm]. 
    DHOR:                Horizontal distance from the station to the point if measured. The flag DHOR must be followed by a value [m]. The flag DSE is mandatory if DHOR is used.
    DSE:                 1-Sigma precision for the horizontal distance. The flag DHOR must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
    <pre/>
	<h3>Example:</h3>
	<pre>
    *DLEV     DNA03.331089
    LHC.MB.B10L1.S    0.672320       TH 0.070000   DHOR     12.76400   DSE 0.02  OBSE 0.02
    LHC.MB.A10L1.E    0.612780
    LHC.MB.A10L1.M    0.549290       TRGT Staff2    OBSE 0.02    PPM  0.1    TH 0.070000     THSE 0.06     DHOR       2.85200  DSE 100
    LHC.MB.A10L1.S    0.481250       TRGT Staff2    OBSE 0.02    PPM 0.1     TH 0.070000     THSE 0.06
    
    *DLEV     DNA03.331089    RefPt STN.253708
    LHC.MB.B10L1.S    0.672320       TH 0.070000   DHOR     12.76400   DSE 0.02  OBSE 0.02
    LHC.MB.A10L1.E    0.612780
    LHC.MB.A10L1.M    0.549290       TRGT Staff2    OBSE 0.02    PPM  0.1    TH 0.070000     THSE 0.06     DHOR       2.85200  DSE 100
    LHC.MB.A10L1.S    0.481250       TRGT Staff3    OBSE 0.02    PPM 0.1     TH 0.070000     THSE 0.06
	</pre>
)"""));

//// SCALE TYPE
////// ECHO
inline const QString _ANNOTATION_ECHO = _ANNOTATE(QObject::tr(R"""(<h2>ECHO</h2>
	<p>Stands for &quot;ECart HOrizontal&quot; and defines measurements from points to a vertical plane.</p>
	<p>This measurement can only appear   in a ROOT frame.</p>
	<p>The plane is defined implicitly by the measured points. Since the observation plane is defined to be horizontal two distance measurements provide the minimum amount of information.</p>
	<h3>Usage:</h3>
	<pre>
    *ECHO scale_ID
     stn obs_m <ac:link><ri:page ri:content-title="SCALE D" /><ac:plain-text-link-body><![CDATA[[]]></ac:plain-text-link-body></ac:link>SCALE scale_instr_ID] [OBSE sigma_mm] [PPM ppm_mm]  [ICSE instr_centering_mm]  [ID observationID]
    -----
    Mandatory arguments:
    scale_ID:            Name of a scale as defined in the instrument section (*SCALE).
    stn:                 Name of the point on which the scale is installed. Must be defined in the point section.
    bs_m:                Observed offset distance value [m].
    Optional arguments:
    SCALE:               Overrides the default staff. The flag SCALE must be followed by the name of a staff defined in the instrument section.
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].    
    ICSE:                Overrides the default instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ECHO   R.FIDU
    PT1     0.10
    PT2     0.20   SCALE   Scale2
    PT3     0.50   OBSE    0.2
    PT4     1.01   PPM     0.1
    PT5     0.99   SCALE   Scale2     OBSE      0.2     PPM        0.1     ICSE        0.1
	</pre>
	<p>To read information about ECHO Sign convention follow <a href="https://confluence.cern.ch/display/SUS/LGC2+User+Guide#LGC2UserGuide-ECHOSignconvention">this</a> link.</p>
)"""));

inline const QString _ANNOTATION_ECHO_POINT = _ANNOTATE(QObject::tr(R"""(<h3>ECHO POINT</h3>
	<h3>Usage:</h3>
	<pre>
    stn obs_m [SCALE scale_instr_ID] [OBSE sigma_mm] [PPM ppm_mm]  [ICSE instr_centering_mm] [ID observationID]
    -----
    Mandatory arguments:
    stn:                 Name of the point on which the scale is installed. Must be defined in the point section.
    obs_m:               Observed offset distance value [m].
    Optional arguments:
    SCALE:               Overrides the default staff. The flag SCALE must be followed by the name of a staff defined in the instrument section.
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    ICSE:                Overrides the default instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].  
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ECHO   R.FIDU
    PT1     0.10
    PT2     0.20   SCALE   Scale2
    PT3     0.50   OBSE    0.2
    PT4     1.01   PPM     0.1
    PT5     0.99   SCALE   Scale2   OBSE    0.2   PPM     0.1   ICSE     0.1
	</pre>
	<p>To read information about ECHO Sign convention follow <a href="https://confluence.cern.ch/display/SUS/LGC2+User+Guide#LGC2UserGuide-ECHOSignconvention">this</a> link.</p>
)"""));

////// ECSP
inline const QString _ANNOTATION_ECSP = _ANNOTATE(QObject::tr(R"""(<h2>ECSP</h2>
	<p>Shortest spatial distance from a point (station) to a spatial line define by 2 points. This measurement can only appear in a ROOT frame.</p>
	<h3>Usage:</h3>
	<pre> 
    *ECSP A B scale_ID
    point obs_m [SCALE scale_instr_ID] [OBSE sigma_mm] [PPM ppm_mm] [ICSE instr_centering_mm] [ID observationID] 
    ----
    Mandatory arguments:
    A:                   flt: first point on the pline
    B:                   flt: second point on the line
    point:               Name of the point measured. Must be defined in the point section.
    obs_m:               Observed offset distance value [m].
    Optional arguments:
    SCALE:               Changes the scale ID for this measurement and the following
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    ICSE:                Overrides the default instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ECSP    LHC.MQ.23L8.E    LHC.MQ.23L8.S    SCALE
    LBE.HLG.35.E    0.1    SCALE SCALE1    OBSE    1    PPM    2    ICSE 3
    LBE.HLG.35.E    0.1
	</pre>
)"""));

inline const QString _ANNOTATION_ECSP_POINT = _ANNOTATE(QObject::tr(R"""(<h3>ECSP POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point obs_m [SCALE scale_instr_ID] [OBSE sigma_mm] [PPM ppm_mm] [ICSE instr_centering_mm] [ID observationID] 
    ----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_m:               Observed offset distance value [m].
    Optional arguments:
    SCALE:               Changes the scale ID for this measurement and the following
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    ICSE:                Overrides the default instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ECSP    LHC.MQ.23L8.E    LHC.MQ.23L8.S    SCALE
    LBE.HLG.35.E    0.1    SCALE SCALE1    OBSE    1    PPM    2    ICSE 3
    LBE.HLG.35.E    0.1
	</pre>
)"""));

////// ECVE
inline const QString _ANNOTATION_ECVE = _ANNOTATE(QObject::tr(R"""(<h2>ECVE</h2>
	<p>Offset from points to a vertical line. This measurement can only appear   in a ROOT frame. Two definition can be used.</p>
	<ol>
	<li>A point on the vertical is known or measured (use PtLine keyword to define it). In this case, the reference point is the same as the PtLine</li>
	<li>The point on the line is unknown. The reference point calculated as the mean of all provisonal stationed point and L is the mean of all estimated stationed point (variable in X and Y, Z = Zref).</li>
	</ol>
	<h3>Usage:</h3>
	<pre>
    *ECVE scale_ID   [PtLine point_on_line]
    point obs_m [SCALE scale_instr_ID] [OBSE sigma_mm] [PPM ppm_mm] [ICSE instr_centering_mm] [ID observationID] 
    ----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_m:               Observed offset distance value [m].
    Optional arguments:
    PtLine:              Point on the line measured or known (L)
    SCALE:               Changes the scale ID for this measurement and the following
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    ICSE:                Overrides the default instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ECVE    SCALE    PtLine    LBE.HLG.35.S
    LBE.HLG.35.E    0.1    SCALE    SCALE1    OBSE    1    PPM    2    ICSE    3
    LBE.HLG.35.E    0.1
	</pre>
)"""));

inline const QString _ANNOTATION_ECVE_POINT = _ANNOTATE(QObject::tr(R"""(<h3>ECVE POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point obs_m [SCALE scale_instr_ID] [OBSE sigma_mm] [PPM ppm_mm] [ICSE instr_centering_mm] [ID observationID] 
    ----
    Mandatory arguments:
    point:               Name of the point measured. Must be defined in the point section.
    obs_m:               Observed offset distance value [m].
    Optional arguments:
    SCALE:               Changes the scale ID for this measurement and the following
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    PPM:                 Overrides the default ppm value for this observation. The flag PPM must be followed by a value [mm/km].  
    ICSE:                Overrides the default instrument centering 1-sigma precision for this observation. The flag ICSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *ECVE    SCALE    PtLine    LBE.HLG.35.S
    LBE.HLG.35.E    0.1    SCALE    SCALE1    OBSE    1    PPM    2    ICSE    3
    LBE.HLG.35.E    0.1
	</pre>
)"""));

//// INCL
////// INCLY

inline const QString _ANNOTATION_INCLY = _ANNOTATE(QObject::tr(R"""(<h2>INCLY</h2>
    <p>Inclinometer observation that measure an angle with respect to the local vertical of the stationned point.</p>
    <p>The measurement cannot appear in the ROOT frame. The angle is measured clockwise around the Y axis of the frame in which the observation is defined.</p>
    <h3>Usage:</h3>
    <pre> 
    *INCLY instr_ID
    stn obs_gon [INSTR instr_ID] [OBSE sigma_angle_cc] [PPM sigma_ppm ] [AC aCorr_gon] [ACSE sigma_aCorr_cc] [RF refAngle_gon] [RFSE sigma_refAngle_cc] [ID observationID]
    -----
    Mandatory arguments:
    instr_ID:            Name of a incl as defined in the instrument section (*INCL).
    stn:                 Name of the point on which the incl is installed. Must be declared.
    obs_gon:             Observed angle distance value [gon].
    Optional arguments:
    INSTR:               Overrides the default incl instrument. The flag INSTR must be followed by the name of an INCL instrument defined in the instrument section.
                         The flag is defined on an observation line, it will apply for this observation and all the following ones in this round of measurement.   
    OBSE:                Overrides the default angle 1-sigma precision for this observation. The flag OBSE must be followed by a value [cc].
    PPM:                 Overrides the default ppm 1-sigma precision for this observation. The flag PPM must be followed by a value [μrad or μm/m].
    AC:                  Overrides the default angle correction observation. The flag AC must be followed by a value [gon].  
    ACSE:                Overrides the default angle correction 1-sigma precision for this observation. The flag ACSE must be followed by a value [cc].
    RF:                  Overrides the default reference angle correction observation. The flag RF must be followed by a value [gon].  
    RFSE:                Overrides the default referece angle correction 1-sigma precision for this observation. The flag RFSE must be followed by a value [cc]
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
    <pre/>
    <h3>Example:</h3>
    <pre>
    *INCLY test2
    PT1     -0.007
    PT1      0.007 AC 0.01
    PT2     -0.008 ACSE 50
    PT1     -0.006 INSTR test
    PT3     -0.009 INSTR test AC 0.01 ACSE 2 RF 2 RFSE 15 PPM 3
    PT1     -0.007 RF 10 PPM 3
    PT1      0.007 RFSE 20
    </pre> 
)"""));

//// HLSR
////// ECWS

inline const QString _ANNOTATION_ECWS = _ANNOTATE(QObject::tr(R"""(<h2>ECWS</h2>
    <p>"ECart Water Surface" observation  that measures a distance between an HLS sensor principal point and a water surface .</p>
    <p>The measurement and ECWS keyword  can only appear in the ROOT frame.</p>
    <p>Each ECWS defines 1 water surface. All the observations associated to this water surface and participating in determination in its water surface height must be defined under the same ECWS keyword. The water surface height cannot be initialized to a value or fixed. LGC routine computes an approximative water surface height for each ECWS keyword for the first iteration based on an average of the HLS sensor point heights observed in this round of measurement.</p>
    <p>The observation is a oriented distance (Can be negative) that follows the Z (or local vertical) of where the HLS point is declared.  If the point is declared in the root frame, the distance follows the local vertical or the Z (in OLOC case). if the point in declared inside a FRAME, the distance will follow the Z axis of this frame.</p>
    <p>A positive observation goes along the Z+ axis starting from the HLS sensor point. A negative value goes along Z- axis starting from the HLS sensor point.</p>
    <h3>Usage:</h3>
    <pre> 
    *ECWS    instr_ID    sigma_water_surface_mm    [WSID    water_ID]
    stn obs_m [INSTR hlsr_instr_ID] [OBSE sigma_mm] [IHSE sigma_instr_height_mm]  [ICSE instr_centering_mm]  [WSSE sigma_water_surface_mm] [ID observationID] 
    -----
    Mandatory arguments:
    instr_ID:            Name of a incl as defined in the instrument section (*HLSR).
    stn:                 Name of the point on which the hlsr is installed. Must be declared.
    obs_m:               Observed distance value [mm].
    Optional arguments:
    INSTR:               Overrides the default incl instrument. The flag INSTR must be followed by the name of an HLSR instrument defined in the instrument section.
                         The flag is defined on an observation line, it will apply for this observation and all the following ones in this round of measurement.   
    OBSE:                Overrides the default distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    IHSE:                Instrument height sigma. The flag must be followed by the value [mm]. If no IHSE is defined, the value defined for in the instrument section is taken.
    ICSE:                Instrument centering sigma. The flag must be followed by the value [mm]. If no ICSE is defined, the value defined for in the instrument section is taken.
    WSSE:                Overrides the default distance 1-sigma precision of the water surface for this observation. The flag WSSE must be followed by a value [mm].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
    <pre/>
    <h3>Example:</h3>
    <pre>
*TITR
T7
*OLOC
*PREC 6
*INSTR
*HLSR HL_SNSR_1   0.03  0     0
*HLSR HL_SNSR_2   0.05  0.02  1
*CALA
PT1   4485   5033   330
*VZ 
PT2   4485   5033   331
PT6   4451   4991   331
PT7   4492   5054   331
*ECWS    HL_SNSR_1    0.02    WSID    WATER3001
PT1   0.401   INSTR   HL_SNSR_2
PT2   0.502   OBSE    1
PT3   0.501   IHSE    2
PT4   0.605   ICSE    3          
PT5   0.701   WSSE    4
PT6   0.803   INSTR   HL_SNSR_1
PT7   0.902     
*ECWS    HL_SNSR_1    0.03    
PT1   0.402
PT2   0.501
PT3   0.503
*FRAME F1  4489   5046   2329    0      0   0  1  TZ
*CALA
PT3   0   0  -0.5
*ENDFRAME
*FRAME F2  4489   5046   2329    200    0   0  1  TZ
*CALA
PT4   0  0   1.1
PT5   0  0   1
*ENDFRAME
*END
</pre> 
)"""));


//// WPSR
////// ECWI

inline const QString _ANNOTATION_ECWI = _ANNOTATE(QObject::tr(R"""(<h2>ECWI</h2>
    <p>"ECart WIre" observation  that measures two distance between a WPS sensor origin and a wire.</p>
    <p>The ECWI keyword can only appear in the ROOT frame.</p>
    <h3>Wire parameters</h3>
    <p>Each ECWI defines 1 wire. All the observations associated to this wire and contributing to the wire parameters determination must be defined under the same ECWI keyword.</p>
    <p>A wire introduces 5 parameters, its: bearing, slope, 2 translations and 1 sag values. Only the sag can be fixed to a provisional value in the LS computation using the SAGFIX Flag.</p>
    <p>The wire is modelled using the following sequences of frames:</p>
    <pre>
    *FRAME  WireFirst       XRefWire    YRefWire    ZRefWire    0       0   Bearing 1
    *FRAME  WireSecond      DRefX       0           DRefZ       Slope   0   0       1
    </pre>
    <p>Where:</p>
	<ul>
    <li>XRefWire YRefWire ZRefWire are fixed values defined as the middle of the 2 anchor points provisional values</li>
    <li>Bearing, slope are 2 variable angles of the wire</li>
    <li>DRefX DRefZ are 2 variable translations defined in the WireFirst Frame.</li>
    </ul>
    <h3>Observations FRAME</h3>
    <p>An ECWI observation is an oriented distance (Can be negative) that follows the X-axis and Z-axis of where the station point is declared. An ECWI keyword can welcome observations defined in different frames.</p>
    <h3>Usage:</h3>
    <pre> 
    *ECWI    instr_ID    sag_distance_m    sigma_wire_mm   anchorpoint_1   anchorpoint_2    [WIID  wire_ID]    [SAGFIX [SAGSE sigma_sag_mm]]
    stn obs_X_m obs_Z_m [INSTR wpsr_instr_ID] [XSE sigma_dist_X_mm] [ZSE sigma_dist_Z_mm]  [XICSE sigma_instr_centering_X_mm]  [ZICSE sigma_instr_centering_X_mm] [ID observationID]
    -----
    instr_ID:            Name of a wps as defined in the instrument section (*WPSR).
    stn:                 Name of the point on which the wps is installed. Must be declared.
    obs_X_m:             Observed X distance value from the station point to the wire [m].
    obs_Z_m:             Observed Z distance value from the station point to the wire [m].
    sag_distance_m:      Provisional value for the sag value defined in the middle of the 2 anchor points [m]. By default the sag is a parameter to determine by the LS computation. Can be fixed using the SAGFIX flag.
    sigma_wire_mm:       1-sigma precision for this wire [mm].
    anchorpoint_1:       Name of the first anchor points. Must be declared.
    anchorpoint_2:       Name of the second anchor points. Must be declared.
    Optional arguments:
    INSTR:               Overrides the default wps instrument. The flag INSTR must be followed by the name of an HLSR instrument defined in the instrument section.
                         The flag is defined on an observation line, it will apply for this observation and all the following ones in this round of measurement.   
    XSE:                 Overrides the default distance 1-sigma precision for this X distance observation. The flag XSE must be followed by a value [mm].
    ZSE:                 Overrides the default distance 1-sigma precision for this Z distance observation. The flag ZSE must be followed by a value [mm].
    XICSE:               Instrument centering sigma along the station X-axis coordinate system. The flag must be followed by the value [mm]. If no XICSE is defined, the value defined for in the instrument section is taken.
    ZICSE:               Instrument centering sigma along the station Z-axis coordinate system. The flag must be followed by the value [mm]. If no XICSE is defined, the value defined for in the instrument section is taken.
    WIID:                Assign a name to the wire round of measurements (ROM). Name must be unique between all the ECWI ROMs. If not given, LGC will automatically assign one. The flag WIID must be followed by a string [str].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method. 
    SAGFIX:              Indicates the sag value is fixed, if so one other flag can be defined:
                         SAGSE : 1-sigma precision for this wire sag [mm]. The flag must be followed by the value [mm]. If no SAGSE  is defined, the sag precision will be defined to 0. SAGSE is ignored if the SAGFIX flag is not present.
    </pre>
    <h3>Example:</h3>
    <pre>
*TITR  
Test   
*OLOC      
*INSTR 
*WPSR WP_SNSR_1 0.03    0.03    0   0  
*WPSR WP_SNSR_2 0.01    0.01    0.02    0.02   
*CALA  
PT0         0   0   0
*ECWI  WP_SNSR_1    0   0.001   PT0 PT5
PTMINUS     0.65    1.61    XSE 0.05
PT0         0.75    0.75       
PT1         -0.85   -0.21   XICSE 0.05 
PT2         -0.95   1.01    ZICSE 0.05
PT3         1.05    0.09    ID TEST
PT4         -1.15   0.51    ZSE 0.05
PT5         1.25    1.25   
*ECWI   WP_SNSR_2   0.9999  0.001   PT5 PT0     WIID    WPS2    SAGFIX  SAGSE   0.1
PTMINUS     0.65    1.61       
PT0         0.75    0.75    % Point declared in the Root --> observations follow the X and Z Axis of the ROOT frame 
PT1         -0.85   -0.21   % Point declared in a FRAME  --> observations follow the X and Z Axis of the subframe   
PT2         -0.95   1.01       
PT3         1.05    0.09       
PT4         -1.15   0.51    INSTR WP_SNSR_1
PT5         1.25    1.25       
*FRAME A-TAP.THEO 0 0 0 0 0 0 1
*FRAME FRAME_PTMINUS 0 -1 0 0 0 0 1
*CALA  
PTMINUS 0 0 0  
*ENDFRAME      
*FRAME FRAME_PT1 0 -1 0 0 200 0 1  
*CALA  
PT1 0 2 0  
*ENDFRAME  
*FRAME FRAME_PT2 0 2 0 0 200 0 1 TX TZ 
*CALA  
PT2 -1 0 -2
*ENDFRAME  
*FRAME FRAME_PT3 -1 0 2 0 0 0 1
*CALA  
PT3 1 3 -2 
*ENDFRAME  
*FRAME FRAME_PT4 0 4 0 0 0 200 1   
*CALA  
PT4 0 0 0  
*ENDFRAME  
*FRAME FRAME_PT5 0 5 0 0 0 0 1 
*POIN  
PT5 0 0 0
*OBSXYZ
PT5 0 0 0 0.2 0.3 0.5  
*ENDFRAME  
*ENDFRAME  
*END
    </pre> 
)"""));

//// NO TYPE
////// DVER
inline const QString _ANNOTATION_DVER = _ANNOTATE(QObject::tr(R"""(<h2>DVER</h2>
	<p>Give   an horizontal difference (dh) between two points.</p>
	<p>This measurement can only appear   in a ROOT frame.</p>
	<p>Default distance correction value for the measurement is zero, it is used to eliminate the systematic error found in the previous calculation.</p>
	<p>DVER is used in a geodetic reference system (XYH).</p>
	<p>No LEVEL instrument is used.</p>
	<h3>Usage:</h3>
	<pre> 
    *DVER [sigma_mm]
    point1 point2 obs_m [OBSE sigma_mm] [DCOR correction_m] [ID observationID]
    ----- 
    Mandatory arguments:
    point1:              Name of the first point. Must be defined in the point section.
    point2:              Name of the second point. Must be defined in the point section.
    obs_m:               Observed offset distance value [m]. (point2 - point1)
    Optional arguments:
    sigma_mm:            1-sigma precision on the vertical measurement [mm]. (By default 1 mm, if not given )
    OBSE:                Distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    DCOR:                Distance correction value for the measurements. The flag DCOR must be followed by a value [m].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *DVER   0.1  
    ST1     PT1     0.10
    ST1     PT2     0.20   
    ST1     PT3     0.50   OBSE    0.2
    ST2     PT3     -0.5   DCOR    0.1
    PT2     PT4     -0.3   OBSE    0.2     DCOR        0.1
	</pre>
)"""));

inline const QString _ANNOTATION_DVER_POINT = _ANNOTATE(QObject::tr(R"""(<h3>DVER POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point1 point2 obs_m [OBSE sigma_mm] [DCOR correction_m] [ID observationID]
    ----- 
    Mandatory arguments:
    point1:              Name of the first point. Must be defined in the point section.
    point2:              Name of the second point. Must be defined in the point section.
    obs_m:               Observed offset distance value [m]. (point2 - point1)
    Optional arguments:
    sigma_mm:            1-sigma precision on the vertical measurement [mm]. (By default 1 mm, if not given )
    OBSE:                Distance 1-sigma precision for this observation. The flag OBSE must be followed by a value [mm].
    DCOR:                Distance correction value for the measurements. The flag DCOR must be followed by a value [m].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.  
	<pre/>
	<h3>Example:</h3>
	<pre>
    *DVER   0.1  
    ST1     PT1     0.10
    ST1     PT2     0.20   
    ST1     PT3     0.50   OBSE    0.2
    ST2     PT3     -0.5   DCOR    0.1
	</pre>
)"""));

////// RADI
inline const QString _ANNOTATION_RADI = _ANNOTATE(QObject::tr(R"""(<h2>RADI</h2>
	<p>Allow to give an constraint orientation for a point. This constraint must be defined in root.</p>
	<p>The sigma in mm correspond to the size of the &quot;corridor&quot; in which the point can move.</p>
	<h3>Usage:</h3>
	<pre>
    *RADI sigma_mm  [ACST constant_angle_gons]
    point bearing_gons [OBSE sigma_mm]  [ACST constant_angle_gons] [ID observationID]
    -----
    Mandatory arguments:
    sigma_mm:           1-sigma precision defining the corridor [mm].
    point:              Name of the point on which to apply the constraint. Must be defined in the point section.
    bearing_gons:       Constraint orientation [gon].
    Optional arguments:
    OBSE sigma_mm:      Overrides the 1-sigma precision for the corridor for this point. The flag OBSE must be followed by a value [mm].
    ACST:               Constant angle to be added to the given bearing [gon].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *RADI   0.1  
    PT1     100
    PT2     200   OBSE    0.1
    PT3     50    OBSE    0.3    ACST   100
	</pre>
)"""));

inline const QString _ANNOTATION_RADI_POINT = _ANNOTATE(QObject::tr(R"""(<h3>RADI POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point bearing_gons [OBSE sigma_mm] [ACST constant_angle_gons] [ID observationID]
    ----- 
    Mandatory arguments:
    sigma_mm:           1-sigma precision defining the corridor [mm].
    point:              Name of the point on which to apply the constraint. Must be defined in the point section.
    bearing_gons:       Constraint orientation [gon].
    Optional arguments:
    OBSE sigma_mm:      Overrides the 1-sigma precision for the corridor for this point. The flag OBSE must be followed by a value [mm].
    ACST:               Constant angle to be added to the given bearing [gon].
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *RADI   0.1  
    PT1     100
    PT2     200   OBSE    0.1
    PT3     50    OBSE    0.3    ACST   100
)"""));

////// OBSXYZ
inline const QString _ANNOTATION_OBSXYZ = _ANNOTATE(QObject::tr(R"""(<h2>OBSXYZ</h2>
	<p>This type of measurement can be equivalent to CMM measurements &rarr; 3 coordinates plus the 1-sigma precisions (see the figure).</p>
	<p>Allow to give an 3D constraint for a point.   This constraint can be defined in any frame (root or sub-frames).</p>
	<h3>Usage:</h3>
	<pre>
    *OBSXYZ
    point obsX_m obsY_m obsZ_m sigmaX_mm  sigmaY_mm  sigmaZ_mm  [ID observationID]
    -----
    Mandatory arguments:
    t:                    Name of the measured point. Must be defined in the point section.
    obsX_m:               Observation along the X-axis of the frame in which the measurement is defined.
    obsY_m:               Observation along the Y-axis of the frame in which the measurement is defined.
    obsZ_m:               Observation along the Z-axis of the frame in which the measurement is defined.
    sigmaX_mm:            1-sigma precision of the observation along the X-axis [mm].
    sigmaY_mm:            1-sigma precision of the observation along the Y-axis [mm].
    sigmaZ_mm:            1-sigma precision of the observation along the Z-axis [mm].
    Optional arguments:
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *OBSXYZ
    PT1    1    2   3     0.1   0.2   0.3
    PT2    100  200 300   0.01  10    0.001
	</pre>
	<p>Mainly three cases are emerging for this type of measurement:</p>
	<ul>
	<li>Use a previous computation XYZ and associated sigmas as an input for a new calcultation.</li>
	<li>Use the fiducialisation values with precisions as observations.</li>
	<li>In addition with frames, &quot;best fit&quot; routines.</li></ul>
	<h3>Note:</h3>
	<ul>
	<li>Observed coordinates <strong>must be</strong> provided in <strong>3D Cartesian XYZ</strong> even if the observations are defined in the root frame and the datum option is different than *OLOC.</li>
	<li>All the arguments must appear in the input file.</li></ul>
)"""));

inline const QString _ANNOTATION_OBSXYZ_POINT = _ANNOTATE(QObject::tr(R"""(<h3>OBSXYZ POINT</h3>
	<h3>Usage:</h3>
	<pre>
    point obsX_m obsY_m obsZ_m sigmaX_mm  sigmaY_mm  sigmaZ_mm [ID observationID]
    ----- 
    Mandatory arguments:
    point:               Name of the measured point. Must be defined in the point section.
    obsX_m:              Observation along the X-axis of the frame in which the measurement is defined.
    obsY_m:              Observation along the Y-axis of the frame in which the measurement is defined.
    obsZ_m:              Observation along the Z-axis of the frame in which the measurement is defined.
    sigmaX_mm:           1-sigma precision of the observation along the X-axis [mm].
    sigmaY_mm:           1-sigma precision of the observation along the Y-axis [mm].
    sigmaZ_mm:           1-sigma precision of the observation along the Z-axis [mm].
    Optional arguments:
    ID:                  Gives a unique identifier to the observation. The flag has to be set on all measurements where a user of monitoring application wants to use the updateMeasurement method.
	<pre/>
	<h3>Example:</h3>
	<pre>
    *OBSXYZ
    PT1    1    2   3     0.1   0.2   0.3
    PT2    100  200 300   0.01  10    0.001
	</pre>
)"""));
