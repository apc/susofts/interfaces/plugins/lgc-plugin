----------------------------------------------------------------------------
--
-- © Copyright CERN 2000-2024. All rigths reserved. This software is released under a CERN proprietary software licence.
-- Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
--
----------------------------------------------------------------------------

%parser Table_LGCInputLexerIO
%decl LGCInputLexerIO.hpp
%impl LGCInputLexerIO_gen.cpp

-- BASIC TOKENS
%token UNKNOWN "Unknown" -- Unrecognized token
%token EOL_SYMBOL "New line '\\n'"
%token COMMENTSTRING "Comment"
%token WORD "Word (Pointname?)"
%token INTEGER "Integer number"
%token DECIMAL "Decimal number"
%token QUOTE "\""
%token DOLLAR "$"

-- KEYWORDS
	%token KEYWORDS_START -- THIS MUST BE THE FIRST useful TOKEN
	%token TITLE "*TITR"
	%token END "*END"
	%token FIN "*FIN"
	-- Transformation Parameters
		%token TX "TX"
		%token TY "TY"
		%token TZ "TZ"
		%token RX "RX"
		%token RY "RY"
		%token RZ "RZ"
		%token SCL "SCL"
	-- Options
		-- Datum Options
			%token LEP "*LEP"
			%token OLOC "*OLOC"
			%token RS2K "*RS2K"
			%token SPHE "*SPHE"
		-- Calculation Options
			%token ALLFIXED "*ALLFIXED"
			%token PREC "*PREC" -- int
			%token SIMU "*SIMU" -- int
		-- Output Options
			%token APRI "*APRI"
			%token CHABA "*CHABA"
			%token COVAR "*COVAR"
			%token CONSI "*CONSI"
			-- CONSI Keywords
				%token CONSILIBR "LIBR"
			%token DEFA "*DEFA"
			%token EREL "*EREL" -- list of point pairs
			%token ERELFRAME "*ERELFRAME" -- two frame names
			%token FAUT "*FAUT" -- double double
			%token FMTP "*FMTP" -- COL|SEP "separator"
			-- FMTP Keywords
				%token FMTPSEP "SEP" -- "separator"
				%token FMTPCOL "COL"
			%token HIST "*HIST"
			%token JSON "*JSON"
			-- JSON Keywords
				%token JSONCOVAR "COVAR"
			%token NODUP "*NODUP"
			%token PDOR "*PDOR" -- point name
			%token PRES "*PRES"
			%token PUNC "*PUNC" -- PuncParameter
			-- PUNC parameters
				%token PUNCE "E"
				%token PUNCEE "EE"
				%token PUNCH "H"
				%token PUNCZ "Z"
				%token PUNCHZ "HZ"
				%token PUNCHN "HN"
				%token PUNCZHN "ZHN"
				%token PUNCT "T"
				%token PUNCOUT1 "OUT1"
				%token PUNCOUT3 "OUT3"
			%token SOBS "*SOBS"
	-- Instruments
		%token INSTR "*INSTR"
		%token CAMD "*CAMD"
		%token EDM "*EDM"
		%token LEVEL "*LEVEL"
		%token POLAR "*POLAR"
		%token SCALE "*SCALE"
		%token INCL "*INCL"
		%token HLSR "*HLSR"
		%token WPSR "*WPSR"
	-- Frames
		%token FRAMESTART "*FRAME"
		%token FRAMEEND "*ENDFRAME"	
		-- Frames parameter keywords
			%token FR_KEYWORDS_START -- must enclose the measure parameter keyword tokens
			%token FRSLAVE "SLAVE"
			%token FR_KEYWORDS_END -- must enclose the measure parameter keyword tokens
	-- Points
		%token CALA "*CALA"
		%token POIN "*POIN"
		%token VXY "*VXY"
		%token VXZ "*VXZ"
		%token VYZ "*VYZ"
		%token VZ "*VZ"
	-- Measurements
		-- CAMD type
			%token CAM "*CAM"
			-- CAM Measures
				%token CAMUVD "*UVD"
				%token CAMUVEC "*UVEC"
		-- EDM type
			-- EDM Measures
				%token DSPT "*DSPT"
		-- LEVEL type
			-- LEVEL Measures
				%token DLEV "*DLEV"
		-- POLAR type
			%token TSTN "*TSTN"
			-- TSTN Keywords
				%token TSTNV0 "*V0"
			-- POLAR Measures
				-- TSTN
					%token TSTNANGL "*ANGL"
					%token TSTNDHOR "*DHOR"
					%token TSTNDIST "*DIST"
					%token TSTNECDIR "*ECDIR"
					%token TSTNECTH "*ECTH"
					%token TSTNPLR3D "*PLR3D"
					%token TSTNZEND "*ZEND"
				-- Other
					%token ORIE "*ORIE"
		-- SCALE type
			-- SCALE Measures
				%token ECHO "*ECHO"
				%token ECVE "*ECVE"
				%token ECSP "*ECSP"
		-- INCL type
			-- INCL Measures
				%token INCLY "*INCLY"
		-- HLSR type
			-- HLSR Measures
				%token ECWS "*ECWS"
		-- WPSR type
			-- ECWI Measures
				%token ECWI "*ECWI"
		-- NO TYPE
			-- NO TYPE Measures
				%token DVER "*DVER"
				%token OBSXYZ "*OBSXYZ"
				%token RADI "*RADI"
		-- Measurement parameter keywords
		%token MSRID "ID" -- CAM/UVEC/UVD, EDM/DSPT, LEVEL/DLEV, POLAR/ANGL/DHOR/DIST/ECDIR/ECTH/ORIE/ZEND/PLR3D, SCALE/ECHO/ECSP/ECVE, INCL/INCLY, HLSR/ECWS, NO/DVER, NO/RADI, OBSXYZ, WPSR/ECWI
		%token MSR_KEYWORDS_START -- must enclose the measure parameter keyword tokens
		%token MSRAC "AC" -- INCL/INCLY
		%token MSRACSE "ACSE" -- INCL/INCLY
		%token MSRACST "ACST" -- POLAR/V0, NO/RADI
		%token MSRASE "ASE" -- POLAR/PLR3D
		%token MSRCST "CST" -- POLAR/ORIE
		%token MSRDCOR "DCOR" -- NO/DVER
		%token MSRDHOR "DHOR" -- LEVEL/DLEV
		%token MSRDSE "DSE" -- CAM/UVD, LEVEL/DLEV, POLAR/PLR3D
		%token MSRICSE "ICSE" -- CAM, EDM/DSPT, POLAR/ECDIR/ECTH/ORIE/TSTN, SCALE/ECHO/ECSP/ECVE, HLSR/ECWS
		%token MSRIH "IH" -- EDM/DSPT, POLAR/TSTN
		%token MSRIHFIX "IHFIX" -- POLAR/TSTN
		%token MSRIHSE "IHSE" -- EDM/DSPT, POLAR/TSTN, HLSR/ECWS
		%token MSRINSTR "INSTR" -- INCL/INCLY, WPSR/ECWI
		%token MSRPPM "PPM" -- EDM/DSPT, LEVEL/DLEV, POLAR/DHOR/DIST/ECDIR/ECTH/PLR3D, SCALE/ECHO/ECSP/ECVE, INCL/INCLY
		%token MSRPTLINE "PtLine" -- SCALE/ECVE
		%token MSRREFPT "RefPt" -- LEVEL/DLEV
		%token MSRRF "RF" -- INCL/INCLY
		%token MSRRFSE "RFSE" -- INCL/INCLY
		%token MSRROT3D "ROT3D" -- POLAR/TSTN
		%token MSRSAGFIX "SAGFIX" -- WPSR/ECWI
		%token MSRSAGSE "SAGSE" -- WPSR/ECWI
		%token MSRSCALE "SCALE" -- POLAR/ECDIR/ECTH, SCALE/ECHO/ECSP/ECVE
		%token MSRTCSE "TCSE" -- CAM/UVEC/UVD, EDM/DSPT, POLAR/ANGL/DHOR/DIST/ORIE/PLR3D/ZEND
		%token MSRTH "TH" -- EDM/DSPT, LEVEL/DLEV, POLAR/DIST/PLR3D/ZEND
		%token MSRTHSE "THSE" -- EDM/DSPT, LEVEL/DLEV, POLAR/DIST/PLR3D/ZEND
		%token MSRTRGT "TRGT" -- CAM/CAM/UVEC/UVD, EDM/DSPT, LEVEL/DLEV, POLAR/ANGL/DHOR/DIST/ORIE/PLR3D/TSTN/V0/ZEND
		%token MSROBSE "OBSE" -- EDM/DSPT, LEVEL/DLEV, POLAR/ANGL/DHOR/DIST/ECDIR/ECTH/ORIE/ZEND, SCALE/ECHO/ECSP/ECVE, INCL/INCLY, HLSR/ECWS, NO/DVER, NO/RADI
		%token MSRWIID "WIID" -- WPSR/ECWI
		%token MSRWSID "WSID" -- HLSR/ECWS
		%token MSRWSSE "WSSE" -- HLSR/ECWS
		%token MSRXICSE "XICSE" -- WPSR/ECWI
		%token MSRXSE "XSE" -- CAM/UVEC/UVD, WPSR/ECWI
		%token MSRYSE "YSE" -- CAM/UVEC/UVD
		%token MSRZICSE "ZICSE" -- WPSR/ECWI
		%token MSRZSE "ZSE" -- POLAR/PLR3D, WPSR/ECWI
		%token MSR_KEYWORDS_END -- must enclose the measure parameter keyword tokens

-- STARTING POINT
%start LGCDocument

-- LGCInputLexerIO.hpp

/:
/**
 * /!\ This file is automatically generated by QLALR from LGCInputLexerIO.g.
 * /!\ Don't edit it!
 */

#ifndef LGCINPUTLEXERIO_HPP
#define LGCINPUTLEXERIO_HPP

#include <unordered_set>

#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/text/LexerParser.hpp>

#include "LGCMeasures.hpp"
#include "$header"

class LGCPlugin;

class LGCInputLexerIO : public LexerParser<$table>
{
	Q_OBJECT

public:
	LGCInputLexerIO(QObject *parent = nullptr);
	virtual ~LGCInputLexerIO() override = default;

	// QsciLexerCustom
	enum Styles
	{
		/** Default */
		Default,
		/** Comment / ignored part */
		Comment,
		/** Functional Comment / ignored part / used by external software */
		FunctionalComment,
		/** not special stuff */
		Normal,
		// keywords
		/** Start or end keyword */
		StartEnd,
		/** Option keywords */
		DatumOption,
		CalculationOption,
		OutputOption,
		OptionParameter,
		/** Intruments */
		InstrumentType,
		InstrumentName,
		TargetName,
		/** Points */
		PointType,
		PointName,
		PointCoord,
		/** Frames */
		Frame,
		FrameName,
		/** Measures (same as Options) */
		MeasureType,
		MeasureParameter = OptionParameter,
		FrameParameter = OptionParameter,
	};
	// implemented in LGCInputLexerIO_impl.cpp
	virtual const char *language() const override { return "LGCPoint"; }
	virtual const char *keywords(int set) const override;
	virtual QString description(int style) const override;
	virtual const char *wordCharacters() const override;
	virtual bool defaultEolFill(int style) const override;
	QColor defaultColor(int style) const override;
	QFont defaultFont(int style) const override;
	QColor defaultPaper(int style) const override;

	// IShareablePointsListIO
	// implemented in LGCInputLexerIO_impl.cpp
	virtual const std::string &getMIMEType() const override;
	virtual ShareablePointsList read(const std::string &contents) override;
	virtual ShareableExtraInfos readExtraInfos(const std::string &contents) override;
	virtual ShareableFrame readFrame(const std::string &contents) override;
	virtual ShareableParams readParams(const std::string &contents) override;
	virtual ShareablePoint readPoint(const std::string &contents) override;
	virtual ShareablePosition readPosition(const std::string &contents) override;
	virtual std::string write(const ShareablePointsList &list) override;
	virtual std::string write(const ShareableExtraInfos &) override { return std::string(); }
	virtual std::string write(const ShareableFrame &frame) override;
	virtual std::string write(const ShareableParams &params) override;
	virtual std::string write(const ShareablePoint &point) override;
	virtual std::string write(const ShareablePosition &position) override;

	// LexerParser
	virtual void parse() override;

	// LGCInputLexerIO
	/** Return the parsed list. This should never be used, except rare occasions. */
	ShareablePointsList &getCurList() noexcept { return _curList; }
	/** Return LGCMeasures. */
	const LGCMeasures &lgcMeasures() const noexcept { return _measures; }
	/** Returns the map with lines that have toggleable sigmas. */
	std::unordered_map<int, std::unordered_set<QString>> &getSigmaLines() { return _sigmaLines; }
	/** Returns the position of the *END keyword */
	int getEndPos() const noexcept { return _endPos; }

protected:
	// TextLexer
	virtual QString calltip(int position, int indicatorType, const QString &indicator) const override;
	// LexerParser
	virtual void endLexer() override;

private:
	// LexerParser
	// generated in in LGCInputLexerIO_gen.cpp
	int nextToken(QStringRef value) override;
	void consumeRule(int ruleno) override;
	
	/** Convenience method for adding info annotations only during the second run. */
	inline void addAnnotation(const QString& annotation);
	/** Convenience method for adding annotations for point types. */
	inline void addPointAnnotation();
	
	bool checkBool(int symidx, const QString &name, Styles style = Styles::Normal);
	int checkInt(int symidx, const QString &name, int min = 0, int max = 0, Styles style = Styles::Normal);
	double checkDouble(int symidx, const QString &name, double min = 0, double max = 0, Styles style = Styles::Normal);

	// used for parsing, uses sym(1) and _currentLine
	void setOption(const std::string &key, char sep = '\n');
	void setInstrument(bool checkTarget = true);
	void setTarget();
	void setMeasure(const QString &measureName);
	void setPoint(const QString &type);
	void setFrame(const QString &frameName);
	void jumpTo(const LGCMeasures::MeasureMap &map, const QString &type, int idx = 1, QString key = "");
	void countMeasure(const QString &type, int pointSymbolIdx = 1);
	void showMeasure(const QString &type, const QString &point, int idx = 1);
	void reportRepeated(const QString &type, int idx = 1);
	inline bool isRootFrame(const std::string &name) const;

	/** tells if we are in lexer mode */
	bool lexerMode() { return _lexerMode || _firstRun || _secondRun; }

private:
	// Auto completion
	class LGCLexerAPIs;
	friend LGCLexerAPIs;
	/** Inline contexts */
	enum InlineCxt
	{
		CONSI = LGCInputLexerIO::CONSI,
		FMTP = LGCInputLexerIO::FMTP,
		JSON = LGCInputLexerIO::JSON,
		PUNC = LGCInputLexerIO::PUNC,
		FRAMESTART = LGCInputLexerIO::FRAMESTART
	};
	/** Contexts */
	enum Context
	{
		FRAME,
		MEASURES
	};

private:
	/** List of point used during deserializing. This shouldn't be used outside as it may be unvalid. Used for parsing */
	ShareablePointsList _curList;
	/** Current frame during parsing */
	ShareableFrame *_curFrame = nullptr;
	/** TMP variable. Used for parsing */
	QString _curTmp;
	QString _curTmp2; // for ORIE
	QString _curTmpFrame; // for FRAMEs ENDFRAME indicator
	QString _curPointMeasure;
	const char _functionalComment = '$';
	// maps for ctrl+click
	LGCMeasures _measures;
	/** Some observations may be defined before the point definition appears (e.g. when using FRAMEs). They are counted and added when they finally appear. */
	std::unordered_map<QString, std::unordered_map<QString, int>> _missingPointObservations;
	/**
	 * True if we are in the first run.
	 * We run the parsing twice if we are in lexer mode to get all the point definitions in the first run and show errors in the second run if some points
	 * are used while they are not defined.
	 */
	bool _firstRun = false;
	/** True if we are in the second run. */
	bool _secondRun = false;
	/** used when (de)serializing and write a SPL in LGC format */
	bool _firstFrameWritten = false;
	/** used when (de)serializing and write a SPL in LGC format */
	bool _firstPointWritten = false;
	/** used to ignore any content appearing after *END or *FIN keyword */
	bool _isPostEnd = false;
	/** *END keyword position */
	int _endPos = -1;
	/** Mapping between lines and possible sigma keyword at the given line */
	std::unordered_map<int, std::unordered_set<QString>> _sigmaLines;
};

#endif // LGCINPUTLEXERIO_HPP
:/

-- LGCInputLexerIO_gen.cpp

/.
/**
 * /!\ This file is automatically generated by QLALR from LGCInputLexerIO.g.
 * /!\ Don't edit it!
 */

#include <QRegularExpression>

#include <Qsci/qsciscintilla.h>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePosition.hpp>

#include "tabs/inputs/annotations.hpp"
#include "tabs/inputs/LGCInputLexerIO.hpp"

#define ADDSTYLE(rule, style) ADDSTYLEIDX((rule), style, 1)
#define ADDSTYLEIDX(rule, style, symidx) \
	case (rule): \
		addStyleSym(Styles::style, (symidx)); \
		break

namespace {
inline std::string toTrimmedString(const QString &str, bool utf8)
{
	return toByteArray(str.trimmed(), utf8).constData();
}

inline std::string toTrimmedString(const QStringRef &str, bool utf8)
{
	return toByteArray(str.trimmed().toString(), utf8).constData();
}

inline QString toTrimmedQString(const QStringRef &str)
{
	return str.trimmed().toString();
}
} // namespace

void LGCInputLexerIO::addAnnotation(const QString& annotation)
{
	if(_secondRun && !annotation.isEmpty())
		_infoAnnotation += annotation;
}

void LGCInputLexerIO::addPointAnnotation()
{
	if (_secondRun && !_curTmp.isEmpty() && _ANNOTATION_POINTTYPES.count(_curTmp))
		addAnnotation(_ANNOTATION_POINTTYPES.at(_curTmp));
}

bool LGCInputLexerIO::checkBool(int symidx, const QString &name, Styles style)
{
	const double boo = sym(symidx).toDouble();
	if (boo != 0 && boo != 1)
	{
		QStringList error = {
			name + " must be a boolean value:",
			"0",
			"1"
		};
		throw ParserException(toTrimmedQString(sym(symidx)), error);
	}
	addStyleSym(style, symidx);
	return (bool) boo;
}

int LGCInputLexerIO::checkInt(int symidx, const QString &name, int min, int max, Styles style)
{
	const int sigma = sym(symidx).toInt();
	if (min != max && (sigma < min || sigma > max))
	{
		QStringList error = {name + " must be an integer:"};
		error += ("bigger than " + QString::number(min) + " and (equal or) smaller than " + QString::number(max));
		throw ParserException(toTrimmedQString(sym(symidx)), error);
	}
	addStyleSym(style, symidx);
	return sigma;
}

double LGCInputLexerIO::checkDouble(int symidx, const QString &name, double min, double max, Styles style)
{
	const double sigma = sym(symidx).toDouble();
	if (sigma < min || (min != max && sigma > max))
	{
		QStringList error = {name + " must be a decimal value:"};
		error += "more than " + QString::number(min);
		if (min != max)
			error += "less than " + QString::number(max);
		throw ParserException(toTrimmedQString(sym(symidx)), error);
	}
	addStyleSym(style, symidx);
	return sigma;
}

void LGCInputLexerIO::setOption(const std::string &key, char sep)
{
	if (!lexerMode())
	{
		std::string &option = _curList.getParams().extraInfos[key];
		if (!option.empty())
			option += sep;
		option += toTrimmedString(_currentLine, utf8());
	}
}

void LGCInputLexerIO::setInstrument(bool checkTarget)
{
	if (!lexerMode())
		return;

	const QString instr = toTrimmedQString(sym(2));
	_curTmp = instr;
	if (_firstRun)
	{
		if (contains(_measures.instruments, instr))
			reportRepeated("instrument", 2);
		_measures.instruments.emplace(instr, LGCMeasures::Value{sym(2).position(), instr, instr, _currentLine.toString(), toTrimmedQString(sym(1)), toTrimmedQString(sym(3)), _unusedComment});
	}
	else if (_secondRun && checkTarget)
		jumpTo(_measures.targets, "target", 3, instr + '/' + toTrimmedQString(sym(3)));
	_unusedComment.clear();
}

void LGCInputLexerIO::setTarget()
{
	if (_firstRun)
	{
		QString tgt = toTrimmedQString(sym(1));
		QString tgtid = _curTmp + '/' + tgt;
		if (contains(_measures.targets, tgtid))
			reportRepeated("target", 1);
		_measures.targets.emplace(tgtid, LGCMeasures::Value{sym(1).position(), tgtid, tgt, _currentLine.toString(), "", _curTmp, _unusedComment});
	}
	_unusedComment.clear();
}

void LGCInputLexerIO::setMeasure(const QString &measureName)
{
	if (_firstRun)
	{
		QString distinction = _curTmp.isEmpty() && !_curTmpFrame.isEmpty() ? _curTmpFrame.remove(0, _curTmpFrame.lastIndexOf('/') + 1) : _curTmp;
		QString msrid = distinction + '/' + measureName;
		QString msrtype;
		int i = 0;
		
		while (contains(_measures.measures, msrid + '/' + QString::number(i)))
			i++;
		msrid += '/' + QString::number(i);
		if (contains(_measures.instruments, _curTmp))
			msrtype = _measures.instruments.at(_curTmp).type;
		_measures.measures.emplace(msrid, LGCMeasures::Value{sym(1).position(), msrid, measureName, _currentLine.toString(), msrtype, _curTmp, _unusedComment});
	}
	_unusedComment.clear();
}

void LGCInputLexerIO::setPoint(const QString &type)
{
	QString point = toTrimmedQString(sym(1));
	if (_firstRun && type != "*PDOR")
	{
		std::unordered_map<QString, int> counter = {};
		if (contains(_measures.points, point))
			reportRepeated("point", 1);
		else if (_missingPointObservations.find(point) != std::cend(_missingPointObservations))
		{
			counter = _missingPointObservations.at(point);
			_missingPointObservations.erase(point);
		}
		_measures.points.emplace(point, LGCMeasures::Value{sym(1).position(), point, point, _currentLine.toString(), type, "", _unusedComment, counter});
	}
	else if (_secondRun && type == "*PDOR")
	{
		jumpTo(_measures.points, "point");

		_measures.points[point].others = type;
	}
	else if (!lexerMode() && type != "*PDOR")
	{
		double x = sym(2).toDouble();
		double y = sym(3).toDouble();
		double z = sym(4).toDouble();
		ShareablePosition pos{};
		if (type == "*CALA")
			pos = {x, y, z, 0, 0, 0, false, false, false};
		else if (type == "*POIN")
			pos = {x, y, z, 0, 0, 0, true, true, true};
		else if (type == "*VXY")
			pos = {x, y, z, 0, 0, 0, true, true, false};
		else if (type == "*VXZ")
			pos = {x, y, z, 0, 0, 0, true, false, true};
		else if (type == "*VYZ")
			pos = {x, y, z, 0, 0, 0, false, true, true};
		else if (type == "*VZ")
			pos = {x, y, z, 0, 0, 0, false, false, true};

		// inline comment
		QStringRef line = _currentLine, id, dcum;
		int commentpos = -1;
		for (const auto &c : commentChars())
		{
			if (line.contains(c))
			{
				commentpos = line.indexOf(c);
				break;
			}
		}
		// functional comment
		if (line.contains(_functionalComment))
			commentpos = line.indexOf(_functionalComment);
		if (commentpos < 0)
			line.clear();
		else
		{
			if (line[commentpos] != '$')
				line = line.mid(commentpos + 1).trimmed();
			else
			{
				line = line.mid(commentpos + 1).trimmed();
				const auto &split = line.split(' ',
#if (QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
					QString::SplitBehavior::SkipEmptyParts
#else
					Qt::SplitBehaviorFlags::SkipEmptyParts
#endif
				);
				if (split.size() >= 2)
				{
					dcum = split[0];
					id = split[1];
					if (split.size() > 3)
						line = line.mid(split[2].position());
					else
						line.clear();
				}
			}
		}
		
		ShareableExtraInfos infos = {{"lgcType", toByteArray(type, utf8()).constData()}};
		if (!id.isEmpty())
			infos.addExtraInfo("id", toByteArray(id.toString(), utf8()).constData());
		if (!dcum.isEmpty())
			infos.addExtraInfo("DCUM", toByteArray(dcum.toString(), utf8()).constData());

		_curFrame->add(new ShareablePoint{
			toByteArray(point, utf8()).constData(),
			pos,
			toByteArray(line.toString(), utf8()).constData(),
			toByteArray(_unusedComment, utf8()).constData(),
			true,
			infos
		});
	}
	_unusedComment.clear();
}

void LGCInputLexerIO::setFrame(const QString &frameName)
{
	if (_firstRun)
	{
		if (contains(_measures.frames, frameName))
			reportRepeated("frame", 1);
		_measures.frames.emplace(frameName, LGCMeasures::Value{sym(0).position(), frameName, frameName, _currentLine.toString(), toTrimmedQString(sym(0)), "", _unusedComment});	}
	else if (!lexerMode())
	{
		ShareablePosition &translation = _curFrame->getTranslation();
		ShareablePosition &rotation = _curFrame->getRotation();
		
		// Frame position
		translation.x = sym(2).toDouble();
		translation.y = sym(3).toDouble();
		translation.z = sym(4).toDouble();
		rotation.x = sym(5).toDouble();
		rotation.y = sym(6).toDouble();
		rotation.z = sym(7).toDouble();
		_curFrame->setScale(sym(8).toDouble());
		
		QStringRef line = _currentLine, id, dcum;
		// Frame freedom
		// translation
		if (line.contains(LGCInputLexerIO::spell[LGCInputLexerIO::TX]))
			translation.isfreex = true;
		if (line.contains(LGCInputLexerIO::spell[LGCInputLexerIO::TY]))
			translation.isfreey = true;
		if (line.contains(LGCInputLexerIO::spell[LGCInputLexerIO::TZ]))
			translation.isfreez = true;
		// rotation
		if (line.contains(LGCInputLexerIO::spell[LGCInputLexerIO::RX]))
			rotation.isfreex = true;
		if (line.contains(LGCInputLexerIO::spell[LGCInputLexerIO::RY]))
			rotation.isfreey = true;
		if (line.contains(LGCInputLexerIO::spell[LGCInputLexerIO::RZ]))
			rotation.isfreez = true;
		// scale
		if (line.contains(LGCInputLexerIO::spell[LGCInputLexerIO::SCL]))
			_curFrame->isFreeScale(true);
	}
}

void LGCInputLexerIO::jumpTo(const LGCMeasures::MeasureMap &map, const QString &type, int idx, QString key)
{
	if (_secondRun)
	{
		QString name = toTrimmedQString(sym(idx));
		if (key.isEmpty())
			key = name;
		if (!contains(map, key))
			throw ParserException(name, {key + ' ' + type + " is not defined"});
		addIndicationSym(TextIndicator::INTERNAL, idx, map.at(key).position);
	}
}

void LGCInputLexerIO::countMeasure(const QString &type, int pointSymbolIdx)
{
	if (_firstRun)
	{
		QString pointname = toTrimmedQString(sym(pointSymbolIdx));
		std::unordered_map<QString, int> *usedMap;

		if (!contains(_measures.points, pointname))
			usedMap = &_missingPointObservations[pointname];
		else
			usedMap = &_measures.points.at(pointname).counter;

		if (type == "CAMUVEC")
			usedMap->operator[](type + " (/2)") += 2;
		else if (type == "CAMUVD" || type == "OBSXYZ")
			usedMap->operator[](type + " (/3)") += 3;
		else if (type == "TSTNPLR3D")
		{
			usedMap->operator[]("TSTNANGL")++;
			usedMap->operator[]("TSTNZEND")++;
			usedMap->operator[]("TSTNDIST")++;
		}
		else
			usedMap->operator[](type) += 1;

		if (!_curPointMeasure.isEmpty() && contains(_measures.points, _curPointMeasure))
			_measures.points.at(_curPointMeasure).counter[type]++;
	}
}

void LGCInputLexerIO::showMeasure(const QString &type, const QString &point, int idx)
{
	auto addMeasureMarker = [this, idx](TextMarker marker, const QString &errortype, int nbobs) -> void {
		const int pos = sym(idx).position();
		int line, index;
		editor()->lineIndexFromPosition(pos, &line, &index);
		const std::bitset<32> markers = editor()->markersAtLine(line);

		if (marker == TextMarker::ERROR
			|| (marker == TextMarker::WARNING && !markers[TextMarker::ERROR])
			|| (!markers[TextMarker::ERROR] && !markers[TextMarker::WARNING]))
			addMarker(editor(), pos, marker);

		static const QString statusstring = tr(R"""(<h1 style="color: %1;">%2</h1>
			<p style="color: %1;">Not enough observations for this type of point: %3.</p>)""");
		if (marker == TextMarker::INFO)
			_infoAnnotation += statusstring.arg("blue").arg(errortype, QString::number(nbobs));
		else
			_errorAnnotation += statusstring.arg("red").arg(errortype, QString::number(nbobs));
	};

	int count = _measures.points[point].totalCount();
	if (type == "*POIN" && count < 3)
			addMeasureMarker(TextMarker::WARNING, "Warning", count);
	else if ((type == "*VXY" || type == "*VXZ" || type == "*VYZ") && count < 2)
			addMeasureMarker(TextMarker::WARNING, "Warning", count);
	else if (type == "*VZ" && count < 1)
			addMeasureMarker(TextMarker::WARNING, "Warning", count);
}

void LGCInputLexerIO::reportRepeated(const QString &type, int idx)
{
	// Marker should be added automatically on throw in LexerParser but since Input does parsing twice it has to be handled manually
	addMarker(editor(), sym(idx).position(), TextMarker::ERROR);
	throw ParserException(toTrimmedQString(sym(idx)), {toTrimmedQString(sym(idx)) + ' ' + type + " already exists"});
}

bool LGCInputLexerIO::isRootFrame(const std::string &name) const
{
	return name == "ROOT";
}

void LGCInputLexerIO::consumeRule(int rule)
{
	switch(rule)
	{
./

----------------------------------------------------------- RULES

---- Definitions

-- EOL is not used as we always use EOLC instead
--EOL: EOL_SYMBOL
--	| EOL EOL_SYMBOL;

EOLC: EOL_SYMBOL
	| CommentString EOL_SYMBOL
	| EOLC EOL_SYMBOL
	| EOLC CommentString EOL_SYMBOL;
CommentString: COMMENTSTRING;
/.
	ADDSTYLE($rule_number, Comment);
./

-- Comment conveying some important information that should not be modified, mostly used by external software 
FunctionalCommentString: DOLLAR
/. case $rule_number: ./
	| DOLLAR FunctionalCommentRubbish;
/.
	case $rule_number: // POINTNAMEDEF
		addStyleSym(Styles::FunctionalComment, 1);
		break;
./
FunctionalCommentRubbish: FunctionalCommentWord
/. case $rule_number: ./
	| FunctionalCommentWord FunctionalCommentRubbish;
/.
	case $rule_number: // POINTNAMEDEF
		addStyleSym(Styles::FunctionalComment, 1);
		break;
./
FunctionalCommentWord: WORD
	| DOLLAR
	| Number;

Number: INTEGER
	| DECIMAL;

POINTNAMEDEF: WORD
/. case $rule_number: ./
	| INTEGER;
/.
	case $rule_number: // POINTNAMEDEF
		addStyleSym(Styles::PointName, 1);
		if (_secondRun && _curTmp != "*PDOR")
		{
			addIndicationSym(TextIndicator::HOVERONLY);
			showMeasure(_curTmp, toTrimmedQString(sym(1)));
		}
		break;
./
POINTNAME: WORD
/. case $rule_number: ./
	| INTEGER;
/.
	case $rule_number: // POINTNAME
		addStyleSym(Styles::PointName, 1);
		jumpTo(_measures.points, "point");
		break;
./

INSTRNAMEDEF: WORD;
/.
	ADDSTYLE($rule_number, InstrumentName);
./
INSTRNAME: WORD;
/.
	case $rule_number:
		addStyleSym(Styles::InstrumentName, 1);
		jumpTo(_measures.instruments, "instrument");
		break;
./
TGTNAME: WORD;
/.
	ADDSTYLE($rule_number, TargetName);
./
MSRIDTEXT: WORD
/. case $rule_number: ./
	| Number;
/.
	case $rule_number: // MSRIDTEXT
		addStyleSym(Styles::Normal, 1);
		break;
./

FRAMENAME: WORD;
/.
	case $rule_number:
		addStyleSym(Styles::FrameName, 1);
		_curTmp = toTrimmedQString(sym(1));
		if (!isRootFrame(_curTmp.toStdString())) // Since ROOT frame isn't defined linking it to the definition is skipped
			jumpTo(_measures.frames, "frame");
		break;
./
FrameDefParameter: TX
/. case $rule_number: ./
	| TY
/. case $rule_number: ./
	| TZ
/. case $rule_number: ./
	| RX
/. case $rule_number: ./
	| RY
/. case $rule_number: ./
	| RZ
/. case $rule_number: ./
	| SCL;
/.
	case $rule_number:
		addStyleSym(Styles::FrameParameter, 1);
		break;
./	


		
------ Global

LGCDocument: Title Options Instruments Content End
	| Title Options Instruments Content End PostEnd;

------ Content

Content: Pdor Contents -- can start with Pdor or without
	| Contents;
Contents: Contents ContentType -- the rest can be in mixed order
	| ContentType;
ContentType: Frame
	| PointList
	| MeasurementList;

------ Title

Title: TitleToken EOLC
	| Title TitleWords EOLC;
/.
	case $rule_number:
		if (!lexerMode())
			_curList.setTitle(_curList.getTitle() + '\n');
		break;
./
TitleToken: TITLE;
/.
	// *TITR
	case $rule_number:
		addAnnotation(_ANNOTATION_TITR);
		addStyleSym(Styles::StartEnd, 1);
		break;
./
TitleWords: WORD
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_TITR);
./
	| Number
/.
	case $rule_number:
		addStyleSym(Styles::Normal, 1);
		if (!lexerMode())
			_curList.setTitle(_curList.getTitle() + toTrimmedString(sym(1), utf8()));
		break;
./
	| TitleWords WORD
/. case $rule_number: ./
	| TitleWords Number;
/.
	case $rule_number:
		addStyleSym(Styles::Normal, 2);
		if (!lexerMode())
			_curList.setTitle(_curList.getTitle() + toTrimmedString(sym(2), utf8()));
		break;
./

------ Options

Options: OptionsDef;
/.
	case $rule_number:
		endFolding();
		break;
./
OptionsDef: DatumOption
	| DatumOption CalculationOptions
	| DatumOption OutputOptions
	| DatumOption CalculationOptions OutputOptions
	| DatumOption OutputOptions CalculationOptions
	| DatumOption OutputOptions CalculationOptions OutputOptions;

-- Datum options
DatumOption: DatumOptionToken EOLC;
DatumOptionToken: LEP
/. case $rule_number: ./
	| OLOC
/. case $rule_number: ./
	| RS2K
/. case $rule_number: ./
	| SPHE;
/.
	case $rule_number: // Datum Option
		addAnnotation(_ANNOTATION_DATUMOPTIONS);
		addStyleSym(Styles::DatumOption, 1);
		startFolding();
		if (!lexerMode())
		{
			_curList.getParams().coordsys = toTrimmedString(sym(1), utf8()) == "*OLOC" ? ShareableParams::ECoordSys::k3DCartesian : ShareableParams::ECoordSys::k2DPlusH;
			setOption("coordinateSystem");
		}
		break;
./

-- Calculation options
CalculationOptions: CalculationOption EOLC
	| CalculationOptions CalculationOption EOLC;
CalculationOption: ALLFIXED
/. 
	case $rule_number: 
		addAnnotation(_ANNOTATION_ALLFIXED);
		addStyleSym(Styles::CalculationOption, 1);

		setOption("CalculationOptions");
		break;
./
	| Simu;
Simu: SimuToken INTEGER
/.
	case $rule_number: // SIMU 1 param
		checkInt(2, "Number of Simulations or Seed Value");

		setOption("CalculationOptions");
		break;
./
	| SimuToken INTEGER INTEGER
/. case $rule_number: ./
	| SimuToken INTEGER INTEGER SimuRubbish;

/.
	case $rule_number: // SIMU 2 params
		checkInt(2, "Number of Simulations");
		checkInt(3, "Seed Value");

		setOption("CalculationOptions");
		break;
./
SimuToken: SIMU;
/.
	// *SIMU
	case $rule_number:
		addAnnotation(_ANNOTATION_SIMU);
		addStyleSym(Styles::CalculationOption, 1);
		break;
./
SimuRubbish: Number
/.
	case $rule_number:
		addStyleSym(Styles::Comment, 1);
		break;
./
	| SimuRubbish Number;
/.
	case $rule_number:
		addStyleSym(Styles::Comment, 2);
		break;
./

-- Output options
OutputOptions: OutputOption
	| OutputOptions OutputOption;
OutputOption: OutputOptionSimple
	| OutputOptionComplex;
OutputOptionSimple: OutputOptionSimpleToken EOLC;
OutputOptionSimpleToken: APRI
/.
	case $rule_number: 
		addAnnotation(_ANNOTATION_APRI);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		break;
./
	| CHABA
/.
	case $rule_number: 
		addAnnotation(_ANNOTATION_CHABA);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		break;
./
	| COVAR
/.
	case $rule_number: 
		addAnnotation(_ANNOTATION_COVAR);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		break;
./
	| DEFA
/.
	case $rule_number: 
		addAnnotation(_ANNOTATION_DEFA);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		break;
./
	| HIST
/.
	case $rule_number: 
		addAnnotation(_ANNOTATION_HIST);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		break;
./
	| NODUP
/.
	case $rule_number: 
		addAnnotation(_ANNOTATION_NODUP);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		break;
./	
	| PRES
/.
	case $rule_number: 
		addAnnotation(_ANNOTATION_PRES);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		break;
./
	| SOBS;
/.
	case $rule_number: // Simple options
		addAnnotation(_ANNOTATION_SOBS);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		break;
./
OutputOptionComplex: Erel
/. case $rule_number: ./
	| ErelFrame
/.
	case $rule_number:
		endFolding();
		break;
./
	| Consi
	| Faut
	| Fmtp
	| Json
	| Prec
	| Punc;

Consi: ConsiToken EOLC
	| ConsiToken ConsiLibrOption EOLC
	| ConsiToken ConsiLibrOption ConsiLibrParameters EOLC;
ConsiToken: CONSI;
/.
	case $rule_number: // CONSI
		addAnnotation(_ANNOTATION_CONSI);
		addStyleSym(Styles::OutputOption, 1);
		_inlineContexts.insert(InlineCxt::CONSI);

		//setOption("OutputOptions");
		break;
./
ConsiLibrOption: CONSILIBR;
/.
	case $rule_number: // LIBR
	addStyleSym(Styles::OptionParameter, 1);
	break;
./
ConsiLibrParameters: ConsiLibrParameter
	| ConsiLibrParameter ConsiLibrParameters;
ConsiLibrParameter: TX
/. case $rule_number: ./
	| TY
/. case $rule_number: ./
	| TZ
/. case $rule_number: ./
	| RX
/. case $rule_number: ./
	| RY
/. case $rule_number: ./
	| RZ
/. case $rule_number: ./
	| SCL;
/.
	case $rule_number:
	addStyleSym(Styles::OptionParameter, 1);
	break;
./

Erel: ErelToken EOLC ErelEntry EOLC
	| Erel ErelEntry EOLC;
ErelToken: EREL;
/.
	case $rule_number: // EREL
		addAnnotation(_ANNOTATION_EREL);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		startFolding();
		break;
./
ErelEntry: POINTNAME POINTNAME
/. case $rule_number: ./
	| POINTNAME POINTNAME FRAMENAME;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_EREL);

		setOption("OutputOptions");
		break;
./

ErelFrame: ErelFrameToken EOLC ErelFrameEntry EOLC
	| ErelFrame ErelFrameEntry EOLC;
ErelFrameToken: ERELFRAME;
/.
	case $rule_number: // ERELFRAME
		addAnnotation(_ANNOTATION_ERELFRAME);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		startFolding();
		break;
./
ErelFrameEntry: FRAMENAME FRAMENAME;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_ERELFRAME);
		setOption("OutputOptions");
		break;
./

Faut: FautToken EOLC
	| FautToken FautTwoNumbers EOLC;
FautToken: FAUT;
/.
	case $rule_number: // FAUT
		addAnnotation(_ANNOTATION_FAUT);
		addStyleSym(Styles::OutputOption, 1);

		setOption("OutputOptions");
		break;
./
FautTwoNumbers: Number Number;
/.
	case $rule_number:
		checkDouble(1, "alpha", 0, 1);
		checkDouble(2, "beta", 0, 1);
		break;
./
Fmtp: FmtpCol EOLC
	| FmtpSep EOLC;
FmtpCol: FmtpToken FMTPCOL;
/.
	case $rule_number: // FMTPCOL
		addAnnotation(_ANNOTATION_FMTP);
		addStyleSym(Styles::OptionParameter, 2);
		break;
./
FmtpSep: FmtpToken FMTPSEP QUOTE WORD QUOTE;
/.
	case $rule_number: // FMTPSEP WORD
	{
		addAnnotation(_ANNOTATION_FMTP);
		addStyleSym(Styles::OptionParameter, 2);
		QStringRef sep = sym(3).trimmed();
		addStyleSym(Styles::Normal, 3);
		addStyleSym(Styles::Normal, 4);
		addStyleSym(Styles::Normal, 5);
		break;
	}
./
FmtpToken: FMTP;
/.
	case $rule_number: // *FMTP
		addStyleSym(Styles::OutputOption, 1);
		_inlineContexts.insert(InlineCxt::FMTP);

		setOption("OutputOptions");
		break;
./

Json: JsonToken EOLC
	| JsonToken JsonOptions EOLC;
JsonToken: JSON;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_JSON);
		addStyleSym(Styles::OutputOption, 1);
		_inlineContexts.insert(InlineCxt::JSON);

		setOption("OutputOptions");
		break;
./
JsonOptions: JsonOption;
JsonOption: JSONCOVAR;
/.
	case $rule_number:
		addStyleSym(Styles::OutputOption, 1);
		break;
./


Prec: PrecInt EOLC;
PrecInt: PrecToken INTEGER;
/.
	case $rule_number:
	{
		int prec = checkInt(2, "precision", 0, 7);
		if (!lexerMode())
			_curList.getParams().precision = prec;
		break;
	}
./
PrecToken: PREC;
/.
	case $rule_number:	// *PREC
	{
		addAnnotation(_ANNOTATION_PREC);
		addStyleSym(CalculationOption, 1);
		break;
	}
./
Punc: PuncToken EOLC
	| PuncToken PuncParameter EOLC;
PuncToken: PUNC;
/.
	case $rule_number: // *PUNC
		addAnnotation(_ANNOTATION_PUNC);
		addStyleSym(Styles::OutputOption, 1);
		_inlineContexts.insert(InlineCxt::PUNC);

		setOption("OutputOptions");
		break;
./
PuncParameter: PUNCE
/. case $rule_number: ./
	| PUNCEE
/. case $rule_number: ./
	| PUNCH
/. case $rule_number: ./
	| PUNCZ
/. case $rule_number: ./
	| PUNCHZ
/. case $rule_number: ./
	| PUNCHN
/. case $rule_number: ./
	| PUNCZHN
/. case $rule_number: ./
	| PUNCT
/. case $rule_number: ./
	| PUNCOUT1
/. case $rule_number: ./
	| PUNCOUT3;
/.
	case $rule_number: // PUNC parameter
		addStyleSym(Styles::OptionParameter, 1);
		break;
./

------ Content

---- Instruments

Instruments: InstrToken EOLC
/. case $rule_number: ./
	| InstrToken EOLC InstrList;
/.
	case $rule_number:
		endFolding();
		break;
./
InstrToken: INSTR;
/.
	case $rule_number: // *INSTR
		addAnnotation(_ANNOTATION_INSTR);
		addStyleSym(Styles::StartEnd, 1);
		startFolding();
		_unusedComment.clear();
		break;
./
InstrList: Instrument
	| InstrList Instrument;
Instrument: Camd CamdTargets
/. case $rule_number: ./
	| Edm EdmTargets
/. case $rule_number: ./
	| Level LevelStaves
/. case $rule_number: ./
	| Polar PolarTargets
/.
	case $rule_number:
		endFolding();
		break;
./
	| Scale
	| Incl
	| Hlsr
	| Wpsr;

-- CAMD
Camd: CamdDef EOLC;
CamdDef: CAMD INSTRNAMEDEF TGTNAME Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_CAMD);
		addStyleSym(Styles::InstrumentType, 1); // *CAMD
		checkDouble(4, "sigma_instr_centering_mm"); // sigma_instr_centering_mm

		startFolding();
		setInstrument();
		break;
./
CamdTargets: CamdTarget
	| CamdTargets CamdTarget;
CamdTarget: CamdTargetDef EOLC;
CamdTargetDef: TGTNAME Number Number Number Number
/.
	case $rule_number: // CAMD old
		addAnnotation(_ANNOTATION_CAMD_TGT);
		checkDouble(2, "sigma_X"); // sigma_X
		checkDouble(3, "sigma_Y"); // sigma_Y
		checkDouble(4, "sigma_dist_mm"); // sigma_dist_mm
		checkDouble(5, "sigma_tgt_centering_mm"); // sigma_tgt_centering_mm

		setTarget();
		break;
./
	| TGTNAME Number Number Number Number Number;
/.
	case $rule_number: // CAMD v2.02
		addAnnotation(_ANNOTATION_CAMD_TGT);
		checkDouble(2, "sigma_X"); // sigma_X
		checkDouble(3, "sigma_Y"); // sigma_Y
		checkDouble(4, "sigma_Z"); // sigma_Z
		checkDouble(5, "sigma_dist_mm"); // sigma_dist_mm
		checkDouble(6, "sigma_tgt_centering_mm"); // sigma_tgt_centering_mm

		setTarget();
		break;
./

-- EDM
Edm: EdmDef EOLC;
EdmDef: EDM INSTRNAMEDEF TGTNAME Number Number Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_EDM);
		addStyleSym(Styles::InstrumentType, 1); // *EDM
		addStyleSym(Styles::Normal, 4); // instr_height_m
		checkDouble(5, "sigma_instr_height_mm"); // sigma_instr_height_mm
		checkDouble(6, "sigma_instr_centering_mm"); // sigma_instr_centering_mm

		startFolding();
		setInstrument();
		break;
./
EdmTargets: EdmTarget
	| EdmTargets EdmTarget;
EdmTarget: EdmTargetDef EOLC;
EdmTargetDef: TGTNAME Number Number Number Number Number Number Number Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_EDM_TGT);
		checkDouble(2, "sigma_dist_mm"); // sigma_dist_mm
		addStyleSym(Styles::Normal, 3); // ppm_mm
		checkBool(4, "dCorr_adjustable"); // dCorr_adjustable
		addStyleSym(Styles::Normal, 5); // dCorr_m
		checkDouble(6, "sigma_dCorr_mm"); // sigma_dCorr_mm
		checkDouble(7, "sigma_tgt_centering_mm"); // sigma_tgt_centering_mm
		addStyleSym(Styles::Normal, 8); // tgt_height_m
		checkDouble(9, "sigma_tgt_height_mm"); // sigma_tgt_height_mm

		setTarget();
		break;
./

-- LEVEL
Level: LevelDef EOLC;
LevelDef: LEVEL INSTRNAMEDEF TGTNAME Number Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_LEVEL);
		addStyleSym(Styles::InstrumentType, 1); // *LEVEL
		checkBool(4, "collimationAngle_adjustable"); // collimationAngle_adjustable
		addStyleSym(Styles::Normal, 5); // collimationAngle_gon

		startFolding();
		setInstrument();
		break;
./
LevelStaves: LevelStaff
	| LevelStaves LevelStaff;
LevelStaff: LevelStaffDef EOLC;
LevelStaffDef: TGTNAME Number Number Number Number Number Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_LEVEL_TGT);
		checkDouble(2, "sigma_dist_mm"); // sigma_dist_mm
		addStyleSym(Styles::Normal, 3); // ppm_mm
		addStyleSym(Styles::Normal, 4); // dCorr_m
		checkDouble(5, "sigma_dCorr_mm"); // sigma_dCorr_mm
		addStyleSym(Styles::Normal, 6); // vert_offset_m
		addStyleSym(Styles::Normal, 7); // sigma_offset_mm

		setTarget();
		break;
./

-- POLAR
Polar: PolarDef EOLC;
PolarDef: POLAR INSTRNAMEDEF TGTNAME Number Number Number Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_POLAR);
		addStyleSym(Styles::InstrumentType, 1); // *POLAR
		addStyleSym(Styles::Normal, 4); // instr_height_m
		checkDouble(5, "sigma_instr_height_mm"); // sigma_instr_height_mm
		checkDouble(6, "sigma_instr_centering_mm"); // sigma_instr_centering_mm
		addStyleSym(Styles::Normal, 7); // const_angl_gon

		startFolding();
		setInstrument();
		break;
./
PolarTargets: PolarTarget
	| PolarTargets PolarTarget;
PolarTarget: PolarTargetDef EOLC;
PolarTargetDef: TGTNAME Number Number Number Number Number Number Number Number Number Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_POLAR_TGT);
		checkDouble(2, "sigma_ANGL_cc"); // sigma_ANGL_cc
		checkDouble(3, "sigma_ZEND_cc"); // sigma_ZEND_cc
		checkDouble(4, "sigma_DIST_mm"); // sigma_DIST_mm
		addStyleSym(Styles::Normal, 5); // ppm_mm
		checkBool(6, "dCorr_adjustable"); // dCorr_adjustable
		addStyleSym(Styles::Normal, 7); // dCorr_m
		checkDouble(8, "sigma_dCorr_mm"); // sigma_dCorr_mm
		checkDouble(9, "sigma_tgt_centering_mm"); // sigma_tgt_centering_mm
		addStyleSym(Styles::Normal, 10); // tgt_height_m
		checkDouble(11, "sigma_tgt_height_mm"); // sigma_tgt_height_mm

		setTarget();
		break;
./

-- SCALE
Scale: ScaleDef EOLC;
ScaleDef: SCALE INSTRNAMEDEF Number Number Number Number Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_SCALE);
		addStyleSym(Styles::InstrumentType, 1); // *SCALE
		checkDouble(3, "sigma_dist_mm"); // sigma_dist_mm
		addStyleSym(Styles::Normal, 4); // ppm_mm
		addStyleSym(Styles::Normal, 5); // dCorr_m
		checkDouble(6, "sigma_dCorr_mm"); // sigma_dCorr_mm
		checkDouble(7, "sigma_instr_centering_mm"); // sigma_instr_centering_mm

		setInstrument(false);
		break;
./

-- INCL
Incl: InclDef EOLC;
InclDef: INCL INSTRNAMEDEF Number Number Number Number Number Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_INCL);
		addStyleSym(Styles::InstrumentType, 1); // instr_ID
		addStyleSym(Styles::Normal, 3); // sigma_angle_cc
		addStyleSym(Styles::Normal, 4); // sigma_ppm  
		addStyleSym(Styles::Normal, 5); // aCorr_gon
		addStyleSym(Styles::Normal, 6); // sigma_aCorr_cc
		addStyleSym(Styles::Normal, 7); // refAngle_gon
		addStyleSym(Styles::Normal, 8); // sigma_refAngle_cc

		setInstrument(false);
		break;
./

-- HLSR
Hlsr: HlsrDef EOLC;
HlsrDef: HLSR INSTRNAMEDEF Number Number Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_HLSR);
		addStyleSym(Styles::InstrumentType, 1); // *HLSR
		checkDouble(3, "sigma_dist_mm"); // sigma_dist_mm
		addStyleSym(Styles::Normal, 4); // sigma_instr_height_mm
		addStyleSym(Styles::Normal, 5); // sigma_instr_centering_mm

		setInstrument(false);
		break;
./

-- WPSR
Wpsr: WpsrDef EOLC;
WpsrDef: WPSR INSTRNAMEDEF Number Number Number Number;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_WPSR);
		addStyleSym(Styles::InstrumentType, 1); // *WPSR
		checkDouble(3, "sigma_dist_X_mm"); // sigma_dist_X_mm
		checkDouble(4, "sigma_dist_Z_mm"); // sigma_dist_Z_mm
		addStyleSym(Styles::Normal, 5); // sigma_instr_centering_X_mm
		addStyleSym(Styles::Normal, 6); // sigma_instr_centering_Z_mm

		setInstrument(false);
		break;
./

---- FRAME
Frame: FrameStart FrameContents FrameEndToken EOLC
	| FrameStart FrameEndToken EOLC;

------ FRAME CONTENTS 

FrameContents: FrameContent
	| FrameContents FrameContent;
FrameContent: Frame
	| FramePointList
	| FrameMeasurementList;

------ FRAME COMPONENTS

FrameStart: FrameStartToken FrameDef EOLC;
FrameDef: FRAMENAME FrameDefPosition
/. case $rule_number: ./
	| FRAMENAME FrameDefPosition FrameDefParameters
/. case $rule_number: ./
	| FRAMENAME FrameDefPosition FrameDefParameters FrameDefSlave;
/.
	case $rule_number:
		_curTmp = toTrimmedQString(sym(1));
		if(!lexerMode())
		{
			_curFrame->add(new ShareableFrame(_curList.getParamsPointer(), _curTmp.toStdString()));
			_curFrame = &_curFrame->getFrame(_curFrame->sizeFrames() - 1);		
		}
		setFrame(_curTmp);
		_curTmpFrame.append('/' + _curTmp);
		break;
./	
FrameDefPosition: Number Number Number Number Number Number Number;
FrameDefParameters: FrameDefParameter
	| FrameDefParameter FrameDefParameters;	
FrameDefSlave: FRSLAVE WORD;
/.
	case $rule_number:
		addStyleSym(Styles::Frame, 1);
		addStyleSym(Styles::FrameName, 2);
		break;
./

FrameStartToken: FRAMESTART;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_FRAME);
		addStyleSym(Styles::Frame, 1); // FRAMESTART
		_inlineContexts.insert(InlineCxt::FRAMESTART);
		startFolding();
		break;
./
FrameEndToken: FRAMEEND;
/.
	case $rule_number:
		addAnnotation(_ANNOTATION_FRAME);
		addStyleSym(Styles::Frame, 1); // FRAMEEND
		// Take the most nested Frame name to escape it first
		if (_curTmpFrame.contains('/'))
		{
			if(_secondRun)
				jumpTo(_measures.frames, "", 1, _curTmpFrame.right(_curTmpFrame.size() - _curTmpFrame.lastIndexOf('/') - 1));
			_curTmpFrame = _curTmpFrame.left(_curTmpFrame.lastIndexOf('/'));
		}
		// if not empty and does not contain /, then it is the outermost frame
		else if (!_curTmpFrame.isEmpty())
			_curTmpFrame = "";
		if(!lexerMode())
			_curFrame = _curFrame->getParentFrame();
		
		endFolding();
		break;
./
	
------ FRAME POINTLIST
	
FramePointList: FramePointType EOLC Points;
/.
	case $rule_number:
		_curTmp.clear();
		endFolding();
		break;
./
FramePointType: CALA
/. case $rule_number: ./
	| POIN;
/.
	case $rule_number: // Point type
		addAnnotation(_ANNOTATION_POINTS);
		addStyleSym(Styles::PointType, 1);
		_curTmp = toTrimmedQString(sym(1));
		startFolding();
		_unusedComment.clear();
		break;
./
	

------ FRAME MEASURES

FrameMeasurementList: FrameMsrCamd
/. case $rule_number: ./
	| FrameMsrTstn
/. case $rule_number: ./
	| FrameMsrNoType
/. case $rule_number: ./
	| FrameMsrIncl;
/.
	case $rule_number:
		endFolding();
		_curTmp.clear();
		_curTmp2.clear();
		_curPointMeasure.clear();
		break;
./

-------- FRAME POLAR//TSTN ANGL ZEND DIST
FrameMsrTstn: TstnDef V0Def
/. case $rule_number: ./
	| TstnDef V0Def FrameTstnMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
FrameTstnMeasures: FrameTstnMeasure
	| FrameTstnMeasures FrameTstnMeasure;
FrameTstnMeasure: MsrAngle
/. case $rule_number: ./
	| MsrDist
/. case $rule_number: ./
	| MsrZend;
/.
	case $rule_number:
		endFolding();
		break;
./

-------- FRAME CAM// UVEC UVD
FrameMsrCamd: MsrCamd;

-------- FRAME NOTYPE// OBSXYZ
FrameMsrNoType: MsrObsxyz;

-------- FRAME INCL// INCLY
FrameMsrIncl: MsrIncl;


---- PointLists

PointList: PointType EOLC Points;
/.
	case $rule_number:
		_curTmp.clear();
		endFolding();
		break;
./
PointType: CALA
/. case $rule_number: ./
	| POIN
/. case $rule_number: ./
	| VXY
/. case $rule_number: ./
	| VXZ
/. case $rule_number: ./
	| VYZ
/. case $rule_number: ./
	| VZ;
/.
	case $rule_number: // Point type
		addAnnotation(_ANNOTATION_POINTS);
		addStyleSym(Styles::PointType, 1);
		_curTmp = toTrimmedQString(sym(1));
		startFolding();
		_unusedComment.clear();
		break;
./
Points: Point
	| Points Point;
Point: PointDef EOLC
	| PointDef FunctionalCommentString EOLC
	| PointDef Number Number Number EOLC -- see SUS-2059
	| PointDef Number Number Number FunctionalCommentString EOLC; -- see SUS-2059
PointDef: POINTNAMEDEF Number Number Number;
/.
	case $rule_number: // PointDef
		addPointAnnotation();
		addStyleSym(Styles::PointCoord, 2);
		addStyleSym(Styles::PointCoord, 3);
		addStyleSym(Styles::PointCoord, 4);
		setPoint(_curTmp);
		break;
./

-- PDOR
Pdor: PdorToken EOLC PdorParam EOLC;
/.
	case $rule_number:
		endFolding();
		_curTmp.clear();
		_unusedComment.clear();
		break;
./
PdorToken: PDOR;
/.
	case $rule_number: // PDOR
		addAnnotation(_ANNOTATION_PDOR);
		addStyleSym(Styles::PointType, 1);
		_curTmp = toTrimmedQString(sym(1));
		startFolding();
		break;
./
PdorPoint: WORD
/. case $rule_number: ./
       | INTEGER;
/.
	case $rule_number: // PdorParam
		addAnnotation(_ANNOTATION_PDOR);
		addStyleSym(Styles::PointName, 1);
		setPoint(_curTmp);
		break;
./
PdorParam: PdorPoint
       | PdorPoint Number;
/.
	case $rule_number: // PdorParam
		addStyleSym(Styles::Normal, 2);
		break;
./

---- MeasurementLists

MeasurementList: MsrCamd
/. case $rule_number: ./
	| MsrEdm
/. case $rule_number: ./
	| MsrLevel
/. case $rule_number: ./
	| MsrPolar
/. case $rule_number: ./
	| MsrScale
/. case $rule_number: ./
	| MsrHlsr
/. case $rule_number: ./
	| MsrWpsr
/. case $rule_number: ./
	| MsrNoType;
/.
	case $rule_number:
		endFolding();
		_curTmp.clear();
		_curTmp2.clear();
		_curPointMeasure.clear();
		break;
./

-- Optional arguments
ArgAc: MSRAC Number;
/.
	case $rule_number: // AC default_angle_gon
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgAcse: MSRACSE Number;
/.
	case $rule_number: // ACSE default_angle_cc
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgAcst: MSRACST Number;
/.
	case $rule_number: // ACST constant_angle_gons
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgAse: MSRASE Number;
/.
	case $rule_number: // ASE sigma_ANGL_cc
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_ANGL_cc");
		break;
./
ArgCst: MSRCST Number;
/.
	case $rule_number: // CST constant_angle
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgDcor: MSRDCOR Number;
/.
	case $rule_number: // DCOR correction_m
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgDhorDse: MSRDHOR Number ArgDse;
/.
	case $rule_number: // DHOR dist_m  DSE sigma_dist_mm
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgDse: MSRDSE Number;
/.
	case $rule_number: // DSE sigma_dist_mm
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_dist_mm");
		break;
./
ArgIcse: MSRICSE Number;
/.
	case $rule_number: // ICSE  sigma_instr_centering_mm
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_instr_centering_mm");
		break;
./
ArgIh: MSRIH Number;
/.
	case $rule_number: // IH instr_heigth_m
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgIhfix: MSRIHFIX;
/.
	// IHFIX
	ADDSTYLE($rule_number, MeasureParameter);
./
ArgIhse: MSRIHSE Number;
/.
	case $rule_number: // IHSE sigma_instr_height_mm
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_instr_height_mm");
		break;
./
ArgInstr: MSRINSTR INSTRNAME;
/.
	case $rule_number: // INSTR incl_instr_ID
		addStyleSym(Styles::MeasureParameter, 1);
		break;
./
ArgPpm: MSRPPM Number;
/.
	case $rule_number: // PPM ppm_mm
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgPtLine: MSRPTLINE POINTNAME;
/.
	case $rule_number: // PtLine point_on_line
		addStyleSym(Styles::MeasureParameter, 1);
		break;
./
ArgRefPt: MSRREFPT POINTNAME;
/.
	case $rule_number: // RefPt reference_point
		addStyleSym(Styles::MeasureParameter, 1);
		_curPointMeasure = toTrimmedQString(sym(2));
		break;
./
ArgRf: MSRRF Number;
/.
	case $rule_number: // AC default_angle_gon
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgRfse: MSRRFSE Number;
/.
	case $rule_number: // AC default_angle_cc
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgRot3D: MSRROT3D; -- ROT3D
/.
	// ROT3D
	ADDSTYLE($rule_number, MeasureParameter);
./
ArgSagfix: MSRSAGFIX; -- SAGFIX
/.
	// SAGFIX
	ADDSTYLE($rule_number, MeasureParameter);
./
ArgSagse: MSRSAGSE Number; -- SAGSE
/.
	case $rule_number: // SAGSE sigma_sag_mm
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_sag_mm");
		break;
./
ArgScale: MSRSCALE INSTRNAME;
/.
	case $rule_number: // SCALE scale_instr_ID
		addStyleSym(Styles::MeasureParameter, 1);
		break;
./
ArgTcse: MSRTCSE Number;
/.
	case $rule_number: // TCSE sigma_tgt_centering_mm
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_mm");
		break;
./
ArgTh: MSRTH Number;
/.
	case $rule_number: // TH tgt_height_m
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::Normal, 2);
		break;
./
ArgThse: MSRTHSE Number;
/.
	case $rule_number: // THSE sigma_tgt_height_mm
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_mm");
		break;
./
ArgTrgt: MSRTRGT TGTNAME;
/.
	case $rule_number: // TRGT changed_tgt_ID
		addStyleSym(Styles::MeasureParameter, 1);

		jumpTo(_measures.targets, "target", 2, _curTmp + '/' + toTrimmedQString(sym(2)));
		break;
./
ArgObse: MSROBSE Number;
/.
	case $rule_number: // OBSE sigma_cc_mm
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_mm");
		break;
./
ArgXicse: MSRXICSE Number;
/.
	case $rule_number: // XICSE sigma_instr_centering_X_mm
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_instr_centering_X_mm");
		break;
./
ArgXse: MSRXSE Number;
/.
	case $rule_number: // XSE sigma_x
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_mm");
		break;
./
ArgYse: MSRYSE Number;
/.
	case $rule_number: // YSE sigma_y
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_mm");
		break;
./
ArgZicse: MSRZICSE Number;
/.
	case $rule_number: // ZICSE sigma_instr_centering_Z_mm
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_instr_centering_Z_mm");
		break;
./
ArgZse: MSRZSE Number;
/.
	case $rule_number: // ZSE sigma_z
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_mm");
		break;
./
ArgWsse: MSRWSSE Number;
/.
	case $rule_number: // WSSE sigma_water_surface_mm
		addStyleSym(Styles::MeasureParameter, 1);
		checkDouble(2, "sigma_water_surface_mm");
		break;
./
ArgWiid: MSRWIID WORD;
/.
	case $rule_number: // WIID wire_ID
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::PointName, 2);
		// fake point to link to
		_measures.points.emplace(sym(2).toString().trimmed(),
			LGCMeasures::Value{sym(2).position(), sym(2).toString().trimmed(), sym(2).toString(), _currentLine.toString(), "", "", ""});
		break;
./
ArgWsid: MSRWSID WORD;
/.
	case $rule_number: // WSID water_ID
		addStyleSym(Styles::MeasureParameter, 1);
		addStyleSym(Styles::PointName, 2);
		// fake point to link to
		_measures.points.emplace(sym(2).toString().trimmed(),
			LGCMeasures::Value{sym(2).position(), sym(2).toString().trimmed(), sym(2).toString(), _currentLine.toString(), "", "", ""});
		break;
./
ArgID: MSRID MSRIDTEXT;
/.
	case $rule_number: // ID observationID
		addStyleSym(Styles::MeasureParameter, 1);
		break;
./
-- CAMD
MsrCamd: MsrCam
/. case $rule_number: ./
	| MsrCam CamMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./

	-- CAM
MsrCam: CamDef EOLC
	| CamDef CamArgs EOLC;
CamDef: CAM POINTNAME INSTRNAME;
/.
	case $rule_number: // *CAM cam_position instr_ID
		addAnnotation(_ANNOTATION_CAM);
		addStyleSym(Styles::MeasureType, 1);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		_curTmp = toTrimmedQString(sym(3));
		_curPointMeasure = toTrimmedQString(sym(2));
		break;
./
CamArgs: CamArg
	| CamArgs CamArg;
CamArg: ArgIcse
	| ArgTrgt;

CamMeasures: CamMeasure
	| CamMeasures CamMeasure;
CamMeasure: MsrUvd
/. case $rule_number: ./
	| MsrUvec;
/.
	case $rule_number:
		endFolding();
		break;
./

	-- UVD
MsrUvd: UvdDef
	| UvdDef UvdMeasures;
UvdDef: UvdToken EOLC
	| UvdToken ArgTrgt EOLC;
UvdToken: CAMUVD;
/.
	case $rule_number: // *UVD
		addAnnotation(_ANNOTATION_UVD);
		addStyleSym(Styles::MeasureType, 1);
		startFolding();
		break;
./
UvdMeasures: UvdMeasure
	| UvdMeasures UvdMeasure;
UvdMeasure: UvdMeasureDef EOLC
	| UvdMeasureDef FunctionalCommentString EOLC
	| UvdMeasureDef UvdMeasureArgs EOLC
	| UvdMeasureDef UvdMeasureArgs FunctionalCommentString EOLC;
UvdMeasureDef: POINTNAME Number Number Number Number;
/.
	case $rule_number: // point obs_x obs_y z_comp obs_dist_m
		addAnnotation(_ANNOTATION_UVD_POINT);
		addStyleSym(Styles::Normal, 2);
		addStyleSym(Styles::Normal, 3);
		addStyleSym(Styles::Normal, 4);
		addStyleSym(Styles::Normal, 5);

		setMeasure("CAMUVD/" + toTrimmedQString(sym(1)));
		countMeasure("CAMUVD");
		break;
./
UvdMeasureArgs: UvdMeasureArg
	| UvdMeasureArgs UvdMeasureArg;
UvdMeasureArg: ArgDse
	| ArgTcse
	| ArgTrgt
	| ArgXse
	| ArgYse
	| ArgID;

	-- UVEC
MsrUvec: UvecDef
	| UvecDef UvecMeasures;
UvecDef: UvecToken EOLC
	| UvecToken ArgTrgt EOLC;
UvecToken: CAMUVEC;
/.
	case $rule_number: // *UVEC
		addAnnotation(_ANNOTATION_UVEC);
		addStyleSym(Styles::MeasureType, 1);
		startFolding();
		break;
./
UvecMeasures: UvecMeasure
	| UvecMeasures UvecMeasure;
UvecMeasure: UvecMeasureDef EOLC
	| UvecMeasureDef FunctionalCommentString EOLC
	| UvecMeasureDef UvecMeasureArgs EOLC
	| UvecMeasureDef UvecMeasureArgs FunctionalCommentString EOLC;
UvecMeasureDef: POINTNAME Number Number Number;
/.
	case $rule_number: // point obs_x obs_y z_comp
		addAnnotation(_ANNOTATION_UVEC_POINT);
		addStyleSym(Styles::Normal, 2);
		addStyleSym(Styles::Normal, 3);
		addStyleSym(Styles::Normal, 4);

		setMeasure("CAMUVEC/" + toTrimmedQString(sym(1)));
		countMeasure("CAMUVEC");
		break;
./
UvecMeasureArgs: UvecMeasureArg
	| UvecMeasureArgs UvecMeasureArg;
UvecMeasureArg: ArgTcse
	| ArgTrgt
	| ArgXse
	| ArgYse
	| ArgID;

-- EDM
MsrEdm: MsrDspt;

	-- DSPT
MsrDspt: DsptDef
/. case $rule_number: ./
	| DsptDef DsptMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
DsptDef: DsptDefBeg EOLC
	| DsptDefBeg DsptDefArgs EOLC;
DsptDefBeg: DSPT POINTNAME INSTRNAME;
/.
	case $rule_number: // *DSPT stn instr_ID
		addAnnotation(_ANNOTATION_DSPT);
		addStyleSym(Styles::MeasureType, 1);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		_curTmp = toTrimmedQString(sym(3));
		_curPointMeasure = toTrimmedQString(sym(2));
		break;
./
DsptDefArgs: DsptDefArg
	| DsptDefArgs DsptDefArg;
DsptDefArg: ArgIcse
	| ArgIh
	| ArgIhfix
	| ArgIhse
	| ArgTrgt;
DsptMeasures: DsptMeasure
	| DsptMeasures DsptMeasure;
DsptMeasure: DsptMeasureDef EOLC
	| DsptMeasureDef FunctionalCommentString EOLC
	| DsptMeasureDef DsptMeasureArgs EOLC
	| DsptMeasureDef DsptMeasureArgs FunctionalCommentString EOLC;
DsptMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_dist_m
		addAnnotation(_ANNOTATION_DSPT_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("DSPT/" + toTrimmedQString(sym(1)));
		countMeasure("DSPT");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
DsptMeasureArgs: DsptMeasureArg
	| DsptMeasureArgs DsptMeasureArg;
DsptMeasureArg: ArgObse
	| ArgPpm
	| ArgTcse
	| ArgTh
	| ArgThse
	| ArgTrgt
	| ArgID;

-- LEVEL
MsrLevel: MsrDlev;

	-- DLEV
MsrDlev: DlevDef
/. case $rule_number: ./
	| DlevDef DlevMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
DlevDef: DlevDevBeg EOLC
	| DlevDevBeg ArgRefPt EOLC;
DlevDevBeg: DLEV INSTRNAME;
/.
	case $rule_number: // *DLEV instr_ID
		addAnnotation(_ANNOTATION_DLEV);
		addStyleSym(Styles::MeasureType, 1);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		_curTmp = toTrimmedQString(sym(2));
		break;
./
DlevMeasures: DlevMeasure
	| DlevMeasures DlevMeasure;
DlevMeasure: DlevMeasureDef EOLC
	| DlevMeasureDef FunctionalCommentString EOLC
	| DlevMeasureDef DlevMeasureArgs EOLC
	| DlevMeasureDef DlevMeasureArgs FunctionalCommentString EOLC;
DlevMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_m
		addAnnotation(_ANNOTATION_DLEV_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("DLEV/" + toTrimmedQString(sym(1)));
		countMeasure("DLEV");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
DlevMeasureArgs: DlevMeasureArg
	| DlevMeasureArgs DlevMeasureArg;
DlevMeasureArg: ArgDhorDse
	| ArgObse
	| ArgPpm
	| ArgTh
	| ArgThse
	| ArgTrgt
	| ArgID;

-- POLAR
MsrPolar: MsrOrie
	| MsrTstn;

	-- ORIE
MsrOrie: OrieDef
/. case $rule_number: ./
	| OrieDef OrieMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
OrieDef: OrieDefBeg EOLC
	| OrieDefBeg OrieDefArgs EOLC;
OrieDefBeg: ORIE POINTNAME INSTRNAME;
/.
	case $rule_number: // *ORIE stn_point POLAR_ID
		addAnnotation(_ANNOTATION_ORIE);
		addStyleSym(Styles::MeasureType, 1);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		_curTmp2 = toTrimmedQString(sym(2));
		_curTmp = toTrimmedQString(sym(3));
		_curPointMeasure = _curTmp2;
		break;
./
OrieDefArgs: OrieDefArg
	| OrieDefArgs OrieDefArg;
OrieDefArg: ArgCst
	| ArgIcse
	| ArgTrgt;
OrieMeasures: OrieMeasure
	| OrieMeasures OrieMeasure;
OrieMeasure: OrieMeasureDef EOLC
	| OrieMeasureDef FunctionalCommentString EOLC
	| OrieMeasureDef OrieMeasureArgs EOLC
	| OrieMeasureDef OrieMeasureArgs FunctionalCommentString EOLC;
OrieMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_gon
		addAnnotation(_ANNOTATION_ORIE_POINT);
		addStyleSym(Styles::Normal, 2);

		std::swap(_curTmp, _curTmp2);
		setMeasure("ORIE/" + toTrimmedQString(sym(1)));
		std::swap(_curTmp, _curTmp2);
		countMeasure("ORIE");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
OrieMeasureArgs: OrieMeasureArg
	| OrieMeasureArgs OrieMeasureArg;
OrieMeasureArg: ArgObse
	| ArgTcse
	| ArgTrgt
	| ArgID;

	-- TSTN
MsrTstn: TstnDef V0Def
/. case $rule_number: ./
	| TstnDef V0Def TstnMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
TstnDef: TstnDefBeg EOLC
	| TstnDefBeg TstnDefArgs EOLC;
TstnDefBeg: TSTN POINTNAME INSTRNAME;
/.
	case $rule_number: // *TSTN stationned_point instr_ID
		addAnnotation(_ANNOTATION_TSTN);
		addStyleSym(Styles::MeasureType, 1);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		_curTmp = toTrimmedQString(sym(3));
		_curPointMeasure = toTrimmedQString(sym(2));
		break;
./
TstnDefArgs: TstnDefArg
	| TstnDefArgs TstnDefArg;
TstnDefArg: ArgIcse
	| ArgIhfix
	| ArgIh
	| ArgIhse
	| ArgRot3D
	| ArgTrgt;
V0Def: V0Token EOLC
/. case $rule_number: ./
	| V0Token V0DefArgs EOLC;
/.
	case $rule_number:
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("ACST");
		}
		break;
./
V0Token: TSTNV0;
/.
	case $rule_number: // *V0
		addAnnotation(_ANNOTATION_V0);
		addStyleSym(MeasureType, 1);
		break;
./
V0DefArgs: V0DefArg
	| V0DefArgs V0DefArg;
V0DefArg: ArgAcst
	| ArgTrgt;
TstnMeasures: TstnMeasure
	| TstnMeasures TstnMeasure;
TstnMeasure: MsrAngle
/. case $rule_number: ./
	| MsrDhor
/. case $rule_number: ./
	| MsrDist
/. case $rule_number: ./
	| MsrEcdir
/. case $rule_number: ./
	| MsrEcth
/. case $rule_number: ./
	| MsrPlr3D
/. case $rule_number: ./
	| MsrZend;
/.
	case $rule_number:
		endFolding();
		break;
./

	-- ANGL
MsrAngle: AngleDef
	| AngleDef AngleMeasures;
AngleDef: AngleToken EOLC
	| AngleToken ArgTrgt EOLC;
AngleToken: TSTNANGL;
/.
	case $rule_number: // *ANGL
		addAnnotation(_ANNOTATION_ANGL);
		addStyleSym(Styles::MeasureType, 1);
		startFolding();
		break;
./
AngleMeasures: AngleMeasure
	| AngleMeasures AngleMeasure;
AngleMeasure: AngleMeasureDef EOLC
	| AngleMeasureDef FunctionalCommentString EOLC
	| AngleMeasureDef AngleMeasureArgs EOLC
	| AngleMeasureDef AngleMeasureArgs FunctionalCommentString EOLC;
AngleMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_ANGL
		addAnnotation(_ANNOTATION_ANGL_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("TSTNANGL/" + toTrimmedQString(sym(1)));
		countMeasure("TSTNANGL");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
AngleMeasureArgs: AngleMeasureArg
	| AngleMeasureArgs AngleMeasureArg;
AngleMeasureArg: ArgObse
	| ArgTcse
	| ArgTrgt
	| ArgID;

	-- DHOR
MsrDhor: DhorDef
	| DhorDef DhorMeasures;
DhorDef: DhorToken EOLC
	| DhorToken ArgTrgt EOLC;
DhorToken: TSTNDHOR;
/.
	case $rule_number: // *DHOR
		addAnnotation(_ANNOTATION_DHOR);
		addStyleSym(Styles::MeasureType, 1);
		startFolding();
		break;
./
DhorMeasures: DhorMeasure
	| DhorMeasures DhorMeasure;
DhorMeasure: DhorMeasureDef EOLC
	| DhorMeasureDef FunctionalCommentString EOLC
	| DhorMeasureDef DhorMeasureArgs EOLC
	| DhorMeasureDef DhorMeasureArgs FunctionalCommentString EOLC;
DhorMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_m
		addAnnotation(_ANNOTATION_DHOR_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("TSTNDHOR/" + toTrimmedQString(sym(1)));
		countMeasure("TSTNDHOR");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
DhorMeasureArgs: DhorMeasureArg
	| DhorMeasureArgs DhorMeasureArg;
DhorMeasureArg: ArgObse
	| ArgPpm
	| ArgTcse
	| ArgTrgt
	| ArgID;

	-- DIST
MsrDist: DistDef
	| DistDef DistMeasures;
DistDef: DistToken EOLC
	| DistToken ArgTrgt EOLC;
DistToken: TSTNDIST;
/.
	case $rule_number: // *DIST
		addAnnotation(_ANNOTATION_DIST);
		addStyleSym(Styles::MeasureType, 1);
		startFolding();
		break;
./
DistMeasures: DistMeasure
	| DistMeasures DistMeasure;
DistMeasure: DistMeasureDef EOLC
	| DistMeasureDef FunctionalCommentString EOLC
	| DistMeasureDef DistMeasureArgs EOLC
	| DistMeasureDef DistMeasureArgs FunctionalCommentString EOLC;
DistMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_dist_m
		addAnnotation(_ANNOTATION_DIST_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("TSTNDIST/" + toTrimmedQString(sym(1)));
		countMeasure("TSTNDIST");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
DistMeasureArgs: DistMeasureArg
	| DistMeasureArgs DistMeasureArg;
DistMeasureArg: ArgObse
	| ArgPpm
	| ArgTcse
	| ArgTh
	| ArgThse
	| ArgTrgt
	| ArgID;

	-- ECDIR
MsrEcdir: EcdirDef
	| EcdirDef EcdirMeasures;
EcdirDef: EcdirDefBeg EOLC;
EcdirDefBeg: TSTNECDIR Number Number INSTRNAME;
/.
	case $rule_number: // *ECDIR hz_angle_gon vert_angle_gon scale_ID
		addAnnotation(_ANNOTATION_ECDIR);
		addStyleSym(Styles::MeasureType, 1);
		addStyleSym(Styles::Normal, 2);
		addStyleSym(Styles::Normal, 3);

		startFolding();
		break;
./
EcdirMeasures: EcdirMeasure
	| EcdirMeasures EcdirMeasure;
EcdirMeasure: EcdirMeasureDef EOLC
	| EcdirMeasureDef FunctionalCommentString EOLC
	| EcdirMeasureDef EcdirMeasureArgs EOLC
	| EcdirMeasureDef EcdirMeasureArgs FunctionalCommentString EOLC;
EcdirMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_m
		addAnnotation(_ANNOTATION_ECDIR_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("TSTNECDIR/" + toTrimmedQString(sym(1)));
		countMeasure("TSTNECDIR");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
EcdirMeasureArgs: EcdirMeasureArg
	| EcdirMeasureArgs EcdirMeasureArg;
EcdirMeasureArg: ArgIcse
	| ArgObse
	| ArgPpm
	| ArgScale
	| ArgID;

	-- ECTH
MsrEcth: EcthDef
	| EcthDef EcthMeasures;
EcthDef: EcthDefBeg EOLC;
EcthDefBeg: TSTNECTH Number INSTRNAME;
/.
	case $rule_number: // *ECTH angl_gon scale_ID
		addAnnotation(_ANNOTATION_ECTH);
		addStyleSym(Styles::MeasureType, 1);
		addStyleSym(Styles::Normal, 2);

		startFolding();
		break;
./
EcthMeasures: EcthMeasure
	| EcthMeasures EcthMeasure;
EcthMeasure: EcthMeasureDef EOLC
	| EcthMeasureDef FunctionalCommentString EOLC
	| EcthMeasureDef EcthMeasureArgs EOLC
	| EcthMeasureDef EcthMeasureArgs FunctionalCommentString EOLC;
EcthMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_m
		addAnnotation(_ANNOTATION_ECTH_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("TSTNECTH/" + toTrimmedQString(sym(1)));
		countMeasure("TSTNECTH");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
EcthMeasureArgs: EcthMeasureArg
	| EcthMeasureArgs EcthMeasureArg;
EcthMeasureArg: ArgIcse
	| ArgObse
	| ArgPpm
	| ArgScale
	| ArgID;

	-- PLR3D
MsrPlr3D: Plr3DDef
	| Plr3DDef Plr3DMeasures;
Plr3DDef: Plr3DToken EOLC
	| Plr3DToken ArgTrgt EOLC;
Plr3DToken: TSTNPLR3D;
/.
	case $rule_number: // *PLR3D
		addAnnotation(_ANNOTATION_PLR3D);
		addStyleSym(Styles::MeasureType, 1);
		startFolding();
		break;
./
Plr3DMeasures: Plr3DMeasure
	| Plr3DMeasures Plr3DMeasure;
Plr3DMeasure: Plr3DMeasureDef EOLC
	| Plr3DMeasureDef FunctionalCommentString EOLC
	| Plr3DMeasureDef Plr3DMeasureArgs EOLC
	| Plr3DMeasureDef Plr3DMeasureArgs FunctionalCommentString EOLC;
Plr3DMeasureDef: POINTNAME Number Number Number;
/.
	case $rule_number: // point obs_ANGL obs_ZEND obs_DIST
		addAnnotation(_ANNOTATION_PLR3D_POINT);
		addStyleSym(Styles::Normal, 2);
		addStyleSym(Styles::Normal, 3);
		addStyleSym(Styles::Normal, 4);

		setMeasure("TSTNPLR3D/" + toTrimmedQString(sym(1)));
		countMeasure("TSTNPLR3D");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert({"ASE", "ZSE", "DSE"});
		}
		break;
./
Plr3DMeasureArgs: Plr3DMeasureArg
	| Plr3DMeasureArgs Plr3DMeasureArg;
Plr3DMeasureArg: ArgAse
	| ArgDse
	| ArgPpm
	| ArgTcse
	| ArgTh
	| ArgThse
	| ArgTrgt
	| ArgZse
	| ArgID;

	-- ZEND
MsrZend: ZendDef
	| ZendDef ZendMeasures;
ZendDef: ZendToken EOLC
	| ZendToken ArgTrgt EOLC;
ZendToken: TSTNZEND;
/.
	case $rule_number: // *ZEND
		addAnnotation(_ANNOTATION_ZEND);
		addStyleSym(Styles::MeasureType, 1);
		startFolding();
		break;
./
ZendMeasures: ZendMeasure
	| ZendMeasures ZendMeasure;
ZendMeasure: ZendMeasureDef EOLC
	| ZendMeasureDef FunctionalCommentString EOLC
	| ZendMeasureDef ZendMeasureArgs EOLC
	| ZendMeasureDef ZendMeasureArgs FunctionalCommentString EOLC;
ZendMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_ZEND
		addAnnotation(_ANNOTATION_ZEND_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("TSTNZEND/" + toTrimmedQString(sym(1)));
		countMeasure("TSTNZEND");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
ZendMeasureArgs: ZendMeasureArg
	| ZendMeasureArgs ZendMeasureArg;
ZendMeasureArg: ArgObse
	| ArgTcse
	| ArgTh
	| ArgThse
	| ArgTrgt
	| ArgID;

-- SCALE
MsrScale: MsrEcho
	| MsrEcve
	| MsrEscp;

	-- ECHO
MsrEcho: EchoDef
/. case $rule_number: ./
	| EchoDef EchoMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
EchoDef: EchoDefBeg EOLC;
EchoDefBeg: ECHO INSTRNAME;
/.
	case $rule_number: // *ECHO scale_ID
		addAnnotation(_ANNOTATION_ECHO);
		addStyleSym(Styles::MeasureType, 1);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		break;
./
EchoMeasures: EchoMeasure
	| EchoMeasures EchoMeasure;
EchoMeasure: EchoMeasureDef EOLC
	| EchoMeasureDef FunctionalCommentString EOLC
	| EchoMeasureDef EchoMeasureArgs EOLC
	| EchoMeasureDef EchoMeasureArgs FunctionalCommentString EOLC;
EchoMeasureDef: POINTNAME Number;
/.
	case $rule_number: // stn obs_m
		addAnnotation(_ANNOTATION_ECHO_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("ECHO/" + toTrimmedQString(sym(1)));
		countMeasure("ECHO");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
EchoMeasureArgs: EchoMeasureArg
	| EchoMeasureArgs EchoMeasureArg;
EchoMeasureArg: ArgIcse
	| ArgObse
	| ArgPpm
	| ArgScale
	| ArgID;

	-- ECVE
MsrEcve: EcveDef
/. case $rule_number: ./
	| EcveDef EcveMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
EcveDef: EcveDefBeg EOLC
	| EcveDefBeg ArgPtLine EOLC;
EcveDefBeg: ECVE INSTRNAME;
/.
	case $rule_number: // *ECVE scale_ID
		addAnnotation(_ANNOTATION_ECVE);
		addStyleSym(Styles::MeasureType, 1);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		break;
./
EcveMeasures: EcveMeasure
	| EcveMeasures EcveMeasure;
EcveMeasure: EcveMeasureDef EOLC
	| EcveMeasureDef FunctionalCommentString EOLC
	| EcveMeasureDef EcveMeasureArgs EOLC
	| EcveMeasureDef EcveMeasureArgs FunctionalCommentString EOLC;
EcveMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_m
		addAnnotation(_ANNOTATION_ECVE_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("ECVE/" + toTrimmedQString(sym(1)));
		countMeasure("ECVE");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
EcveMeasureArgs: EcveMeasureArg
	| EcveMeasureArgs EcveMeasureArg;
EcveMeasureArg: ArgIcse
	| ArgObse
	| ArgPpm
	| ArgScale
	| ArgID;

	-- ECSP
MsrEscp: EscpDef
/. case $rule_number: ./
	| EscpDef EscpMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
EscpDef: EscpDefBeg EOLC;
EscpDefBeg: ECSP POINTNAME POINTNAME INSTRNAME;
/.
	case $rule_number: // *ECSP A B scale_ID
		addAnnotation(_ANNOTATION_ECSP);
		addStyleSym(Styles::MeasureType, 1);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		break;
./
EscpMeasures: EscpMeasure
	| EscpMeasures EscpMeasure;
EscpMeasure: EscpMeasureDef EOLC
	| EscpMeasureDef FunctionalCommentString EOLC
	| EscpMeasureDef EscpMeasureArgs EOLC
	| EscpMeasureDef EscpMeasureArgs FunctionalCommentString EOLC;
EscpMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_m
		addAnnotation(_ANNOTATION_ECSP_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("ECSP/" + toTrimmedQString(sym(1)));
		countMeasure("ECSP");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
EscpMeasureArgs: EscpMeasureArg
	| EscpMeasureArgs EscpMeasureArg;
EscpMeasureArg: ArgIcse
	| ArgObse
	| ArgPpm
	| ArgScale
	| ArgID;

-- INCL
MsrIncl: MsrIncly;

	-- INCLY
MsrIncly: InclyDef
/. case $rule_number: ./
	| InclyDef InclyMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
InclyDef: InclyDefBeg EOLC;
InclyDefBeg: INCLY INSTRNAME;
/.
	case $rule_number: // *INCLY scale_ID
		addAnnotation(_ANNOTATION_INCLY);
		addStyleSym(Styles::MeasureType, 1);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		break;
./
InclyMeasures: InclyMeasure
	| InclyMeasures InclyMeasure;
InclyMeasure: InclyMeasureDef EOLC
	| InclyMeasureDef FunctionalCommentString EOLC
	| InclyMeasureDef InclyMeasureArgs EOLC
	| InclyMeasureDef InclyMeasureArgs FunctionalCommentString EOLC;
InclyMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_m
		addAnnotation(_ANNOTATION_INCLY);
		addStyleSym(Styles::Normal, 2);
		
		setMeasure("INCLY/" + toTrimmedQString(sym(1)));
		countMeasure("INCLY");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
InclyMeasureArgs: InclyMeasureArg
	| InclyMeasureArgs InclyMeasureArg;
InclyMeasureArg: ArgInstr
	| ArgObse
	| ArgPpm
	| ArgAc
	| ArgAcse
	| ArgRf
	| ArgRfse
	| ArgID;

-- HLSR
MsrHlsr: MsrEcws;

	-- ECWS
MsrEcws: EcwsDef
/. case $rule_number: ./
	| EcwsDef EcwsMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
EcwsDef: EcwsDefBeg EOLC
	| EcwsDefBeg ArgWsid EOLC;

EcwsDefBeg: ECWS INSTRNAME Number;
/.
	case $rule_number: // *ECWS instr_ID sigma_water_surface_mm
		addAnnotation(_ANNOTATION_ECWS);
		addStyleSym(Styles::MeasureType, 1);
		addStyleSym(Styles::Normal, 3);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		break;
./
EcwsMeasures: EcwsMeasure
	| EcwsMeasures EcwsMeasure;
EcwsMeasure: EcwsMeasureDef EOLC
	| EcwsMeasureDef FunctionalCommentString EOLC
	| EcwsMeasureDef EcwsMeasureArgs EOLC
	| EcwsMeasureDef EcwsMeasureArgs FunctionalCommentString EOLC;
EcwsMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point obs_m
		addAnnotation(_ANNOTATION_ECWS);
		addStyleSym(Styles::Normal, 2);
		
		setMeasure("ECWS/" + toTrimmedQString(sym(1)));
		countMeasure("ECWS");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
EcwsMeasureArgs: EcwsMeasureArg
	| EcwsMeasureArgs EcwsMeasureArg;
EcwsMeasureArg: ArgInstr
	| ArgObse
	| ArgIhse
	| ArgIcse
	| ArgWsse
	| ArgID;

-- WPSR
MsrWpsr: MsrEcwi;

	-- ECWI
MsrEcwi: EcwiDef
/. case $rule_number: ./
	| EcwiDef EcwiMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
EcwiDef: EcwiDefBeg EOLC
	|  EcwiDefBeg EcwiDefArgs EOLC;
EcwiDefArgs: EcwiDefArg
	| EcwiDefArgs EcwiDefArg;
EcwiDefArg: ArgWiid
	| ArgSagfix
	| ArgSagfix ArgSagse;
EcwiDefBeg: ECWI INSTRNAME Number Number POINTNAME POINTNAME;
/.
	case $rule_number: // *ECWI    instr_ID    sag_distance_m    sigma_wire_mm   anchorpoint_1   anchorpoint_2
		addAnnotation(_ANNOTATION_ECWI);
		addStyleSym(Styles::MeasureType, 1);
		addStyleSym(Styles::Normal, 3);
		addStyleSym(Styles::Normal, 4);
		_contexts.push_back(Context::MEASURES);

		startFolding();
		break;
./
EcwiMeasures: EcwiMeasure
	| EcwiMeasures EcwiMeasure;
EcwiMeasure: EcwiMeasureDef EOLC
	| EcwiMeasureDef FunctionalCommentString EOLC
	| EcwiMeasureDef EcwiMeasureArgs EOLC
	| EcwiMeasureDef EcwiMeasureArgs FunctionalCommentString EOLC;
EcwiMeasureDef: POINTNAME Number Number;
/.
	case $rule_number: // stn obs_X_m obs_Z_m
		addAnnotation(_ANNOTATION_ECWI);
		addStyleSym(Styles::Normal, 2);
		addStyleSym(Styles::Normal, 3);
		
		setMeasure("ECWI/" + toTrimmedQString(sym(1)));
		countMeasure("ECWI");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert({"ZSE"});
		}
		break;
./
EcwiMeasureArgs: EcwiMeasureArg
	| EcwiMeasureArgs EcwiMeasureArg;
EcwiMeasureArg: ArgInstr
	| ArgXse
	| ArgZse
	| ArgXicse
	| ArgZicse
	| ArgID;

-- NOTYPE
MsrNoType: MsrDver
	| MsrObsxyz
	| MsrRadi;

	-- DVER
MsrDver: DverDef
/. case $rule_number: ./
	| DverDef DverMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
DverDef: DverToken EOLC
	| DverToken DverDefArg EOLC;
DverToken: DVER;
/.
	case $rule_number: // *DVER
		addAnnotation(_ANNOTATION_DVER);
		addStyleSym(Styles::MeasureType, 1);
		startFolding();
		_contexts.push_back(Context::MEASURES);
		break;
./
DverDefArg: Number;
/.
	ADDSTYLE($rule_number, MeasureParameter);
./
DverMeasures: DverMeasure
	| DverMeasures DverMeasure;
DverMeasure: DverMeasureDef EOLC
	| DverMeasureDef FunctionalCommentString EOLC
	| DverMeasureDef DverMeasureArgs EOLC
	| DverMeasureDef DverMeasureArgs FunctionalCommentString EOLC;
DverMeasureDef: POINTNAME POINTNAME Number;
/.
	case $rule_number: // point1 point2 obs_m
		addAnnotation(_ANNOTATION_DVER_POINT);
		addStyleSym(Styles::Normal, 3);

		setMeasure("DVER/" + toTrimmedQString(sym(1)) + toTrimmedQString(sym(2)));
		countMeasure("DVER", 1);
		countMeasure("DVER", 2);
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
		}
		break;
./
DverMeasureArgs: DverMeasureArg
	| DverMeasureArgs DverMeasureArg;
DverMeasureArg: ArgDcor
	| ArgObse
	| ArgID;

	-- OBSXYZ
MsrObsxyz: ObsxyzDef
/. case $rule_number: ./
	| ObsxyzDef ObsxyzMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
ObsxyzDef: ObsxyzToken EOLC;
ObsxyzToken: OBSXYZ;
/.
	case $rule_number: // *OBSXYZ
		addAnnotation(_ANNOTATION_OBSXYZ);
		addStyleSym(Styles::MeasureType, 1);
		startFolding();
		_contexts.push_back(Context::MEASURES);
		break;
./
ObsxyzMeasures: ObsxyzMeasure
	| ObsxyzMeasures ObsxyzMeasure;
ObsxyzMeasure: ObsxyzMeasureDef EOLC
	| ObsxyzMeasureDef FunctionalCommentString EOLC
	| ObsxyzMeasureDef ObsxyzMeasureArgs EOLC
	| ObsxyzMeasureDef ObsxyzMeasureArgs FunctionalCommentString EOLC;
ObsxyzMeasureDef: POINTNAME Number Number Number Number Number Number;
/.
	case $rule_number: // point obsX_m obsY_m obsZ_m sigmaX_mm  sigmaY_mm  sigmaZ_mm
		addAnnotation(_ANNOTATION_OBSXYZ_POINT);
		addStyleSym(Styles::PointCoord, 2);
		addStyleSym(Styles::PointCoord, 3);
		addStyleSym(Styles::PointCoord, 4);
		checkDouble(5, "sigmaX_mm");
		checkDouble(6, "sigmaY_mm");
		checkDouble(7, "sigmaZ_mm");

		setMeasure("OBSXYZ/" + toTrimmedQString(sym(1)));
		countMeasure("OBSXYZ");
		break;
./
ObsxyzMeasureArgs: ObsxyzMeasureArg
	| ObsxyzMeasureArgs ObsxyzMeasureArg;
ObsxyzMeasureArg: ArgID;

	-- RADI
MsrRadi: RadiDef
/. case $rule_number: ./
	| RadiDef RadiMeasures;
/.
	case $rule_number:
		if (!_contexts.empty())
			_contexts.pop_back();
		break;
./
RadiDef: RadiDefBeg EOLC
/. case $rule_number: ./
	| RadiDefBeg RadiDefArgs EOLC;
/.
	case $rule_number:
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("ACST");
		}
		break;
./
RadiDefArgs: ArgAcst;
RadiDefBeg: RADI Number
/.
	case $rule_number: // *RADI sigma_mm
		checkDouble(2, "sigma_mm");
		[[fallthrough]];
./
	| RADI;
/.
	case $rule_number: // *RADI sigma_mm
		addAnnotation(_ANNOTATION_RADI);
		addStyleSym(Styles::MeasureType, 1);
		startFolding();
		_contexts.push_back(Context::MEASURES);
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
		}
		break;
./
RadiMeasures: RadiMeasure
	| RadiMeasures RadiMeasure;
RadiMeasure: RadiMeasureDef EOLC
	| RadiMeasureDef FunctionalCommentString EOLC
	| RadiMeasureDef RadiMeasureArgs EOLC
	| RadiMeasureDef RadiMeasureArgs FunctionalCommentString EOLC;
RadiMeasureDef: POINTNAME Number;
/.
	case $rule_number: // point bearing_gons
		addAnnotation(_ANNOTATION_RADI_POINT);
		addStyleSym(Styles::Normal, 2);

		setMeasure("RADI/" + toTrimmedQString(sym(1)));
		countMeasure("RADI");
		if (_secondRun && editor())
		{
			int line = -1, pos = -1;
			editor()->lineIndexFromPosition(sym(1).position(), &line, &pos);
			_sigmaLines[line].insert("OBSE");
			_sigmaLines[line].insert("ACST");
		}
		break;
./
RadiMeasureArgs: RadiMeasureArg
	| RadiMeasureArgs RadiMeasureArg;
RadiMeasureArg: ArgObse
	| ArgAcst
	| ArgID;

---- END

End: EndToken EOLC;
EndToken: END
/. case $rule_number: ./
	| FIN;
/.
	case $rule_number: // *END *FIN
		_isPostEnd = true;
		_endPos = sym(1).position();
		addStyleSym(Styles::StartEnd, 1);
		break;
./

-- Everything after END should not be styled
-- nextToken has special logic at the end of processing (_isPostEnd) and everything after is a WORD or EOL_SYMBOL
PostEnd: PostEndRubbish EOLC;
PostEndRubbish: PostEndTokens
	| PostEndRubbish EOLC PostEndTokens;
-- Can be anything
PostEndTokens: WORD
	| PostEndTokens WORD;

----------------------------------------------------------- ENDRULES

/.
	} // of the switch
}

int LGCInputLexerIO::nextToken(QStringRef value)
{
	if(_isPostEnd)
		return LGCInputLexerIO::WORD;

	static const QRegularExpression numericrgx = ([]() -> QRegularExpression {
		QRegularExpression rgx(R"""(^-?[[:digit:]]*\.?[[:digit:]]+$)""");
		rgx.optimize();
		return rgx;
	})();

	QStringRef tval = value.trimmed();
	if (!tval.isEmpty())
	{
		// comment
		if (commentChars().contains(tval[0]))
			return LGCInputLexerIO::COMMENTSTRING;
		// functional comment
		if (tval[0] == _functionalComment)
			return LGCInputLexerIO::DOLLAR;
		// numbers
		if (numericrgx.match(tval).hasMatch())
		{
			bool ok;
			tval.toInt(&ok);
			if (ok)
				return LGCInputLexerIO::INTEGER;
			tval.toDouble(&ok);
			if (ok)
				return LGCInputLexerIO::DECIMAL;
		}
		// keywords / parameters
		const bool ctxt = value.position() != _currentLine.position() && (!_inlineContexts.empty() || (!_contexts.empty() && _contexts.back() == Context::MEASURES));
		if ((value.position() == _currentLine.position() && tval[0] == '*') || ctxt)
		{
			for (int i = 0; i < LGCInputLexerIO::TERMINAL_COUNT; i++)
			{
				if (tval == LGCInputLexerIO::spell[i])
					return i;
			}
			if (ctxt)
				return LGCInputLexerIO::WORD;
			return LGCInputLexerIO::UNKNOWN;
		}
	}

	return LGCInputLexerIO::WORD;
}
./
