/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef LGCINPUTTEXT_HPP
#define LGCINPUTTEXT_HPP

#include <editors/text/TextEditorWidget.hpp>

struct LGCMeasures;
class QNetworkReply;
enum ToggleCommentMode : int;

/**
 * Text editor for input points in LGC.
 */
class LGCInputText : public TextEditorWidget
{
	Q_OBJECT

public:
	LGCInputText(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~LGCInputText() override;

	// SGraphicalWidget
	virtual ShareablePointsList getContent() const override;

	// LGCInputText
	const LGCMeasures &lgcMeasures();

public slots:
	// SGraphicalWidget
	virtual void setContent(const ShareablePointsList &spl) override;
	virtual void updateUi(const QString &pluginName) override;

protected:
	// SGraphicalWidget
	virtual bool _open(const QString &path) override;
	virtual void _newEmpty() override;

private:
	QByteArray fromMime(const QMimeData *mimedata);
	QMimeData *toMime(const QByteArray &text, QMimeData *mimedata);
	void createActions();
	void toggleComment(ToggleCommentMode mode);
	void toggleSigma(const QString &keyword);
	bool canToggleSigma(int linenbr, const QString &keyword);
	void transferPointDeclaration();
	void setupEditor();

	/** Send update points request to Geode using @NetworkManager connection */
	void updatePoints();
	/** Send update bearings request to Geode using @NetworkManager connection */
	void updateBearings();
	/** Checks if given point @name at given @line is a point definition/declaration */
	bool isPointDefinition(const QString &name, const int line);
	/** Posts request and returns reply */
	QNetworkReply *sendRequest(const QJsonObject &outgoingData, const QUrl &url, const QString &preLog);
	/** Return true if the connection was succesfully established, false otherwise */
	bool checkResponse(QNetworkReply *reply, const QString &preLog);
	/** When response is ready, parse the received data and update the outdated points on success */
	void responseUpdate(QNetworkReply *reply, const QList<QPair<int, int>> linesToUpdate, std::function<void(const QJsonObject &, TextEditor *, int)> addLine);

	/** Convert PLR3D to ANGL/ZEND/DIST or back from it */
	void convertTSTN();
	std::tuple<int, QString> convertTSTN_PLR3DTOANGL(int currLine, const QMap<QString, QStringList> &measuresKeywords, const std::vector<int> &measuresPositions);
	std::tuple<int, QString> convertTSTN_ANGLTOPLR3D(int currLine, const QMap<QString, QStringList> &measuresKeywords, const std::vector<int> &measuresPositions);
	/** Beginning to *END search*/
	void selectToEnd();

private:
	/** pimpl */
	class _LGCInputText_pimpl;
	std::unique_ptr<_LGCInputText_pimpl> _pimpl;
};

#endif // LGCINPUTTEXT_HPP
