#include "MeasureViewer.hpp"
#include "ui_MeasureViewer.h"

#include <QAbstractTableModel>
#include <QSortFilterProxyModel>

#include <Qsci/qsciscintilla.h>

#include "LGCInputText.hpp"
#include "LGCMeasures.hpp"

const QString LBLTEMPLATE = QObject::tr("Total: %1 measure line(s) displayed");

constexpr int COLUMN_COUNT = 3;
constexpr int COLUMN_POSITION = 0;
constexpr int COLUMN_POINTNAME = 1;
constexpr int COLUMN_INSTRUMENT = 2;

class MeasureTableModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	MeasureTableModel(QsciScintilla *editor, std::function<const LGCMeasures &(void)> getMeasures, QWidget *parent) :
		QAbstractTableModel(parent), _editor(editor), _getMeasures(getMeasures)
	{
		_table.reserve(100);
	}

	// QAbstractTableModel
	virtual int rowCount(const QModelIndex & = QModelIndex()) const override { return (int)_table.size(); }
	virtual int columnCount(const QModelIndex & = QModelIndex()) const override { return COLUMN_COUNT; }
	virtual QVariant data(const QModelIndex &index, int role = Qt::ItemDataRole::DisplayRole) const override
	{
		if (!index.isValid() || index.column() >= COLUMN_COUNT || index.row() >= _table.size()
			|| (role != Qt::ItemDataRole::ToolTipRole && role != Qt::ItemDataRole::DisplayRole && role != Qt::ItemDataRole::FontRole))
			return QVariant();

		static const QFont _font = ([]() -> QFont {
			QFont font("Arial", 9);
			font.setStyleHint(QFont::StyleHint::Monospace);
			return font;
		})();

		if (role == Qt::ItemDataRole::ToolTipRole)
			return _table[index.row()].representation;
		if (role == Qt::ItemDataRole::FontRole)
			return _font;

		// DisplayRole
		switch (index.column())
		{
		case COLUMN_POSITION:
			return QString::number(_table[index.row()].position + 1);
		case COLUMN_POINTNAME:
			return _table[index.row()].name;
		case COLUMN_INSTRUMENT:
			return _table[index.row()].others;
		}
		return QVariant();
	}
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::ItemDataRole::DisplayRole) const override
	{
		if (role != Qt::ItemDataRole::DisplayRole || orientation != Qt::Orientation::Horizontal || section >= COLUMN_COUNT)
			return QVariant();

		switch (section)
		{
		case COLUMN_POSITION:
			return "#";
		case COLUMN_POINTNAME:
			return tr("Name");
		case COLUMN_INSTRUMENT:
			return tr("Instrument or Frame");
		}
		return QVariant();
	}

	// MeasureTableModel
	void onDeleteAll()
	{
		beginResetModel();
		_table.clear();
		endResetModel();
	}
	void insertRow(LGCMeasures::Value value)
	{
		beginInsertRows({}, (int)_table.size(), (int)_table.size());
		// change position to line
		value.position = _editor->SendScintilla(QsciScintillaBase::SCI_LINEFROMPOSITION, value.position);
		_table.push_back(value);
		endInsertRows();
	}

public slots:
	void rebuildModel(int line)
	{
		const QString pointName = _editor->text(line).simplified().split(' ').first();
		const LGCMeasures &lgcmeasures = _getMeasures();

		// not a point name
		if (!lgcmeasures.points.count(pointName))
			return;

		// clear table
		onDeleteAll();

		// create rows
		QList<LGCMeasures::Value> pointMeasures;
		pointMeasures.reserve((int)lgcmeasures.measures.size()); // Reserve memory for potential matches

		// filter the relevant
		for (const auto &[key, value] : lgcmeasures.measures)
		{
			if (value.name.contains(pointName)) // if is part of the key
				insertRow(value);
		}
	}

private:
	QsciScintilla *_editor;
	std::function<const LGCMeasures &(void)> _getMeasures;
	QList<LGCMeasures::Value> _table;
};

#include "MeasureViewer.moc"

class MeasureViewer::_MeasureViewer_pimpl
{
public:
	std::unique_ptr<Ui::MeasureViewer> ui;
	MeasureTableModel *model = nullptr;
	QsciScintilla *editor = nullptr;
	int prevLine = -1;
};

MeasureViewer::MeasureViewer(QsciScintilla *editor, std::function<const LGCMeasures &(void)> getMeasures, QWidget *parent) :
	QWidget(parent), _pimpl(std::make_unique<_MeasureViewer_pimpl>())
{
	_pimpl->ui = std::make_unique<Ui::MeasureViewer>();
	_pimpl->ui->setupUi(this);

	_pimpl->editor = editor;

	setupTable(editor, getMeasures);
}

MeasureViewer::~MeasureViewer() = default;

void MeasureViewer::showEvent(QShowEvent *event)
{
	rebuildTable();
	QWidget::showEvent(event);
}

void MeasureViewer::setupTable(QsciScintilla *editor, std::function<const LGCMeasures &(void)> getMeasures)
{
	// Table model
	_pimpl->model = new MeasureTableModel(editor, getMeasures, this);

	// Sortable proxy
	QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(this);
	proxyModel->setSourceModel(_pimpl->model);
	_pimpl->ui->tableView->setModel(proxyModel);

	// Table rebuilding and functioning
	// Rebuild table only if:
	// - cursor has changed to different line
	// - if line contents were modified
	// - if MeasureViewer is shown
	connect(editor, &QsciScintilla::cursorPositionChanged, this, &MeasureViewer::rebuildTable);
	connect(editor, &QsciScintilla::SCN_MODIFIED, [this](int, int mtype, const char *, int, int, const int line, int, int, int, int) {
		constexpr int filter = QsciScintilla::SC_MOD_INSERTTEXT | QsciScintilla::SC_MOD_DELETETEXT | QsciScintilla::SC_MOD_CHANGEMARKER;
		if (!(mtype & filter))
			return;
		rebuildTable(line);
	});
	// Jump to position on double click
	connect(_pimpl->ui->tableView, &QTableView::doubleClicked, [this, editor, proxyModel](const QModelIndex &index) {
		if (!index.isValid() || index.column() >= COLUMN_COUNT || index.row() >= _pimpl->model->rowCount())
			return;

		// Map the proxy index to the source model index
		QModelIndex sourceIndex = proxyModel->mapToSource(index);
		const int linenbr = _pimpl->model->data(sourceIndex.siblingAtColumn(0)).toInt() - 1;

		editor->SendScintilla(QsciScintillaBase::SCI_GOTOLINE, linenbr);
		editor->ensureLineVisible(linenbr);
		editor->setFocus();
	});

	// Table looks
	_pimpl->ui->tableView->horizontalHeader()->setSortIndicatorShown(true);
	_pimpl->ui->tableView->horizontalHeader()->setSortIndicator(0, Qt::SortOrder::AscendingOrder);
	_pimpl->ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
	_pimpl->ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
}

void MeasureViewer::rebuildTable(int line)
{
	// Rebuild only when visible and line was changed
	if (!isVisible())
		return;
	if (line == -1)
		line = _pimpl->editor->SendScintilla(QsciScintilla::SCI_LINEFROMPOSITION, _pimpl->editor->SendScintilla(QsciScintilla::SCI_GETCURRENTPOS));
	if (line == _pimpl->prevLine)
		return;
	_pimpl->prevLine = line;
	_pimpl->model->rebuildModel(line);

	_pimpl->ui->lblNbDisplayed->setText(LBLTEMPLATE.arg(_pimpl->model->rowCount()));
}
