#include <set>

#include <QRegularExpression>

#include <Qsci/qsciabstractapis.h>

#include <ShareablePoints/SPIOException.hpp>
#include <ShareablePoints/ShareableExtraInfos.hpp>
#include <ShareablePoints/ShareableFrame.hpp>
#include <ShareablePoints/ShareableParams.hpp>
#include <ShareablePoints/ShareablePoint.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <ShareablePoints/ShareablePosition.hpp>
#include <StringManager.h>
#include <utils/StylesHelper.hpp>

#include "LGCInputLexerIO.hpp"
#include "LGCMeasures.hpp"
#include "LGCPlugin.hpp"

class LGCInputLexerIO::LGCLexerAPIs : public QsciAbstractAPIs
{
public:
	LGCLexerAPIs(LGCInputLexerIO *lexer) : QsciAbstractAPIs(lexer), _lexer(lexer)
	{
		const char *tmp;
		for (size_t i = LGCInputLexerIO::VariousConstants::KEYWORDS_START + 1; i < LGCInputLexerIO::VariousConstants::TERMINAL_COUNT; i++)
		{
			tmp = LGCInputLexerIO::spell[i];
			if (!tmp)
				continue;

			if (tmp[0] == '*')
				_keywords.append(tmp);
			else if (i > LGCInputLexerIO::VariousConstants::MSR_KEYWORDS_START && i < LGCInputLexerIO::VariousConstants::MSR_KEYWORDS_END)
				_kwmsrparameters.append(tmp);
			else if (i > LGCInputLexerIO::VariousConstants::FR_KEYWORDS_START && i < LGCInputLexerIO::VariousConstants::FR_KEYWORDS_END)
				_kwmfrparameters.append(tmp);
		}
	}

	// QsciAbstractAPIs
	virtual void updateAutoCompletionList(const QStringList &context, QStringList &list) override
	{
		if (context.empty())
			return;

		QStringList apis;
		// fill apis depending on the context
		const auto &editor = *_lexer->editor();
		int line = 0, index = 0;
		editor.getCursorPosition(&line, &index);
		const QStringList realContext = editor.text(line).left(index).split(QRegularExpression(R"""(\s)"""),
#if (QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
			QString::SplitBehavior::SkipEmptyParts
#else
			Qt::SplitBehaviorFlags::SkipEmptyParts
#endif
		);
		switch (realContext.size())
		{
		case 1:
			apis = _keywords + getPoints() + getInstruments() + getTargets();
			break;
		case 2:
			if (kwToTwoPoints.find(realContext.first()) != std::cend(kwToTwoPoints))
			{
				apis = getPoints();
				break;
			}
			else if (kwToPoints.find(realContext.first()) != std::cend(kwToPoints))
			{
				apis = getPoints();
				break;
			}
			else if (kwToInstr.find(realContext.first()) != std::cend(kwToInstr))
			{
				apis = getInstruments();
				break;
			}
		case 3:
			if (kwToTwoPoints.find(realContext.first()) != std::cend(kwToTwoPoints))
			{
				apis = getPoints();
				break;
			}
			else if (kwTotoInstr.find(realContext.first()) != std::cend(kwTotoInstr))
			{
				apis = getInstruments();
				break;
			}
		case 4:
			if (kwTototoInstr.find(realContext.first()) != std::cend(kwTototoInstr))
			{
				apis = getInstruments();
				break;
			}
		default:
			if (realContext.size() > 2 && kwToTarget.find(realContext[realContext.size() - 2]) != std::cend(kwToTarget))
				apis = getTargets();
			else if (realContext.size() > 2 && kwToPoints.find(realContext[realContext.size() - 2]) != std::cend(kwToPoints))
				apis = getPoints();
			else if (realContext.size() > 3 && kwToInstr.find(realContext[realContext.size() - 2]) != std::cend(kwToInstr))
				apis = getInstruments();
			else if (realContext.size() > 9 && kwToFrameParameter.find(realContext.first()) != std::cend(kwToFrameParameter))
				apis = _kwmfrparameters;
			else if (realContext.size() >= 2)
				apis = _kwmsrparameters;
		}

		// fill list with values from apis that matches what the user typed
		QStringRef beginning = &context.last();
		apis.sort(toQtCaseSensitivity());
		list.reserve(apis.size());
		for (const auto &api : apis)
		{
			if (api.compare(beginning, toQtCaseSensitivity()) < 0)
				continue;
			else if (api.startsWith(beginning, toQtCaseSensitivity()))
				list.append(api);
			else
				break;
		}
	}

	virtual QStringList callTips(const QStringList &, int, QsciScintilla::CallTipsStyle, QList<int> &) override { return QStringList(); }

private:
	Qt::CaseSensitivity toQtCaseSensitivity() { return lexer()->caseSensitive() ? Qt::CaseSensitivity::CaseSensitive : Qt::CaseSensitivity::CaseInsensitive; }
	QStringList getPoints()
	{
		QStringList points;
		for (const auto &[key, value] : _lexer->_measures.points)
			points.append(value.name);
		return points;
	}
	QStringList getInstruments()
	{
		QStringList instruments;
		for (const auto &[key, value] : _lexer->_measures.instruments)
			instruments.append(value.name);
		return instruments;
	}
	QStringList getTargets()
	{
		QStringList targets;
		for (const auto &[key, value] : _lexer->_measures.targets)
			targets.append(value.name);
		return targets;
	}

private:
	/** List of keywords that is followed by 2 point names */
	static inline std::set<QString> kwToTwoPoints = {"*ESCP"};
	/** List of keywords that are followed by a point name */
	static inline const std::set<QString> kwToPoints = {"*CAM", "*DSPT", "*ESCP", "*ORIE", "*TSTN", "RefPt", "PtLine"};
	/** List of keywords that are followed by 2 things and then an instrument name */
	static inline const std::set<QString> kwTototoInstr = {"*ECDIR", "*ESCP"};
	/** List of keywords that are followed by something, and then an instrument name */
	static inline const std::set<QString> kwTotoInstr = {"*CAM", "*DLEV", "*DSPT", "*ECTH", "*ORIE", "*TSTN"};
	/** List of keywords that are followed by an instrument name */
	static inline const std::set<QString> kwToInstr = {"*ECHO", "*ECVE", "SCALE", "INSTR", "*INCLY"};
	/** List of keywords that are followed by a target name */
	static inline const std::set<QString> kwToTarget = {"TRGT"};
	/** List of keywords that are followed by Frame parameters */
	static inline const std::set<QString> kwToFrameParameter = {"*FRAME"};

private:
	LGCInputLexerIO *_lexer = nullptr;
	QStringList _keywords;
	QStringList _kwmsrparameters;
	QStringList _kwmfrparameters;
};

LGCInputLexerIO::LGCInputLexerIO(QObject *parent) : LexerParser(parent)
{
	commentChars("%");
	// set APIs for autocompletion
	setAPIs(new LGCLexerAPIs(this));
}

const std::string &LGCInputLexerIO::getMIMEType() const
{
	static const std::string mime = LGCPlugin::_baseMimetype().toStdString() + ".pointRAW";
	return mime;
}

const char *LGCInputLexerIO::keywords(int) const
{
	return nullptr;
}

QString LGCInputLexerIO::description(int style) const
{
	switch (style)
	{
	case Styles::Default:
		return "Default";
	case Styles::Comment:
		return "Comment";
	case Styles::FunctionalComment:
		return "FunctionalComment";
	case Styles::Normal:
		return "Normal";
	case Styles::StartEnd:
		return "Start or end keyword";
	// Options
	case Styles::DatumOption:
		return "Datum option";
	case Styles::CalculationOption:
		return "Calculation option";
	case Styles::OutputOption:
		return "Output option";
	case Styles::OptionParameter:
		return "Option parameter";
	// Intruments
	case Styles::InstrumentType:
		return "Instrument type";
	case Styles::InstrumentName:
		return "Instrument name";
	case Styles::TargetName:
		return "Target name";
	// Points
	case Styles::PointType:
		return "Point type";
	case Styles::PointName:
		return "Point name";
	case Styles::PointCoord:
		return "Point coordinate";
	// Measures
	case Styles::MeasureType:
		return "Measure type";
	// Frames
	case Styles::Frame:
		return "Frame";
	case Styles::FrameName:
		return "Frame Name";
	}

	return QString();
}

const char *LGCInputLexerIO::wordCharacters() const
{
	return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._-+*éèà[]()";
}

bool LGCInputLexerIO::defaultEolFill(int) const
{
	return false;
}

QColor LGCInputLexerIO::defaultColor(int style) const
{
	switch (style)
	{
	case Styles::Default:
		return Qt::GlobalColor::darkGray;
	case Styles::Comment:
		return Qt::GlobalColor::darkGreen;
	case Styles::FunctionalComment:
		return QColor(Qt::GlobalColor::darkGreen).darker(133);
	case Styles::Normal:
		return getThemeColor(QColor(0, 43, 54, 255)); // almost black
	case Styles::StartEnd:
		return QColor(220, 50, 47, 255); // red orange
	case Styles::PointType:
		return QColor(181, 137, 0, 255); // brown
	case Styles::MeasureType:
		return QColor(203, 75, 22, 255); // orange
	// Options
	case Styles::DatumOption:
		return Qt::GlobalColor::darkBlue;
	case Styles::CalculationOption:
		return Qt::GlobalColor::blue;
	case Styles::OutputOption:
		return QColor(57, 135, 214, 255); // lighter blue
	case Styles::OptionParameter:
		return getThemeColor(Qt::GlobalColor::black); 
	// Intruments
	case Styles::InstrumentType:
	case Styles::InstrumentName:
	case Styles::TargetName:
		return QColor(108, 113, 196, 255); // turquoise
	// Points
	case Styles::PointName:
		return Qt::GlobalColor::darkCyan;
	case Styles::PointCoord:
		return Qt::GlobalColor::magenta;
	// Frames
	case Styles::Frame:
		return Qt::GlobalColor::darkRed;
	case Styles::FrameName:
		return QColor(182, 8, 82, 255); // Jazzberry Jam
	}

	return ShareablePointsListIOLexer::defaultColor(style);
}

QFont LGCInputLexerIO::defaultFont(int style) const
{
	QFont baseFont("Courier New", 10);
	baseFont.setStyleHint(QFont::StyleHint::Monospace);
	baseFont.setWeight(QFont::Weight::Light);

	switch (style)
	{
	case Styles::Default:
	case Styles::Normal:
	case Styles::InstrumentName:
	case Styles::PointCoord:
		return baseFont;
	case Styles::Comment:
	case Styles::FunctionalComment:
	case Styles::TargetName:
		baseFont.setItalic(true);
		return baseFont;
	case Styles::DatumOption:
	case Styles::CalculationOption:
	case Styles::OutputOption:
		baseFont.setItalic(true);
		[[fallthrough]];
	case Styles::StartEnd:
	case Styles::OptionParameter:
	case Styles::InstrumentType:
	case Styles::PointName:
	case Styles::PointType:
	case Styles::MeasureType:
	case Styles::Frame:
	case Styles::FrameName:
		baseFont.setWeight(QFont::Weight::Black);
		return baseFont;
	}

	return ShareablePointsListIOLexer::defaultFont(style);
}

QColor LGCInputLexerIO::defaultPaper(int style) const
{
	return ShareablePointsListIOLexer::defaultPaper(style);
}

ShareablePointsList LGCInputLexerIO::read(const std::string &contents)
{
	_curList = ShareablePointsList();
	_curFrame = &_curList.getRootFrame();

	parseString(contents);

	return std::move(_curList);
}

ShareableExtraInfos LGCInputLexerIO::readExtraInfos(const std::string &)
{
	return ShareableExtraInfos();
}

ShareableFrame LGCInputLexerIO::readFrame(const std::string &contents)
{
	_curList = ShareablePointsList();
	_curFrame = &_curList.getRootFrame();

	std::string toparse = ltrim(contents);
	if (toparse.size() == 0 || toparse[0] != '*')
		toparse = "*POIN\n" + toparse;

	toparse = R"""(*TITR
titre
*OLOC
*INSTR
)""" + toparse
		+ R"""(
*END)""" + '\n';
	parseString(toparse);

	return std::move(_curList.getRootFrame());
}

ShareableParams LGCInputLexerIO::readParams(const std::string &contents)
{
	_curList = ShareablePointsList();
	_curFrame = &_curList.getRootFrame();

	std::string toparse = R"""(*TITR
titre
)""" + contents
		+ R"""(
*INSTR
*POIN
point 0 0 0
*OBSXYZ
point 0 0 0 0 0 0
*END)""" + '\n';
	parseString(toparse);

	return std::move(_curList.getParams());
}

ShareablePoint LGCInputLexerIO::readPoint(const std::string &contents)
{
	_curList = ShareablePointsList();
	_curFrame = &_curList.getRootFrame();

	std::string toparse = ltrim(contents);
	if (toparse.size() == 0 || toparse[0] != '*')
		toparse = "*POIN\n" + toparse;

	toparse = R"""(*TITR
titre
*OLOC
*INSTR
)""" + toparse
		+ R"""(
*OBSXYZ
point 0 0 0 0 0 0
*END)""" + '\n';
	parseString(toparse);

	if (_curList.getRootFrame().getPoints().empty())
		throw SPIOException("No point read", "", std::move(toparse));
	auto &p = _curList.getRootFrame().getPoint(0);
	p.parent = nullptr;
	return std::move(p);
}

ShareablePosition LGCInputLexerIO::readPosition(const std::string &contents)
{
	_curList = ShareablePointsList();
	_curFrame = &_curList.getRootFrame();

	std::string toparse = R"""(*TITR
titre
*OLOC
*INSTR
*POIN
point )"""
		+ contents + R"""(
*OBSXYZ
point 0 0 0 0 0 0
*END)""" + '\n';
	parseString(toparse);

	if (_curList.getRootFrame().getPoints().empty())
		throw SPIOException("No point read", "", std::move(toparse));
	return std::move(_curList.getRootFrame().getPoint(0).position);
}

std::string LGCInputLexerIO::write(const ShareablePointsList &list)
{
	std::string result = "*TITR\n";

	result += list.getTitle().empty() ? "title\n\n" : list.getTitle() + "\n\n";
	result += write(list.getParams()) + "\n";
	result += "*INSTR\n\n";

	_firstPointWritten = true;
	result += write(list.getRootFrame());

	result += "\n*END\n";

	return result;
}

std::string LGCInputLexerIO::write(const ShareableFrame &frame)
{
	auto getPointType = [](const ShareablePosition &pos) -> std::string {
		if (!pos.isfreex && !pos.isfreey && !pos.isfreez)
			return "*CALA";
		if (pos.isfreex && pos.isfreey && pos.isfreez)
			return "*POIN";
		if (pos.isfreex && pos.isfreey && !pos.isfreez)
			return "*VXY";
		if (pos.isfreex && !pos.isfreey && pos.isfreez)
			return "*VXZ";
		if (!pos.isfreex && pos.isfreey && pos.isfreez)
			return "*VYZ";
		if (!pos.isfreex && !pos.isfreey && pos.isfreez)
			return "*VZ";
		return "";
	};

	std::string result;
	bool ff = _firstFrameWritten;
	_firstFrameWritten = true;

	// FRAME header
	if (ff)
	{
		result = "*FRAME ";
		result += frame.getName().length() ? frame.getName() : "NotNamedFrame";
		result += ' ' + std::to_string(frame.getTranslation().x);
		result += ' ' + std::to_string(frame.getTranslation().y);
		result += ' ' + std::to_string(frame.getTranslation().z);
		result += ' ' + std::to_string(frame.getRotation().x);
		result += ' ' + std::to_string(frame.getRotation().y);
		result += ' ' + std::to_string(frame.getRotation().z);
		result += ' ' + std::to_string(frame.getScale());
		if (frame.getTranslation().isfreex)
			result += " TX";
		if (frame.getTranslation().isfreey)
			result += " TY";
		if (frame.getTranslation().isfreez)
			result += " TZ";
		if (frame.getRotation().isfreex)
			result += " RX";
		if (frame.getRotation().isfreey)
			result += " RY";
		if (frame.getRotation().isfreez)
			result += " RZ";
		if (frame.isFreeScale())
			result += " SCL";
		result += '\n';
	}

	// points
	std::string pointtype, newtype;
	for (const auto &p : frame.getPoints())
	{
		newtype = p->extraInfos.has("lgcType") ? p->extraInfos.at("lgcType") : getPointType(p->position);
		if (_firstPointWritten && newtype != pointtype)
		{
			pointtype = newtype;
			result += pointtype + '\n';
		}
		else if (!_firstPointWritten)
			pointtype = newtype;
		result += write(*p) + '\n';
		_firstPointWritten = true;
	}

	// inner frames
	for (const auto &f : frame.getFrames())
		result += '\n' + write(*f) + '\n';

	// FRAME footer
	if (ff)
		result += "*ENDFRAME";

	_firstFrameWritten = ff;
	return result;
}

std::string LGCInputLexerIO::write(const ShareableParams &params)
{
	std::string result;

	if (params.extraInfos.has("coordinateSystem"))
		result += params.extraInfos.at("coordinateSystem") + '\n';
	if (params.extraInfos.has("CalculationOptions"))
		result += params.extraInfos.at("CalculationOptions") + '\n';
	result += "*PREC " + std::to_string(params.precision) + '\n';
	if (params.extraInfos.has("OutputOptions"))
		result += params.extraInfos.at("OutputOptions") + '\n';

	return result;
}

std::string LGCInputLexerIO::write(const ShareablePoint &point)
{
	std::string result;
	if (!point.headerComment.empty())
		result += "%" + point.headerComment + '\n';
	result += point.name + ' ' + write(point.position);

	std::string comments;
	if (point.extraInfos.has("DCUM"))
		comments += point.extraInfos.at("DCUM") + ' ';
	if (point.extraInfos.has("id"))
		comments += point.extraInfos.at("id") + ' ';
	comments += point.inlineComment;
	if (!comments.empty())
	{
		if (point.extraInfos.has("DCUM") && point.extraInfos.has("id"))
			result += " $";
		else
			result += " % ";
		result += comments;
	}

	return result;
}

std::string LGCInputLexerIO::write(const ShareablePosition &position)
{
	return std::to_string(position.x) + ' ' + std::to_string(position.y) + ' ' + std::to_string(position.z);
}

void LGCInputLexerIO::parse()
{
	_isPostEnd = false;
	_endPos = -1;
	if (!_lexerMode)
	{
		LexerParser::parse();
		return;
	}

	// We run twice to determine used points that are not declared
	_lexerMode = false;
	_firstRun = true;
	_secondRun = false;
	_measures.clearAll();
	_sigmaLines.clear();
	_missingPointObservations.clear();
	LexerParser::parse();
	// second run
	initLexer();
	_lexerMode = true;
	_isPostEnd = false;
	_endPos = -1;
	std::swap(_firstRun, _secondRun);
	LexerParser::parse();
	// ending
	_secondRun = false;
}

QString LGCInputLexerIO::calltip(int position, int indicatorType, const QString &indicator) const
{
	QString outCalltip = tr("%1\nline: %2");
	const LGCMeasures::MeasureMap *measuremap = nullptr;
	QString measurekey = indicator;

	if (contains(_measures.instruments, measurekey))
		measuremap = &_measures.instruments;
	else if (contains(_measures.points, measurekey))
	{
		if (indicatorType == TextIndicator::HOVERONLY)
			return _measures.points.at(measurekey).getObservationsSummary();
		else
			measuremap = &_measures.points;
	}
	else if (measurekey == "*ENDFRAME")
	{
		// find where ENDFRAME points to, get the frame name, and find its definition
		auto *editr = editor();
		int framePos = editr->SendScintilla(QsciScintillaBase::SCI_INDICATORVALUEAT, TextIndicator::INTERNAL, position);
		int frameLineIdx = editr->SendScintilla(QsciScintilla::SCI_LINEFROMPOSITION, framePos);
		const QString frameLineText = editr->text(frameLineIdx);
		QVector<QStringRef> frameTextSplit = frameLineText.splitRef(QRegExp("[\t ]+"));
		if (frameTextSplit.size() < 2) // In principle, it should be much higher than 2 (considering frame definition)
			return QString();

		const QString frameName = frameTextSplit[1].toString();
		QString target = find(_measures.frames, frameName);
		if (target.isEmpty())
			return QString();
		measuremap = &_measures.frames;
		measurekey = frameName;
	}
	else
	{
		QString target = find(_measures.targets, measurekey);
		if (target.isEmpty())
			return QString();
		measuremap = &_measures.targets;
		measurekey = target;
	}
	if (measuremap)
	{
		outCalltip = outCalltip.arg(measuremap->at(measurekey).getSummary(),
			QString::number(editor()->SendScintilla(QsciScintilla::SCI_LINEFROMPOSITION, measuremap->at(measurekey).position) + 1));
		if (measuremap == &_measures.points)
			outCalltip += "\n\n" + measuremap->at(measurekey).getObservationsSummary();
	}

	return outCalltip;
}

void LGCInputLexerIO::endLexer()
{
	LexerParser::endLexer();

	_curTmp.clear();
	_firstRun = false;
	_secondRun = false;
	_firstFrameWritten = false;
	_firstPointWritten = false;
}
