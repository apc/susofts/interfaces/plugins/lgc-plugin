#include "LGCInputText.hpp"

#include <QComboBox>
#include <QDateTimeEdit>
#include <QDialogButtonBox>
#include <QInputDialog>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include <QNetworkReply>
#include <QRegularExpression>
#include <QSslSocket>
#include <QVBoxLayout>

#include <Logger.hpp>
#include <ShareablePoints/ShareablePointsList.hpp>
#include <editors/text/TextEditor.hpp>
#include <editors/text/TextUtils.hpp>
#include <editors/text/StructureViewer.hpp>
#include <utils/NetworkManager.hpp>
#include <utils/SettingsManager.hpp>

#include "LGCPlugin.hpp"
#include "QtStreams.hpp"
#include "tabs/inputs/LGCInputLexerIO.hpp"
#include "tabs/inputs/MeasureViewer.hpp"
#include "trinity/LGCConfig.hpp"

namespace
{
#ifdef NDEBUG
constexpr char *newtext = R"""(*TITR
Fichier d'input créé le 17-JUL-2018
Opération n 14736, Alignement H4

*RS2K
*HIST
*PUNC   OUT1
*FAUT     .01     .10
*JSON

*INSTR
*POLAR AT402.392781 CCR1.5.1143L .1898 0 0 0
CCR1.5.1143L 3 3 .02 6 0 0 0 0 0 0

*CALA
EHN1.ST.H4-4.                                     754.53167     5263.83883    453.58094   $670.000    776121   coordonnées au 17-JUL-2018 15:48:57
EHN1.ST.H4-5.                                     763.34159     5271.95242    453.57390   $680.000    779740   coordonnées au 17-JUL-2018 15:48:57
EHN1.ST.H4-6.                                     764.56580     5283.09069    453.56984   $690.000    779741   coordonnées au 17-JUL-2018 15:48:57
EHN1.ST.H4-7.                                     766.52069     5289.92934    453.57414   $700.000    779742   coordonnées au 17-JUL-2018 15:48:57
*POIN
H4.XBPF.22716.E                                   759.11954     5300.00270    448.94777   $715.853    780399   coordonnées théoriques au 17-JUL-2018 15:48:57
H4.XBPF.22716.S                                   759.23973     5300.36554    448.87048   $716.243    780400   coordonnées théoriques au 17-JUL-2018 15:48:57

*TSTN   EHN1.ST.H4-6.   AT402.392781    IHFIX   IH  0.189800    TRGT CCR1.5.1143L    %station n268877
*V0   
*ANGL   
EHN1.ST.H4-4.                                125.800700   $1110206 -12-JUL-2018
EHN1.ST.H4-5.                                102.179500   $1110207 -12-JUL-2018
EHN1.ST.H4-7.                                312.938700   $1110208 -12-JUL-2018
H4.XBPF.22716.S                              276.172900   $1110215 -12-JUL-2018
H4.XBPF.22716.E                              275.378300   $1110216 -12-JUL-2018
*ZEND   
EHN1.ST.H4-4.                                100.523400 TH  0.000000   $1110209 -12-JUL-2018
EHN1.ST.H4-5.                                101.054200 TH  0.000000   $1110210 -12-JUL-2018
EHN1.ST.H4-7.                                101.660300 TH  0.000000   $1110211 -12-JUL-2018
H4.XBPF.22716.S                              116.813900 TH  0.000000   $1110217 -12-JUL-2018
H4.XBPF.22716.E                              116.835500 TH  0.000000   $1110218 -12-JUL-2018
*DIST   
EHN1.ST.H4-4.                                 21.710293 TH  0.000000   $1110212 -12-JUL-2018
EHN1.ST.H4-5.                                 11.207336 TH  0.000000   $1110213 -12-JUL-2018
EHN1.ST.H4-7.                                  7.115589 TH  0.000000   $1110214 -12-JUL-2018
H4.XBPF.22716.S                               18.729277 TH  0.000000   $1110219 -12-JUL-2018
H4.XBPF.22716.E                               18.409762 TH  0.000000   $1110220 -12-JUL-2018
*END

)""";
#else
constexpr char *newtext = R"""(*TITR
Fichier d'input créé le 21-AUG-2019
Opération n 17107, Alignements

% Datum option
*RS2K

% Calculation options
*LIBR
*SIMU 4

% Output options
*CHABA
*COVAR
*DEFA
*HIST
*NODUP
*PRES
*SOBS
*EREL %test
TPS.BILBENID.1. TPS.BILBENID.2. %test dfdfgdfgfdg
TPS.BILBENID.4. TPS.BILBENID.5.
 a a
*FAUT
*FAUT 1 1
*FMTP COL
*FMTP SEP "lol"
*JSON COVAR
*PREC 5
*PUNC
*PUNC E
*PUNC EE
*PUNC H
*PUNC Z
*PUNC HZ
*PUNC HN
*PUNC ZHN
*PUNC T
*PUNC OUT1
*PUNC OUT3

% Instruments
*INSTR
*CAMD CAMD CAMDEF 12.3
CAMDEF 0.4 0.5 0.2 43 2
CAMDEF1 0.4 0.5 0.2 43
CAMDEF2 0.4 0.5 0.2 43 2
*EDM EDM EDMDEF -15 4.3 6.5
EDMDEF 5 12 0 0 0 0 0 0
EDMDEF1 5 12 0 0 0 0 0 0
EDMDEF2 5 12 0 0 0 0 0 0
*LEVEL LEVEL LEVELDEF 1 31.2
LEVELDEF 0.3 -4 -5 3.1 0 0
LEVELDEF1 0.3 -4 -5 3.1 0 0
LEVELDEF2 0.3 -4 -5 3.1 0 0
*POLAR AT401.390769 CCR1.5.1 0 0 0 0
CCR1.5.1 3 3 .02 6 0 0 0 0 0 0
CCR2.5.2 3 3 .02 6 0 0 0 0 0 0
CCR3.5.3 3 3 .02 6 0 0 0 0 0 0
*SCALE SCALE 1.2 1.3 1.4 1.5 1.6
*SCALE SCALE1 1.2 1.3 1.4 1.5 1.6
*SCALE SCALE2 1.2 1.3 1.4 1.5 1.6
*SCALE SCALE3 1.2 1.3 1.4 1.5 1.6

% Points
*PDOR
TPS.STL.07081902. 4
*CALA
% Callé !
CALLÉ                                1898.159600     2074.158420    433.793010   $-999.000    792571   coordonnées au 07-AUG-2019 11:36:44
*CALA
LHC.MQ.24L8.E                                    4800.60321     6074.89790    329.03192
LHC.MQ.24L8.S                                    4800.18209     6072.08288    329.0337
LHC.MB.C24L8.E                                   4799.54724     6067.89932    329.03298
*POIN
LHC.MB.C24L8.S                                   4797.91877     6057.22215    329.03512
LHC.MB.B24L8.E                                   4797.17329     6052.42131    329.03576
LHC.MB.B24L8.S                                   4795.49054     6041.75318    329.04039
LHC.MB.A24L8.E                                   4794.71689     6036.95967    329.0376
*VXY
LHC.MB.A24L8.S                                   4792.98037     6026.29701    329.04296
*VXZ
LHC.MQ.23L8.E                                    4792.29507     6022.10132    329.04920
LHC.MQ.23L8.S                                    4791.83184     6019.29436    329.05074
*VYZ
LHC.MB.B23L8.S                                   4786.67425     5989.03963    329.06361
*VZ
TPS.STL.07081902.                                1898.159600     2074.158420    433.793010   $-999.000    792571   coordonnées au 07-AUG-2019 11:36:44
TPS.STL.08081901.                                1896.468940     2071.023040    433.804110   $-999.000    792572   coordonnées au 07-AUG-2019 11:36:44
TPS.STL.12081901.                                1896.475520     2070.740700    433.812790   $-999.000    792573   coordonnées au 07-AUG-2019 11:36:44
TPS.BILBENID.1.                                  1896.932980     2068.795610    434.591520   $1.000    786951   coordonnées au 07-AUG-2019 11:36:44
TPS.BILBENID.2.                                  1895.509660     2071.666270    434.635240   $2.000    786952   coordonnées au 07-AUG-2019 11:36:44
TPS.BILBENID.3.                                  1894.580780     2073.577460    434.664360   $3.000    786953   coordonnées au 07-AUG-2019 11:36:44
TPS.BILBENID.4.                                  1897.883950     2075.968570    434.692650   $4.000    786954   coordonnées au 07-AUG-2019 11:36:44
TPS.BILBENID.5.                                  1898.773550     2074.143750    434.665820   $5.000    786955   coordonnées au 07-AUG-2019 11:36:44
TPS.BILBENID.6.                                  1899.444830     2071.528910    434.606290   $6.000    786956   coordonnées au 07-AUG-2019 11:36:44
LBE.HLG.10.E                                     1899.223445     2069.216491    434.105130   $4.151    792238   coordonnées réelles au 07-AUG-2019 11:36:44
LBE.HLG.10.S                                     1898.238466     2070.807232    434.105280   $6.022    792239   coordonnées réelles au 07-AUG-2019 11:36:44
LBE.HLG.30.E                                     1897.046537     2073.113262    434.105630   $8.610    792240   coordonnées réelles au 07-AUG-2019 11:36:44
LBE.HLG.30.S                                     1896.541264     2073.929533    434.105630   $9.570    792241   coordonnées réelles au 07-AUG-2019 11:36:44
LBE.HLG.35.E                                     1895.483346     2075.638601    434.105640   $11.580    792242   coordonnées réelles au 07-AUG-2019 11:36:44
LBE.HLG.35.S                                     1894.978073     2076.454872    434.105660   $12.540    792243   coordonnées réelles au 07-AUG-2019 11:36:44

% Measurements
% CAMD
*CAM LBE.HLG.35.S CAMD TRGT CAMDEF ICSE 0.3
*UVD TRGT CAMDEF1
TPS.BILBENID.4.                                  1 2 3 10 TRGT CAMDEF2 DSE 0.1 XSE 0.2 YSE 0.3 TCSE 10
TPS.BILBENID.4.                                  1 2 3 10
*UVEC TRGT CAMDEF1
LBE.HLG.35.E 1 2 3 TRGT CAMDEF2 XSE 0.2 YSE 0.3 TCSE 10
LBE.HLG.35.E 1 2 3
*CAM LBE.HLG.35.S CAMD
*UVD
TPS.BILBENID.4.                                  1 2 3 10 TRGT CAMDEF2 DSE 0.1 XSE 0.2 YSE 0.3 TCSE 10
TPS.BILBENID.4.                                  1 2 3 10
*UVEC
LBE.HLG.35.E 1 2 3 TRGT CAMDEF2 XSE 0.2 YSE 0.3 TCSE 10
LBE.HLG.35.E 1 2 3
% EDM
*DSPT LBE.HLG.35.S EDM TRGT EDMDEF1 IH 10 IHSE 1 ICSE 2
LBE.HLG.35.E 0.1 TRGT EDMDEF2 OBSE 1 PPM 2 TH 3 THSE 4 TCSE 5
LBE.HLG.35.E 0.1
*DSPT LBE.HLG.35.S EDM
LBE.HLG.35.E 0.1 TRGT EDMDEF2 OBSE 1 PPM 2 TH 3 THSE 4 TCSE 5
LBE.HLG.35.E 0.1
% LEVEL
*DLEV LEVEL RefPt LBE.HLG.35.S
LBE.HLG.35.E 0.1 TRGT LEVELDEF1 OBSE 1 PPM 2 TH 3 THSE 4 DHOR 5 DSE 0.2
LBE.HLG.35.E 0.1
*DLEV LEVEL
LBE.HLG.35.E 0.1 TRGT LEVELDEF1 OBSE 1 PPM 2 TH 3 THSE 4 DHOR 5 DSE 0.2
LBE.HLG.35.E 0.1
% POLAR
*ORIE LBE.HLG.35.S AT401.390769 CST 120 ICSE 4 TRGT CCR1.5.1
LBE.HLG.35.E 0.1 OBSE 1 TCSE 2 TRGT CCR1.5.1
LBE.HLG.35.E 0.1
*TSTN LBE.HLG.35.S AT401.390769 ICSE 0.1 IHFIX IH 0.2 IHSE 0.3 TRGT CCR1.5.1
*V0 ACST 120 TRGT CCR2.5.2
*ANGL TRGT CCR3.5.3
LBE.HLG.35.E 0.1 OBSE 1 TCSE 2 TRGT CCR1.5.1
LBE.HLG.35.E 0.1
*DHOR TRGT CCR3.5.3
LBE.HLG.35.E 0.1 OBSE 1 TCSE 2 TRGT CCR1.5.1 PPM 40
LBE.HLG.35.E 0.1
*DIST TRGT CCR3.5.3
LBE.HLG.35.E 0.1 OBSE 1 TCSE 2 TRGT CCR1.5.1 PPM 40 TH 2 THSE 6
LBE.HLG.35.E 0.1
*ECDIR 1 2 SCALE1
LBE.HLG.35.E 0.1 OBSE 1 PPM 40 ICSE 12 SCALE SCALE2
LBE.HLG.35.E 0.1
*ECTH 1 SCALE1
LBE.HLG.35.E 0.1 OBSE 1 PPM 40 ICSE 12 SCALE SCALE2
LBE.HLG.35.E 0.1
*PLR3D TRGT CCR3.5.3
LBE.HLG.35.E 0.1 0.2 0.3  ASE 1 DSE 2 ZSE 3 TCSE 2 TRGT CCR1.5.1 PPM 40 TH 2 THSE 6
LBE.HLG.35.E 0.1 0.2 0.3
*ZEND TRGT CCR3.5.3
LBE.HLG.35.E 0.1 TCSE 2 TRGT CCR1.5.1 TH 2 THSE 6 OBSE 12
LBE.HLG.35.E 0.1
*TSTN LBE.HLG.35.S AT401.390769 ROT3D ICSE 0.1 TRGT CCR1.5.1
*V0 ACST 120 TRGT CCR2.5.2
*ANGL TRGT CCR3.5.3
LBE.HLG.35.E 0.1 OBSE 1 TCSE 2 TRGT CCR1.5.1
LBE.HLG.35.E 0.1
*DHOR
LBE.HLG.35.E 0.1 OBSE 1 TCSE 2 TRGT CCR1.5.1 PPM 40
LBE.HLG.35.E 0.1
*DIST
LBE.HLG.35.E 0.1 OBSE 1 TCSE 2 TRGT CCR1.5.1 PPM 40 TH 2 THSE 6
LBE.HLG.35.E 0.1
*PLR3D
LBE.HLG.35.E 0.1 0.2 0.3  ASE 1 DSE 2 ZSE 3 TCSE 2 TRGT CCR1.5.1 PPM 40 TH 2 THSE 6
LBE.HLG.35.E 0.1 0.2 0.3
*ZEND
LBE.HLG.35.E 0.1 TCSE 2 TRGT CCR1.5.1 TH 2 THSE 6 OBSE 12
LBE.HLG.35.E 0.1
*TSTN LBE.HLG.35.S AT401.390769
*V0
*ANGL
LBE.HLG.35.E 0.1 OBSE 1 TCSE 2 TRGT CCR1.5.1
LBE.HLG.35.E 0.1
*ORIE LBE.HLG.35.S AT401.390769
LBE.HLG.35.E 0.1 OBSE 1 TCSE 2 TRGT CCR1.5.1
LBE.HLG.35.E 0.1
% SCALE
*ECHO SCALE
LBE.HLG.35.E 0.1 SCALE SCALE1 OBSE 1 PPM 2 ICSE 3
LBE.HLG.35.E 0.1
*ECVE SCALE PtLine LBE.HLG.35.S
LBE.HLG.35.E 0.1 SCALE SCALE1 OBSE 1 PPM 2 ICSE 3
LBE.HLG.35.E 0.1
*ECVE SCALE
LBE.HLG.35.E 0.1 SCALE SCALE1 OBSE 1 PPM 2 ICSE 3
LBE.HLG.35.E 0.1
*ECSP LHC.MQ.23L8.E LHC.MQ.23L8.S SCALE
LBE.HLG.35.E 0.1 SCALE SCALE1 OBSE 1 PPM 2 ICSE 3
LBE.HLG.35.E 0.1
% NO TYPE
*DVER 0.1
LHC.MQ.23L8.E LHC.MQ.23L8.S 0.1 DCOR 1 OBSE 2
LHC.MQ.23L8.E LHC.MQ.23L8.S 0.1
*DVER
LHC.MQ.23L8.E LHC.MQ.23L8.S 0.1 DCOR 1 OBSE 2
LHC.MQ.23L8.E LHC.MQ.23L8.S 0.1
*OBSXYZ
LHC.MQ.23L8.E 0.1 0.2 0.3 1 2 3
LHC.MQ.23L8.E 0.1 0.2 0.3 1 2 3
*RADI 0.1
LHC.MQ.23L8.E 0.1 OBSE 0.2
LHC.MQ.23L8.E 0.1

*END

)""";
#endif

class DelayedStylingTimer : public QTimer
{
	Q_OBJECT

public:
	DelayedStylingTimer(TextEditorWidget *parent) : QTimer(parent)
	{
		setSingleShot(true);
		stop(); // Don't run on initialization

		// Start/Refresh on text changed
		connect(parent->editor(), &QsciScintilla::textChanged, [this]() { start(_updateTime); });
	}

	void setInterval(double newUpdateTime) { _updateTime = int(newUpdateTime * 1000); /** s to ms*/ }
	int getInterval() { return _updateTime; }

private:
	int _updateTime = 0; // ms
};

class GeodeApiDataPicker : public QDialog
{
	Q_OBJECT

public:
	GeodeApiDataPicker(bool needsDate, bool needsPointType, QWidget *parent) : QDialog(parent)
	{
		QVBoxLayout *dlgLayout = new QVBoxLayout(this);
		if (needsDate)
			addDate();
		if (needsPointType)
			addPointType();

		QDialogButtonBox *bb = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);
		dlgLayout->addWidget(bb);
		resize(250, 200);

		connect(bb, &QDialogButtonBox::rejected, this, &QDialog::reject);
		connect(bb, &QDialogButtonBox::accepted, this, &QDialog::accept);
	}

	QString getDate()
	{
		if (dateEdit)
			return dateEdit->dateTime().toString("yyyy-MM-dd hh:mm:ss");
		return "";
	}
	QString getPointType()
	{
		if (pointCombo)
			return pointCombo->currentText() == "Theoretical position" ? "T" : "R";
		return "";
	}

protected:
	void addDate()
	{
		dateEdit = new QDateTimeEdit(this);
		dateEdit->setDisplayFormat("yyyy/MM/dd hh:mm:ss");
		dateEdit->setDateTime(QDateTime().currentDateTime());
		dateEdit->setCalendarPopup(true);
		layout()->addWidget(dateEdit);
	}
	void addPointType()
	{
		pointCombo = new QComboBox(this);
		pointCombo->addItem("Real position");
		pointCombo->addItem("Theoretical position");
		layout()->addWidget(pointCombo);
	}

private:
	QDateTimeEdit *dateEdit = nullptr;
	QComboBox *pointCombo = nullptr;
};
} // namespace
#include "LGCInputText.moc"

class LGCInputText::_LGCInputText_pimpl
{
public:
	bool lgc1Mode = false;
	DelayedStylingTimer *stylingTimer = nullptr;
	const QStringList pointCategories{"*POIN", "*CALA", "*VXZ", "*VXY", "*VYZ", "*VZ"};
	const QUrl updatePointsEndpoint = QStringLiteral("https://apex-sso.cern.ch/pls/htmldb_accdb/survey/surveypad/coords/");
	const QUrl updateBearingsEndpoint = QStringLiteral("https://apex-sso.cern.ch/pls/htmldb_accdb/survey/surveypad/bearing_for_radi/");
	const QString prelogUpdatePoints = "[Update Points] ";
	const QString prelogUpdateBearings = "[Update Bearings] ";
};

enum ToggleCommentMode : int
{
	onlyComment = 0,
	toggle,
	onlyUncomment
};

LGCInputText::LGCInputText(SPluginInterface *owner, QWidget *parent) : TextEditorWidget(owner, parent), _pimpl(std::make_unique<_LGCInputText_pimpl>())
{
	enablefileWatcher(true);
	installEventFilterAll(this, this);

	setupEditor();

	createActions();
	updateUi(owner->name());
}

LGCInputText ::~LGCInputText() = default;

ShareablePointsList LGCInputText::getContent() const
{
	return LGCInputLexerIO().read(toByteArray(editor()->text(), editor()->isUtf8()).constData());
}

const LGCMeasures &LGCInputText::lgcMeasures()
{
	static const LGCMeasures _m;
	auto lex = qobject_cast<LGCInputLexerIO *>(editor()->lexer());
	return lex ? lex->lgcMeasures() : _m;
}

void LGCInputText::setContent(const ShareablePointsList &spl)
{
	TextEditorWidget::setContent(spl);
	editor()->setText(fromByteArray(LGCInputLexerIO().write(spl).c_str(), editor()->isUtf8()));
}

void LGCInputText::updateUi(const QString &pluginName)
{
	TextEditorWidget::updateUi(pluginName);

	if (pluginName != LGCPlugin::_name())
		return;
	auto config = SettingsManager::settings().settings<LGCConfigObject>(pluginName);

	if (_pimpl->lgc1Mode || !config.color)
	{
		editor()->setLexer(nullptr);
		editor()->clearFolds();
	}
	else if (!_pimpl->lgc1Mode && config.color && !editor()->lexer())
		editor()->setLexer(new LGCInputLexerIO(this));
	if (_pimpl->stylingTimer)
		_pimpl->stylingTimer->setInterval(config.inputUpdateTime);
}

bool LGCInputText::_open(const QString &path)
{
	bool r = TextEditorWidget::_open(path);
	_pimpl->lgc1Mode = !editor()->text().contains(QRegularExpression(R"""(\s\*INSTR\s)"""));
	if (_pimpl->lgc1Mode)
		logWarning() << "LGC1 file detected. All features disabled.";
	updateUi(owner()->name());
	return r;
}

void LGCInputText::_newEmpty()
{
	TextEditorWidget::_newEmpty();
	_pimpl->lgc1Mode = false;
	editor()->setText(newtext);
}

QByteArray LGCInputText::fromMime(const QMimeData *mimedata)
{
	LGCInputLexerIO io;
	io.utf8(editor()->isUtf8());
	return contentFromMime(mimedata, io);
}

QMimeData *LGCInputText::toMime(const QByteArray &text, QMimeData *mimedata)
{
	LGCInputLexerIO io;
	io.utf8(editor()->isUtf8());
	return contentToMime(text, mimedata, io);
}

void LGCInputText::createActions()
{
	// toggle comment
	auto *actToggleComment = new QAction(tr("Toggle line comment"), this);
	actToggleComment->setShortcut(tr("Ctrl+Shift+C"));
	actToggleComment->setToolTip(tr("Toggle current line (or selected lines) into comments"));
	actToggleComment->setStatusTip(tr("Toggle current line (or selected lines) into comments"));
	connect(actToggleComment, &QAction::triggered, [&](bool) { toggleComment(ToggleCommentMode::toggle); });
	addAction(Menus::edit, actToggleComment);
	// comment and uncomment
	auto *actComment = new QAction(tr("Comment line"), this);
	actComment->setShortcut(tr("Ctrl+`"));
	actComment->setToolTip(tr("Comment current line (or selected lines)"));
	actComment->setStatusTip(tr("Comment current line (or selected lines)"));
	connect(actComment, &QAction::triggered, [&](bool) { toggleComment(ToggleCommentMode::onlyComment); });
	addAction(Menus::edit, actComment);
	auto *actUncomment = new QAction(tr("Uncomment line"), this);
	actUncomment->setShortcut(tr("Ctrl+Shift+`"));
	actUncomment->setToolTip(tr("Uncomment current line (or selected lines)"));
	actUncomment->setStatusTip(tr("Uncomment current line (or selected lines)"));
	connect(actUncomment, &QAction::triggered, [&](bool) { toggleComment(ToggleCommentMode::onlyUncomment); });
	addAction(Menus::edit, actUncomment);
	// toggle Sigmas
	auto addSigmaToggleAction = [this](const QString &title, const QString &keyword, const QString &shortcut, const QString &description) -> QAction * {
		auto *action = new QAction(title, this);
		action->setShortcut(shortcut);
		action->setToolTip(description);
		action->setStatusTip(description);
		connect(action, &QAction::triggered, [keyword, this]() { toggleSigma(keyword); });
		addAction(Menus::edit, action);
		return action;
	};
	QAction *actASE = addSigmaToggleAction(tr("Toggle ASE"), "ASE", tr("Ctrl+Shift+A"), tr("Toggle current line (or selected lines) ASE measurement parameter"));
	QAction *actZSE = addSigmaToggleAction(tr("Toggle ZSE"), "ZSE", tr("Ctrl+Shift+Z"), tr("Toggle current line (or selected lines) ZSE measurement parameter"));
	QAction *actDSE = addSigmaToggleAction(tr("Toggle DSE"), "DSE", tr("Ctrl+Shift+D"), tr("Toggle current line (or selected lines) DSE measurement parameter"));
	QAction *actACST = addSigmaToggleAction(tr("Toggle ACST"), "ACST", tr("Ctrl+Shift+K"), tr("Toggle current line (or selected lines) ACST measurement parameter"));
	QAction *actOBSE_SIGMA_PLR3D = addSigmaToggleAction(
		tr("Toggle OBSE/PLR3D"), "OBSE/ASE|ZSE|DSE", tr("Ctrl+Shift+B"), tr("Toggle current line (or selected lines) OBSE/PLR3D measurement parameter"));
	// Transfer point to the other type
	auto *actTransferPoint = new QAction(tr("Change point declaration type"), this);
	actTransferPoint->setToolTip(tr("Transfer point to some other point declaration"));
	actTransferPoint->setStatusTip(tr("Transfer point to some other point declaration"));
	connect(actTransferPoint, &QAction::triggered, this, &LGCInputText::transferPointDeclaration);
	addAction(Menus::edit, actTransferPoint);
	// Update points with Geode
	auto *actUpdatePoints = new QAction(tr("Update points"), this);
	actUpdatePoints->setToolTip(tr("Update points with Geode"));
	actUpdatePoints->setStatusTip(tr("Update points with Geode"));
	connect(actUpdatePoints, &QAction::triggered, this, &LGCInputText::updatePoints);
	addAction(Menus::edit, actUpdatePoints);
	// Update bearings with Geode
	auto *actUpdateBearings = new QAction(tr("Update bearings"), this);
	actUpdateBearings->setToolTip(tr("Update bearings with Geode"));
	actUpdateBearings->setStatusTip(tr("Update bearings with Geode"));
	connect(actUpdateBearings, &QAction::triggered, this, &LGCInputText::updateBearings);
	addAction(Menus::edit, actUpdateBearings);
	// TSTN conversion PLR3D <=> ANGL/ZEND/DIST
	auto *actTstnConversion = new QAction(tr("(Prototype) TSTN Conversion"), this);
	actTstnConversion->setToolTip(tr("Convert PLR3D to ANGL/ZEND/DIST or back from it"));
	actTstnConversion->setStatusTip(tr("Convert PLR3D to ANGL/ZEND/DIST or back from it"));
	connect(actTstnConversion, &QAction::triggered, this, &LGCInputText::convertTSTN);
	addAction(Menus::edit, actTstnConversion);
	// Select everything up to *END
	auto *actSelectToEnd = new QAction(QIcon(":/actions/select-to-end"), tr("Select to *END"), this);
	actSelectToEnd->setToolTip(tr("Create a selection from beginning to *END"));
	actSelectToEnd->setStatusTip(tr("Create a selection from beginning to *END"));
	connect(actSelectToEnd, &QAction::triggered, this, &LGCInputText::selectToEnd);
	addAction(Menus::edit, actSelectToEnd);
	toolbar()->addAction(actSelectToEnd);

	additionalContextualActions({actComment, actOBSE_SIGMA_PLR3D, actASE, actZSE, actDSE, actACST, actTransferPoint, actUpdatePoints, actUpdateBearings, actTstnConversion});
}

void LGCInputText::toggleComment(ToggleCommentMode mode)
{
	Lines l = initEditingLines(editor());

	// toggle comments
	QString linetxt;
	QStringRef lineref;
	editor()->SendScintilla(QsciScintilla::SCI_BEGINUNDOACTION);
	for (int curline = l.endLine; curline >= l.startLine && curline >= 0; curline--)
	{
		linetxt = editor()->text(curline);
		lineref = &linetxt;
		lineref = lineref.trimmed();
		if (lineref.isEmpty())
			continue;
		// toggle and uncomment
		if (mode >= ToggleCommentMode::toggle && (lineref[0] == '%' || lineref[0] == '$' || lineref[0] == '#'))
		{
			int length = lineref[1] == ' ' ? 2 : 1;
			editor()->SendScintilla(QsciScintilla::SCI_DELETERANGE, editor()->positionFromLineIndex(curline, lineref.position()), length);
			if (curline == l.startLine && l.startPos > lineref.position())
				l.startPos = l.startPos < length ? 0 : l.startPos - length;
			if (curline == l.endLine && l.endPos > lineref.position())
				l.endPos = l.endPos < length ? 0 : l.endPos - length;
		}
		// toggle and comment
		else if (mode <= ToggleCommentMode::toggle) // comment
		{
			editor()->insertAt("% ", curline, lineref.position());
			if (curline == l.startLine && l.startPos > lineref.position())
				l.startPos += 2;
			if (curline == l.endLine && l.endPos > lineref.position())
				l.endPos += 2;
		}
	}
	editor()->SendScintilla(QsciScintilla::SCI_ENDUNDOACTION);

	restoreSelection(editor(), l);
}

void LGCInputText::toggleSigma(const QString &keyword)
{
	Lines l = initEditingLines(editor());

	// toggle comments
	QString linetxt;
	QStringRef lineref;
	editor()->SendScintilla(QsciScintilla::SCI_BEGINUNDOACTION);

	for (int curline = l.endLine; curline >= l.startLine && curline >= 0; curline--)
	{
		QString workingKeyword = "";
		QStringListIterator iter = keyword.split('/');
		while (workingKeyword.isEmpty() && iter.hasNext())
		{
			if (canToggleSigma(curline, iter.next().split('|')[0])) // keyword is never empty
				workingKeyword = iter.peekPrevious();
		}
		if (workingKeyword.isEmpty())
			continue;

		linetxt = editor()->text(curline);
		lineref = &linetxt;
		lineref = lineref.trimmed();
		if (lineref.isEmpty())
			continue;
		QRegularExpression rgxSigma("([[:blank:]](" + workingKeyword + ")[[:blank:]]+[[:digit:]]+([.][[:digit:]]+)?)");
		QRegularExpressionMatch matchSigma = rgxSigma.match(linetxt);
		if (matchSigma.hasMatch() && !matchSigma.capturedRef().isEmpty()) // remove Sigma precision
		{
			while (matchSigma.hasMatch())
			{
				if (matchSigma.capturedRef().isEmpty())
					continue;
				editor()->SendScintilla(QsciScintilla::SCI_DELETERANGE, editor()->positionFromLineIndex(curline, matchSigma.capturedStart()), matchSigma.capturedLength());
				if (curline == l.startLine && l.startPos > matchSigma.capturedStart())
					l.startPos = l.startPos < matchSigma.capturedStart() + matchSigma.capturedLength() ? matchSigma.capturedStart() : l.startPos - matchSigma.capturedLength();
				if (curline == l.endLine && l.endPos > matchSigma.capturedStart())
					l.endPos = l.endPos < matchSigma.capturedStart() + matchSigma.capturedLength() ? matchSigma.capturedStart() : l.endPos - matchSigma.capturedLength();

				matchSigma = rgxSigma.match(editor()->text(curline)); // match on the updated line
			}
		}
		else // insert keyword
		{
			const auto &lgcsettings = SettingsManager::settings().settings<LGCConfigObject>(LGCPlugin::_name());
			double value = lgcsettings.inputToggleSigmaPrecision;
			if (keyword.contains("ACST"))
				value = lgcsettings.inputToggleConstantAngle;
			QString toadd = workingKeyword.split('|').join(QString(" %1 ").arg(QString::number(value))) + QString(" %1").arg(QString::number(value));
			int commentpos = linetxt.indexOf(QRegularExpression("[%$#]"));
			if (commentpos < 0)
			{
				commentpos = lineref.position() + lineref.size();
				toadd.prepend(' ');
			}
			else
			{
				toadd.append(' ');
				if (!linetxt[commentpos - 1].isSpace())
					toadd.prepend(' ');
			}
			editor()->insertAt(toadd, curline, commentpos);
			if (curline == l.startLine && l.startPos > commentpos)
				l.startPos += toadd.size();
			if (curline == l.endLine && l.endPos > commentpos)
				l.endPos += toadd.size();
		}
	}
	editor()->SendScintilla(QsciScintilla::SCI_ENDUNDOACTION);

	restoreSelection(editor(), l);
}

bool LGCInputText::canToggleSigma(int linenbr, const QString &keyword)
{
	auto *lexer = qobject_cast<LGCInputLexerIO *>(editor()->lexer());
	if (!lexer)
		return false;
	auto &sigmaLines = lexer->getSigmaLines();
	return (sigmaLines.find(linenbr) != std::cend(sigmaLines)) && (sigmaLines.find(linenbr)->second.count(keyword));
}

void LGCInputText::transferPointDeclaration()
{
	// Pick the category
	bool isOk = false;
	QString pointCategory = QInputDialog::getItem(this, "Select point category", "Category:", _pimpl->pointCategories, 0, false, &isOk);
	if (!isOk)
		return;

	// CHECK
	// Check if there are any points to move
	Lines l = initEditingLines(editor());
	const auto &points = lgcMeasures().points;
	QVector<QPair<int, int>> linesToRemove; // <position, length>
	QStringList pointsToMove;
	QString linetxt;
	for (int curline = l.endLine; curline >= l.startLine && curline >= 0; curline--)
	{
		linetxt = editor()->text(curline);
		QStringList listSplit = linetxt.simplified().split(' ');
		if (listSplit.isEmpty() || !isPointDefinition(listSplit.first(), curline) || points.at(listSplit.first()).type == pointCategory)
			continue; // not a point definition

		pointsToMove.push_back(linetxt);
		linesToRemove.push_back({editor()->positionFromLineIndex(curline, 0), linetxt.size()});
	}
	if (pointsToMove.isEmpty())
		return;
	// Find the position to insert after - it is the first position after the keyword (no support for FRAME yet)
	bool needsCategory = false;
	int insertPos = -1;
	// Find points from the same category
	std::vector<std::pair<int, LGCMeasures::Value>> categoryPoints;
	std::copy_if(points.begin(), points.end(), std::back_inserter(categoryPoints), [&pointCategory](auto &it) { return it.second.type == pointCategory; });
	if (categoryPoints.empty())
	{
		insertPos = std::max_element(std::begin(points), std::end(points), [](auto const &p1, auto const &p2) { return p1.second.position < p2.second.position; })->second.position;
		needsCategory = true;
	}
	else
	{
		insertPos = std::min_element(std::begin(categoryPoints), std::end(categoryPoints), [](auto const &p1, auto const &p2) {
			return p1.second.position < p2.second.position;
		})->second.position;
	}
	// Position + if new category + if shift due to removals of some lines above
	int insertLine = editor()->SendScintilla(QsciScintilla::SCI_LINEFROMPOSITION, insertPos) + 2 * (int)needsCategory
		- std::count_if(linesToRemove.begin(), linesToRemove.end(), [insertPos](auto posLen) { return posLen.first < insertPos; });

	// MODIFICATIONS
	editor()->SendScintilla(QsciScintilla::SCI_BEGINUNDOACTION);
	{
		// Remove the lines
		for (auto &[position, length] : linesToRemove)
			editor()->SendScintilla(QsciScintilla::SCI_DELETERANGE, position, length);
		// Insert
		// point category if needed
		if (needsCategory)
			editor()->insertAt(pointCategory + '\n', insertLine - 1, 0);
		// points
		for (const QString &pointToMove : pointsToMove)
			editor()->insertAt(pointToMove, insertLine, 0);

		// Remove point categories if they are empty after deletions and insertions
		QString current = "", next = "";
		for (int i = l.startLine; i < l.endLine + 1; i++)
		{
			if (i < 0 || i + 1 >= editor()->lines()) // should not happen
				break;

			current = editor()->text(i).trimmed();
			if (!_pimpl->pointCategories.contains(current)) // is a point category
				continue;

			next = editor()->text(i + 1).trimmed();
			QStringList listSplit = next.simplified().split(' ');
			if (!listSplit.isEmpty() && (!points.count(listSplit.first()) && !listSplit.first().startsWith('%'))) // if no point or comment after point keyword
			{
				editor()->SendScintilla(QsciScintilla::SCI_DELETERANGE, editor()->positionFromLineIndex(i, 0), current.size());
				i--;
			}
		}
	}
	editor()->SendScintilla(QsciScintilla::SCI_ENDUNDOACTION);
}

void LGCInputText::setupEditor()
{
	// editor
	editor()->setMimeTypesSupport(
		{LGCInputLexerIO().getMIMEType()}, [this](const QMimeData *mimedata) -> QByteArray { return fromMime(mimedata); },
		[this](const QByteArray &text, QMimeData *mimedata) -> QMimeData * { return toMime(text, mimedata); });

	// timer
	_pimpl->stylingTimer = new DelayedStylingTimer(this);
	connect(_pimpl->stylingTimer, &QTimer::timeout, this, &LGCInputText::updateContent);
	// force annotations refresh for delayed timer
	connect(_pimpl->stylingTimer, &QTimer::timeout, [this]() {
		if (!editor() || _pimpl->stylingTimer->getInterval() == 0)
			return;
		int line, index;
		editor()->getCursorPosition(&line, &index);
		emit editor()->cursorPositionChanged(line, index);
	});
	// Help dock
	// Measure Viewer
	QTabWidget *widgetHelp = qobject_cast<QTabWidget *>(helpWidget());
	if (widgetHelp)
	{
		std::function<const LGCMeasures &(void)> f = std::bind(&LGCInputText::lgcMeasures, this);
		widgetHelp->addTab(new MeasureViewer(editor(), f, this), tr("Measure Viewer"));
		// Set default tab to StructureViewer
 		widgetHelp->setCurrentIndex(2);
		auto *structureViewerWidget = qobject_cast<StructureViewer *>(widgetHelp->currentWidget());
		structureViewerWidget->setStartCollapsed(true);
	}
}

void LGCInputText::updatePoints()
{
	// Get the date and type
	GeodeApiDataPicker dlg(true, true, this);
	dlg.exec();
	if (dlg.result() != QDialog::Accepted)
		return;
	QString date = dlg.getDate();
	QString point_position_type = dlg.getPointType();

	// Get the points
	QList<QPair<int, int>> linesToUpdate; // <line, size>
	int id_section_number = 5; // Section containing spat ID
	QJsonArray outgoingData;
	try
	{
		Lines l = initEditingLines(editor());
		for (int curline = l.startLine; curline <= l.endLine && curline <= l.endLine; curline++)
		{
			QString linetxt = editor()->text(curline);
			QStringList listSplit = linetxt.simplified().split(' ');
			if (listSplit.isEmpty() || !isPointDefinition(listSplit.first(), curline))
				continue; // not a point definition

			QString name = listSplit.first();
			QString id;
			if (listSplit.count() > id_section_number)
			{
				id = listSplit[id_section_number];
				bool ok;
				id.toInt(&ok);
				if (!ok) // check for numerical value
					id = "";
			}

			outgoingData.append(QJsonObject({{"id", id}, {"name", name}}));
			linesToUpdate.append({curline, linetxt.size()});
		}
	}
	catch (const std::exception &e)
	{
		logWarning() << _pimpl->prelogUpdatePoints << "Data serialization error encountered: " << e.what();
		return;
	}
	if (linesToUpdate.isEmpty())
	{
		logInfo() << _pimpl->prelogUpdatePoints << "No lines to update";
		return;
	}

	// POST
	auto reply = sendRequest({{"coord_type", point_position_type}, {"date", date}, {"points", outgoingData}}, _pimpl->updatePointsEndpoint, _pimpl->prelogUpdatePoints);
	// GET
	connect(reply, &QNetworkReply::finished, this, [this, reply, linesToUpdate]() {
		responseUpdate(reply, linesToUpdate, [](const QJsonObject &object, TextEditor *editor, int line) {
			auto jvalueDouble2String = [](const QJsonValue &value) -> QString { return QString::number(value.toDouble(), 'f', 6); };
			editor->insertAt(QString("%1%2     %3     %4   %5\n")
								 .arg(object["name"].toString().leftJustified(44, ' '), jvalueDouble2String(object["x"]), jvalueDouble2String(object["y"]),
									 jvalueDouble2String(object["h"]), object["comm"].toString()),
				line, 0);
		});
		reply->deleteLater();
	});
	// INFORM
	// Popup that closes automatically on request finish (success or not)
	if (reply->isFinished())
		return;
	QMessageBox *pendingDialog = new QMessageBox(
		QMessageBox::Information, QString("Geode data communication"), QString("Please wait until connection is finished or until it is timed out."));
	connect(reply, &QNetworkReply::finished, this, [this, pendingDialog]() {
		pendingDialog->close();
		pendingDialog->deleteLater();
	});
	pendingDialog->show();
}

void LGCInputText::updateBearings()
{
	// Get the date and type
	GeodeApiDataPicker dlg(true, false, this);
	dlg.exec();
	if (dlg.result() != QDialog::Accepted)
		return;
	QString date = dlg.getDate();

	// Get the points
	QList<QPair<int, int>> linesToUpdate; // <line, size>
	QJsonArray outgoingData;
	try
	{
		// Get RADI measures
		auto &measures = lgcMeasures().measures; // filter all the measures to the ones only including RADI and if the name marches == ok
		QSet<QPair<QString, int>> radiMeasures;
		for (const auto &[key, value] : measures)
		{
			if (value.name.startsWith("RADI/"))
				radiMeasures.insert({value.name.mid(5), value.position});
		}
		Lines l = initEditingLines(editor());
		for (int curline = l.startLine; curline <= l.endLine && curline <= l.endLine; curline++)
		{
			QString linetxt = editor()->text(curline);
			QStringList listSplit = linetxt.simplified().split(' ');

			// The RADI name has to exist and the RADI is not a point definition
			if (listSplit.isEmpty())
				continue;
			const QString &name = listSplit.first();
			int position = editor()->positionFromLineIndex(curline, 0);
			if (!radiMeasures.contains({name, position}))
				continue;
			if (isPointDefinition(name, curline) || position != editor()->positionFromLineIndex(curline, 0))
				continue;

			QString id; // Spat ID parsing not yet implemented since it is not used commonly with RADI
			outgoingData.append(QJsonObject({{"id", id}, {"name", name}}));
			linesToUpdate.append({curline, linetxt.size()});
		}
	}
	catch (const std::exception &e)
	{
		logWarning() << _pimpl->prelogUpdateBearings << "Data serialization error encountered: " << e.what();
		return;
	}
	if (linesToUpdate.isEmpty())
	{
		logInfo() << _pimpl->prelogUpdateBearings << "No lines to update";
		return;
	}

	// POST
	auto reply = sendRequest({{"date", date}, {"points", outgoingData}}, _pimpl->updateBearingsEndpoint, _pimpl->prelogUpdateBearings);
	// GET
	connect(reply, &QNetworkReply::finished, this, [this, reply, linesToUpdate]() {
		responseUpdate(reply, linesToUpdate, [](const QJsonObject &object, TextEditor *editor, int line) {
			auto jvalueDouble2String = [](const QJsonValue &value) -> QString { return QString::number(value.toDouble(), 'f', 6); };
			editor->insertAt(QString("%1%2     %3     %4\n")
								 .arg(object["name"].toString().leftJustified(44, ' '), jvalueDouble2String(object["bearing"]),
									 object["sigma"] == '0' ? "" : "ACST    " + jvalueDouble2String(object["sigma"]), object["comm"].toString()),
				line, 0);
		});
		reply->deleteLater();
	});
	// INFORM
	// Popup that closes automatically on request finish (success or not)
	if (reply->isFinished())
		return;
	QMessageBox *pendingDialog = new QMessageBox(
		QMessageBox::Information, QString("Geode data communication"), QString("Please wait until connection is finished or until it is timed out."));
	connect(reply, &QNetworkReply::finished, this, [this, pendingDialog]() {
		pendingDialog->close();
		pendingDialog->deleteLater();
	});
	pendingDialog->show();
}

void LGCInputText::responseUpdate(QNetworkReply *reply, const QList<QPair<int, int>> linesToUpdate, std::function<void(const QJsonObject &, TextEditor *, int)> addLine)
{
	if (!checkResponse(reply, _pimpl->prelogUpdatePoints))
		return;

	// The replace workaround comes from the https://its.cern.ch/jira/browse/SUS-2344
	// The issue is that GEODE cannot send a long JSON and it splits it into chunks. This chunks should be split on JSON elements,
	// but AFAIK, GEODE team has no control over it and the framework splits it randomly, even mid-string which generated invalid JSON.
	// Hence, we remove any newlines as the object should be a one liner in the first place.
	QByteArray replyData = reply->readAll().replace('\n', "");
	QJsonArray points = (QJsonDocument::fromJson(replyData).object()["points"]).toArray();
	if (points.isEmpty())
	{
		logWarning() << _pimpl->prelogUpdatePoints << "No point data received from Geode";
		return;
	}
	if (linesToUpdate.size() != points.size())
	{
		logWarning() << _pimpl->prelogUpdatePoints << "Points to update do not match number of received points from Geode";
		return;
	}

	// REPLACE lines
	QList<QPair<int, int>>::const_iterator iterLinesToUpdate = linesToUpdate.cbegin();
	editor()->SendScintilla(QsciScintilla::SCI_BEGINUNDOACTION);
	int updatedPoints = 0;
	for (const QJsonValue &point : points)
	{
		QJsonObject pointObject = point.toObject();

		if (pointObject["comm"].toString().startsWith("ERROR"))
			logWarning() << _pimpl->prelogUpdatePoints << "Geode could not find requested data:" << pointObject["comm"].toString();
		else
		{
			auto [line, size] = *iterLinesToUpdate;
			editor()->SendScintilla(QsciScintilla::SCI_DELETERANGE, editor()->positionFromLineIndex(line, 0), size);
			addLine(pointObject, editor(), line);
			updatedPoints++;
		}

		iterLinesToUpdate++;
	}
	editor()->SendScintilla(QsciScintilla::SCI_ENDUNDOACTION);
	logInfo() << _pimpl->prelogUpdatePoints << "Number of points updated" << updatedPoints;
}

bool LGCInputText::isPointDefinition(const QString &name, const int line)
{
	auto const &points = lgcMeasures().points;

	if (points.count(name) == 0 || (points.count(name) && points.at(name).position != editor()->positionFromLineIndex(line, 0)))
		return false;
	return true;
}

QNetworkReply *LGCInputText::sendRequest(const QJsonObject &outgoingData, const QUrl &url, const QString &preLog)
{
	// DATA
	QByteArray requestData = QJsonDocument(outgoingData).toJson();

	// REQUEST
	QNetworkRequest request(url);
	request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

	// SEND
	NetworkManager &nm = NetworkManager::getNetworkManager();
	QNetworkReply *reply = nm.post(request, requestData);
	logInfo() << preLog << "Posting request to Geode";
	return reply;
}

bool LGCInputText::checkResponse(QNetworkReply *reply, const QString &preLog)
{
	int httpStatus = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
	logDebug() << preLog << "HTTP status code: " << httpStatus;
	if (httpStatus == 302 && !reply->bytesAvailable())
	{
		logWarning() << preLog << "Please ensure that you are logged in with Geode Plugin";
		return false;
	}
	else if (reply->error() != QNetworkReply::NoError)
	{
		logWarning() << preLog << "Geode connection failed: " << reply->errorString();
		return false;
	}

	logInfo() << _pimpl->prelogUpdatePoints << "Geode connection succesful";
	return true;
}

void LGCInputText::convertTSTN()
{
	// Starting and ending points
	int startLine, index;
	editor()->getCursorPosition(&startLine, &index);
	auto startLineText = editor()->text(startLine).simplified();
	if (startLine > 0 && startLine < (editor()->lines() - 1) && !startLineText.startsWith("*PLR3D") && !startLineText.startsWith("*ANGL"))
	{
		QMessageBox *conversionDeniedMessage = new QMessageBox(QMessageBox::Information, QString("TSTN Conversion"),
			QString("Wrong starting point for the conversion. Please ensure that you execute the action the top-most important section (PLR3D or ANGL)."));
		conversionDeniedMessage->show();
		return;
	}

	// get measures positions
	std::vector<int> measuresPositions;
	const auto &measures = lgcMeasures().measures;
	measuresPositions.reserve(measures.size());
	std::transform(measures.begin(), measures.end(), std::back_inserter(measuresPositions), [](const std::pair<int, LGCMeasures::Value> &m) { return m.second.position; });
	QMap<QString, QStringList> measuresKeywords = {{"ANGL", {"TRGT", "OBSE", "TCSE"}}, {"ZEND", {"TRGT", "OBSE", "TH", "THSE", "TCSE"}},
		{"DIST", {"TRGT", "OBSE", "PPM", "TH", "THSE", "TCSE"}}, {"PLR3D", {"TRGT", "TH", "THSE", "TCSE", "ASE", "ZSE", "DSE", "PPM"}}};

	QString out;
	int endLine = -1;
	// PLR3D => ANGL/ZEND/DIST
	if (startLineText.startsWith("*PLR3D"))
		std::tie(endLine, out) = convertTSTN_PLR3DTOANGL(startLine + 1, measuresKeywords, measuresPositions);
	// ANGL/ZEND/DIST => PLR3D
	else if (startLineText.startsWith("*ANGL"))
		std::tie(endLine, out) = convertTSTN_ANGLTOPLR3D(startLine + 1, measuresKeywords, measuresPositions);

	// Add and remove lines
	if (out.isEmpty() || endLine < 0)
		return;
	editor()->SendScintilla(QsciScintilla::SCI_BEGINUNDOACTION);
	editor()->insertAt(out, endLine, 0);
	editor()->SendScintilla(QsciScintilla::SCI_DELETERANGE, editor()->positionFromLineIndex(startLine, 0),
		editor()->positionFromLineIndex(endLine, 0) - editor()->positionFromLineIndex(startLine, 0));
	editor()->SendScintilla(QsciScintilla::SCI_ENDUNDOACTION);
}

std::tuple<int, QString> LGCInputText::convertTSTN_PLR3DTOANGL(int currLine, const QMap<QString, QStringList> &measuresKeywords, const std::vector<int> &measuresPositions)
{
	QString ANGL = "*ANGL\n";
	QString ZEND = "*ZEND\n";
	QString DIST = "*DIST\n";
	// Lambda to check if given keyword should be added to given measurement type
	auto addKeyword = [&ANGL, &ZEND, &DIST, &measuresKeywords](const QStringList &lineSplit, const QString &keyword) -> void {
		int idx = lineSplit.indexOf(keyword);
		if (idx != -1 && idx + 1 < lineSplit.size())
		{
			QString val = lineSplit[idx + 1];
			if (keyword == "ASE")
				ANGL += " OBSE " + val;
			else if (keyword == "ZSE")
				ZEND += " OBSE " + val;
			else if (keyword == "DSE")
				DIST += " OBSE " + val;
			else
			{
				QString toAdd = ' ' + keyword + ' ' + val;
				if (measuresKeywords["ANGL"].contains(keyword))
					ANGL += toAdd;
				if (measuresKeywords["ZEND"].contains(keyword))
					ZEND += toAdd;
				if (measuresKeywords["DIST"].contains(keyword))
					DIST += toAdd;
			}
		}
	};

	// iterate over lines
	while (currLine < editor()->lines())
	{
		QString lineText = editor()->text(currLine).simplified();
		int linePos = editor()->positionFromLineIndex(currLine, 0);
		if (lineText.startsWith('*'))
			break;
		currLine++;
		if (lineText.isEmpty())
			continue;

		QStringList lineSplit = lineText.split(' ');
		// comment
		QString comment;
		if (lineText.contains('$'))
			comment = " $" + lineText.split('$').at(1);
		else if (lineText.contains('%'))
			comment = " %" + lineText.split('%').at(1);

		QString name = lineSplit.first();
		// is measure
		if (lineSplit.count() >= 4 && std::find(measuresPositions.begin(), measuresPositions.end(), linePos) != measuresPositions.end())
		{
			ANGL += QString("%1%2").arg(name.leftJustified(44, ' '), lineSplit[1].leftJustified(16, ' '));
			ZEND += QString("%1%2").arg(name.leftJustified(44, ' '), lineSplit[2].leftJustified(16, ' '));
			DIST += QString("%1%2").arg(name.leftJustified(44, ' '), lineSplit[3].leftJustified(16, ' '));
			std::for_each(measuresKeywords["PLR3D"].cbegin(), measuresKeywords["PLR3D"].cend(),
				[&addKeyword, &lineSplit](const QString &keyword) { addKeyword(lineSplit, keyword); });
			ANGL += comment;
			ZEND += comment;
			DIST += comment;
		}
		// Probably comment line, add as was before
		else
		{
			ANGL += lineText;
			ZEND += lineText;
			DIST += lineText;
		}
		ANGL += '\n';
		ZEND += '\n';
		DIST += '\n';
	}
	return {currLine, ANGL + ZEND + DIST};
}

std::tuple<int, QString> LGCInputText::convertTSTN_ANGLTOPLR3D(int currLine, const QMap<QString, QStringList> &measuresKeywords, const std::vector<int> &measuresPositions)
{
	// Helpers
	struct MeasureReconstructor
	{
		struct Measure
		{
			QString definition; // defines a measure
			QMap<QString, QString> keywords; // defines measure's keywords <keyword, value>
			QPair<QString, QString> comments; // defines comment // <$, %>
		};

		QStringList orderedMeasures; // for data validation
		QVector<int> linesMeasures; // to preserve order of measures where multiple measure names can repeat
		QMap<int, Measure> measures;

		enum class ESectionCounter
		{
			ANGL = 0,
			ZEND,
			DIST,
			COUNT
		} sectionCounter = ESectionCounter::ANGL;

	} measureReconstructor;

	// add keyword to measureReconstructor from a given line
	auto getKeyword = [&measureReconstructor](const QStringList &lineSplit, int lineIdx, const QString &keyword) -> void {
		int idx = lineSplit.indexOf(keyword);

		if (idx == -1 || idx + 1 >= lineSplit.size())
			return;

		if (keyword == "OBSE")
		{
			if (measureReconstructor.sectionCounter == MeasureReconstructor::ESectionCounter::ANGL)
				measureReconstructor.measures[lineIdx].keywords["ASE"] = lineSplit[idx + 1];
			else if (measureReconstructor.sectionCounter == MeasureReconstructor::ESectionCounter::ZEND)
				measureReconstructor.measures[lineIdx].keywords["ZSE"] = lineSplit[idx + 1];
			else
				measureReconstructor.measures[lineIdx].keywords["DSE"] = lineSplit[idx + 1];
		}
		else
		{
			if (measureReconstructor.sectionCounter == MeasureReconstructor::ESectionCounter::ZEND && !measureReconstructor.measures[lineIdx].keywords[keyword].isEmpty())
			{
				logInfo() << QString("The keyword %1 repeats again in *ZEND. The value from *ANGL will be used.").arg(keyword);
				return;
			}
			else if (measureReconstructor.sectionCounter == MeasureReconstructor::ESectionCounter::DIST && !measureReconstructor.measures[lineIdx].keywords[keyword].isEmpty())
			{
				logInfo() << QString("The keyword %1 repeats again in *DIST. The value from *ANGL or *ZEND will be used, whatever came first.").arg(keyword);
				return;
			}

			measureReconstructor.measures[lineIdx].keywords[keyword] = lineSplit[idx + 1];
		}
	};

	int startLine = currLine;
	// data validation block
	{
		// Check if all sections are available (and in order) and if the point names match
		while (currLine < editor()->lines())
		{
			QString lineText = editor()->text(currLine).simplified();
			if (lineText.startsWith('*'))
			{
				if (measureReconstructor.sectionCounter == MeasureReconstructor::ESectionCounter::ANGL && lineText.startsWith("*ZEND"))
					measureReconstructor.sectionCounter = MeasureReconstructor::ESectionCounter::ZEND;
				else if (measureReconstructor.sectionCounter == MeasureReconstructor::ESectionCounter::ZEND && lineText.startsWith("*DIST"))
					measureReconstructor.sectionCounter = MeasureReconstructor::ESectionCounter::DIST;
				else
					break;
			}
			// Get point names
			else
			{
				measureReconstructor.orderedMeasures.push_back(lineText.split(' ').first());
				measureReconstructor.linesMeasures.append(currLine);
			}

			currLine++;
		}
		if (measureReconstructor.sectionCounter != MeasureReconstructor::ESectionCounter::DIST)
		{
			QMessageBox::information(this, tr("TSTN Conversion"),
				tr("The following paragraphs do not match required format. Please ensure that you execute the action in top-most important section that follows "
				   "proper order (ANGL > ZEND > DIST)."));
			return {-1, ""};
		}
		else if (measureReconstructor.orderedMeasures.size() % (int)MeasureReconstructor::ESectionCounter::COUNT)
		{
			QMessageBox::information(this, tr("TSTN Conversion"), tr("The point/comment count in ANGL ZEND DIST sections does not match."));
			return {-1, ""};
		}
		else
		{
			for (int i = 0; i < (measureReconstructor.orderedMeasures.size() / (int)MeasureReconstructor::ESectionCounter::COUNT); i++)
			{
				int anglId = i;
				int zendId = i + measureReconstructor.orderedMeasures.size() / (int)MeasureReconstructor::ESectionCounter::COUNT;
				int distId = i + 2 * (measureReconstructor.orderedMeasures.size() / (int)MeasureReconstructor::ESectionCounter::COUNT);

				if (measureReconstructor.orderedMeasures[anglId] != measureReconstructor.orderedMeasures[zendId]
					|| measureReconstructor.orderedMeasures[zendId] != measureReconstructor.orderedMeasures[distId])
				{
					QMessageBox::information(this, tr("TSTN Conversion"),
						QString("The point names or line comments (%1,  %2, %3) do not match.")
							.arg(measureReconstructor.orderedMeasures[anglId], measureReconstructor.orderedMeasures[zendId], measureReconstructor.orderedMeasures[distId]));
					return {-1, ""};
				}
			}
		}
	}

	// initialize maps
	measureReconstructor.orderedMeasures = measureReconstructor.orderedMeasures.mid(0, measureReconstructor.orderedMeasures.size() / (int)MeasureReconstructor::ESectionCounter::COUNT);
	measureReconstructor.sectionCounter = MeasureReconstructor::ESectionCounter::ANGL;
	for (int i = 0; i < measureReconstructor.linesMeasures.size() / (int)MeasureReconstructor::ESectionCounter::COUNT; i++)
	{
		measureReconstructor.measures[measureReconstructor.linesMeasures[i]].definition = measureReconstructor.orderedMeasures[i].leftJustified(44, ' ');
		measureReconstructor.measures[measureReconstructor.linesMeasures[i]].keywords;
		measureReconstructor.measures[measureReconstructor.linesMeasures[i]].comments;
	}
	// span of an entire section (in lines) from the first line of ANGL to the first line of ZEND
	int sectionSpan = measureReconstructor.linesMeasures.at(measureReconstructor.linesMeasures.size() / (int)MeasureReconstructor::ESectionCounter::COUNT)
		- measureReconstructor.linesMeasures.first();
	// Compose the points
	// iterate over lines
	int endLine = currLine;
	for (int line = startLine; line < endLine; line++)
	{
		QString lineText = editor()->text(line).simplified();
		int lineIdx = line - sectionSpan * (int)measureReconstructor.sectionCounter;
		// Adapt counter
		if (lineText.startsWith("*ZEND"))
		{
			measureReconstructor.sectionCounter = MeasureReconstructor::ESectionCounter::ZEND;
			continue;
		}
		else if (lineText.startsWith("*DIST"))
		{
			measureReconstructor.sectionCounter = MeasureReconstructor::ESectionCounter::DIST;
			continue;
		}

		QStringList lineSplit = lineText.split(' ');
		QString name = lineSplit.first();
		// measure
		if (lineSplit.count() >= 2 && std::find(measuresPositions.begin(), measuresPositions.end(), editor()->positionFromLineIndex(line, 0)) != measuresPositions.end())
		{
			measureReconstructor.measures[lineIdx].definition += lineSplit.at(1).leftJustified(16, ' ');
			// comment - only accept one (first that appears) to avoid stacking repetitions
			if (lineText.contains('$') && measureReconstructor.measures[lineIdx].comments.first.isEmpty())
				measureReconstructor.measures[lineIdx].comments.first = measureReconstructor.measures[lineIdx].comments.first + lineText.split('$').at(1);
			else if (lineText.contains('%') && measureReconstructor.measures[lineIdx].comments.first.isEmpty() && measureReconstructor.measures[lineIdx].comments.second.isEmpty())
				measureReconstructor.measures[lineIdx].comments.second = measureReconstructor.measures[lineIdx].comments.second + " %" + lineText.split('%').at(1);
			// keywords
			switch (measureReconstructor.sectionCounter)
			{
			case MeasureReconstructor::ESectionCounter::ANGL:
				for (const auto &kw : measuresKeywords["ANGL"])
					getKeyword(lineSplit, lineIdx, kw);
				break;
			case MeasureReconstructor::ESectionCounter::ZEND:
				for (const auto &kw : measuresKeywords["ZEND"])
					getKeyword(lineSplit, lineIdx, kw);
				break;
			case MeasureReconstructor::ESectionCounter::DIST:
				for (const auto &kw : measuresKeywords["DIST"])
					getKeyword(lineSplit, lineIdx, kw);
				break;
			}
		}
		// Probably comment line, add full line
		else
			measureReconstructor.measures[lineIdx].definition = lineText;
	}

	// Combine everything
	QString out = "*PLR3D\n";
	for (int i = 0; i < measureReconstructor.linesMeasures.size() / (int)MeasureReconstructor::ESectionCounter::COUNT; i++)
	{
		int lineIdx = measureReconstructor.linesMeasures[i];
		out += measureReconstructor.measures[lineIdx].definition;
		QMapIterator<QString, QString> mapIter(measureReconstructor.measures[lineIdx].keywords);
		while (mapIter.hasNext())
		{
			mapIter.next();
			out += ' ' + mapIter.key() + ' ' + mapIter.value();
		}
		if (!measureReconstructor.measures[lineIdx].comments.first.isEmpty())
			out += " $ " + measureReconstructor.measures[lineIdx].comments.first;
		else if (!measureReconstructor.measures[lineIdx].comments.second.isEmpty())
			out += " % " + measureReconstructor.measures[lineIdx].comments.second;
		out += '\n';
	}

	return {endLine, out};
}

void LGCInputText::selectToEnd()
{
	auto lex = qobject_cast<LGCInputLexerIO *>(editor()->lexer());
	if (!lex)
		return;
	int pos = lex->getEndPos();
	if (pos <= 0)
		return;
	int line, index;
	editor()->lineIndexFromPosition(pos, &line, &index);
	editor()->setSelection(0, 0, line, index);
}
