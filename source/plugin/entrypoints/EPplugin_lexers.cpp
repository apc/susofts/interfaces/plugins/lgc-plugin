#include "EPplugin_lexers.hpp"

#include <interface/SPluginInterface.hpp>

#include "tabs/inputs/LGCInputLexerIO.hpp"
#include "tabs/outputs/LGCOutputResLexer.hpp"

QList<QString> EPplugin_lexers::getNames()
{
	return {LGCInputLexerIO().language(), LGCOutputResLexer().language()};
}

QList<std::function<QsciLexer *(QWidget *)>> EPplugin_lexers::getLexers()
{
	return {[](QWidget *parent) -> QsciLexer * { return new LGCInputLexerIO(parent); }, [](QWidget *parent) -> QsciLexer * { return new LGCOutputResLexer(parent); }};
}
